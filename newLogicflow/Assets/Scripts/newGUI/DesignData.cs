﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using Netlist;

[Serializable()]
public class DesignData : ISerializable {

	[SerializeField] protected DesignObj _design;
	[SerializeField] protected List<DesignData> _designDataList;
	[SerializeField] protected List<NetData> _netDataList;
	[SerializeField] protected Vector2 _position;
	[SerializeField] protected LogicValue _logicValue;
	[SerializeField] protected bool exported;
	[SerializeField] protected float _clockTimer;

	[NonSerialized] protected GameObject _gameObject;

	[NonSerialized] private Design topDesign = TopDesign.instance.topDesign;
	[NonSerialized] private TopDesign instance = TopDesign.instance;

	//------------------------------------------------Constructors-------------------------------------------------------//
	DesignData() {}

	/// <summary>
	/// Initializes a new instance of the <see cref="DesignData"/> class.
	/// It Searches for a name on the Design Library and create a Instance object.
	/// </summary>
	/// <param name="designName">Design name.</param>
	/// <param name="n_inputs">N_inputs.</param>
	/// <param name="position">Position.</param>
	/// <param name="defaultValue">Default value.</param>
	public DesignData(string designName, int n_inputs, Vector2 position, LogicValue defaultValue = LogicValue.ZERO) {

		_designDataList = new List<DesignData> ();
		_netDataList = new List<NetData> ();
		_position = position;
		createInstance (designName, n_inputs, defaultValue);
		_gameObject = gameObject;
	}
	/**
	 * Creates a Instance. It only creates if the previous design is also a Instance (for integraty purposes)
	 **/
	public void createInstance(string instanceName, int n_inputs, LogicValue defaultValue = LogicValue.ZERO) {
		if (_design != null && !isInstance) {
			throw new System.InvalidOperationException ("Cannot create Instance, different Design type.");
		} else {
			if (TopDesign.instance.library.ContainsKey (instanceName + n_inputs)) {
				//busca na biblioteca primitiva
				Instance inst;
				if (_design != null) {
					//design already exists, remove it from the top design and disconnect from any net he is connected
					inst = (Instance) _design;
					inst.getInstTerminals (TermType.IN | TermType.OUT).ForEach (instterm => instterm.disconnect ());
					topDesign.removeInstance (inst);
				}
				Design cell = instance.library [instanceName + n_inputs];
				inst = Instance.createInstance (cell, topDesign, instance.registerName (cell.getName(),"I"));

				cell.getTerminals(TermType.IN | TermType.OUT).ForEach( t => InstTerminal.createInstTerminal(inst, t));
				_design = inst;
			} else if (TopDesign.instance.customLibrary.ContainsKey (instanceName)) {
				//busca na biblioteca customizada
				Instance inst;
				if (_design != null) {
					//design already exists, remove it from the top design and disconnect from any net he is connected
					inst = (Instance) _design;
					inst.getInstTerminals (TermType.IN | TermType.OUT).ForEach (instterm => instterm.disconnect ());
					topDesign.removeInstance (inst);
				}
				Design cell = instance.customLibrary[instanceName];
				inst = Instance.createInstance (cell, topDesign, instance.registerName (instanceName,"I"));

				cell.getTerminals(TermType.IN | TermType.OUT).ForEach( t => InstTerminal.createInstTerminal(inst, t));
				_design = inst;

			} else
				throw new System.MissingMemberException("Cannot create Instance, Design not found in Library: " + instanceName + n_inputs + ".");
		}
		_gameObject = null;

	}
	/// <summary>
	/// Initializes a new instance of the <see cref="DesignData"/> class.
	/// </summary>
	/// <param name="designName">Design name.</param>
	/// <param name="position">Position.</param>
	public DesignData(string designName, Vector2 position) {
		_designDataList = new List<DesignData>();
		_netDataList = new List<NetData> ();
		_position = position;
		createDesign (designName);
		_gameObject = gameObject;
	}

	public void createDesign(string designName) {
		if (_design != null) {
			//design jah existe
			//Design des = (Design) _design;
		}
		_design = Design.createDesign (designName);
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="DesignData"/> class.
	/// </summary>
	/// <param name="terminalName">Terminal name.</param>
	/// <param name="position">Position.</param>
	/// <param name="type">Type.</param>
	/// <param name="defaultValue">Default value.</param>
	public DesignData(string terminalName, Vector2 position, TermType type, LogicValue defaultValue = LogicValue.ZERO) {
		_designDataList = new List<DesignData>();
		_netDataList = new List<NetData> ();
		_position = position;
		createTerminal (terminalName, type, defaultValue);
		_gameObject = gameObject;
	}
	public DesignData(string terminalName, Vector2 position, TermType type, float defaultClock = 1000) {
		_designDataList = new List<DesignData>();
		_netDataList = new List<NetData> ();
		_position = position;
		clockTimer = defaultClock;
		createTerminal (terminalName, type, LogicValue.ZERO);
		_gameObject = gameObject;
	}
	/**
	 * Creates a Terminal
	 **/
	public void createTerminal(string terminalName, TermType type, LogicValue defaultValue = LogicValue.ZERO) {
		if (_design != null && !isTerminal) {
			throw new System.InvalidOperationException ("Cannot create Terminal, different Design type.");
		} else {
			if (_design != null) {
				Terminal term = (Terminal) _design;
				term.disconnect ();
				this.topDesign.removeTerminal (term);
			}

			_design = Terminal.createTerminal(topDesign, instance.registerName(terminalName, "T"), type, terminalName);
			if (type == TermType.IN)
				this.instance.simulator.setValue(_design, defaultValue);
		}
		_gameObject = null;
	}

	public DesignData(SerializationInfo info, StreamingContext context) {
		_designDataList = (List<DesignData>)info.GetValue ("DesignDataList", typeof(List<DesignData>));
		_netDataList 	= (List<NetData>)info.GetValue ("NetDataList", typeof(List<NetData>));
		_design 		= (DesignObj)info.GetValue ("Design", typeof(DesignObj));
		_logicValue		= (LogicValue)info.GetValue ("LogicValue", typeof(LogicValue));
		float posX 		= (float)info.GetValue ("PosX", typeof(float));
		float posY 		= (float)info.GetValue ("PosY", typeof(float));
		_position 		= new Vector2 (posX,posY);
		_clockTimer		= (float)info.GetValue ("ClockTimer", typeof(float));
	}

	void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
		info.AddValue("DesignDataList", _designDataList, typeof(List<DesignData>));
		info.AddValue("NetDataList", _netDataList, typeof(List<NetData>));
		info.AddValue("Design", _design, typeof(DesignObj));
		info.AddValue("LogicValue", _logicValue, typeof(LogicValue));
		info.AddValue ("PosX", _position.x, typeof(float));
		info.AddValue ("PosY", _position.y, typeof(float));
		info.AddValue("ClockTimer", _clockTimer, typeof(float));

	}

	//------------------------------------------------Getters and Setters-------------------------------------------------------//
	public List<DesignData> designDataList {
		get { return _designDataList; }
		set { _designDataList = value; }
	}
	public List<NetData> netDataList {
		get {
			if (_netDataList == null) {
				_netDataList = new List<NetData>();
			}
			return _netDataList;
		}
		set { _netDataList = value; }
	}
	public GameObject gameObject {
		get {
			if (_gameObject == null) {
				//calls for a prefab for each type of Design
				if (isInstance || isTerminal || isDesign) {

					_gameObject = (GameObject) GameObject.Instantiate(Resources.Load ("newGUI/instanceItem"));
					_gameObject.transform.position = new Vector3(position.x, position.y, 1);
					_gameObject.name = _design.getName();
					_gameObject.GetComponent<instanceItem>().designData = this;
				}
			}
			return _gameObject;
		}
	}
	public Vector2 position {
		get { return _position; }
		set { _position = value; }
	}
	public DesignObj design {
		get { return _design; }
	}

	public string designName {
		get {
			if (this.isInstance) {
				Instance inst = (Instance) this.design;
				return inst.getComponent ().getName ();
			} else if (this.isTerminal) {
				Terminal term = (Terminal) this.design;
				return term.getComponentName();
			}
			return name;
		}
	}
	public LogicValue logicValue {
		get { 
			_logicValue = TopDesign.instance.getLogicValue(design);
			return _logicValue;
		}
		set {
			TopDesign.instance.setLogicValue(design,value);
			_logicValue = value;
		}
	}
	public void setLogicValueOnLoad() {
		TopDesign.instance.setLogicValue(design,_logicValue);
	}
	public string name { get { return _design.getName (); } }

	public float clockTimer {
		get { return _clockTimer; }
		set {_clockTimer = value; }
	}

	public bool isInstance 	  { get { return _design != null ? _design.GetType ().Name.Equals ("Instance") : false; } }
	public bool isTerminal 	  { get { return _design != null ? _design.GetType ().Name.Equals ("Terminal") : false; } }
	public bool isNet 		  { get { return _design != null ? _design.GetType().Name.Equals("Net") : false;		} }
	public bool isDesign 	  { get { return _design != null ? _design.GetType().Name.Equals("Design") : ( isInstance ? ((Instance) _design).getComponent().isModule() : false ) ; } }
	public bool isModule 	  {	get { return isDesign ? ((Design) design).isModule() : false; } }
	public bool isTermTypeIn  { get { return isTerminal ? ((Terminal)_design).getTerminalType().Equals(TermType.IN) : false; } }
	public bool isTermTypeOut { get { return isTerminal ? ((Terminal)_design).getTerminalType().Equals(TermType.OUT) : false; } }
	//------------------------------------------------Utilities-------------------------------------------------------//
	public bool isExportable {
		get {
			if (!isModule)
				return false;
			Design des = (Design) design;
			if (des.getTerminals(TermType.IN).Count == 0 || des.getTerminals(TermType.OUT).Count == 0)
				return false;
			return true;
		}
	}
	public int n_inputs {
		get {
			if (isDesign) {
				Design des = (Design) design;
				return des.getTerminals(TermType.IN).Count;
			} else if (isInstance) {
				Instance inst = (Instance) design;
				return inst.getInstTerminals(TermType.IN).Count;
			} else
				return -1;
		}
	}
	public int n_outputs {
		get {
			if (isDesign) {
				Design des = (Design) design;
				return des.getTerminals(TermType.OUT).Count;
			} else if (isInstance) {
				Instance inst = (Instance) design;
				return inst.getInstTerminals(TermType.OUT).Count;
			} else
				return -1;
		}
	}
	public List<TerminalItem> getTerminals() {
		TerminalItem[] terminals = gameObject.GetComponentsInChildren<TerminalItem> ();
		return terminals.ToList ();
	}

	public TerminalItem findTerminalItem(ConnectionPoint cp) {
		TerminalItem term = getTerminals ().Find (t => t.connectionPoint.Equals (cp));
		if (term == null) {
			designDataList.ForEach( d => {
				TerminalItem ret = d.findTerminalItem(cp);
				term = ret != null ? ret : term;
			});
		}
		return term;
	}

	public bool removeDesign(DesignData desData) {
		bool retorno = false;
		if (desData != null) {
			Design des = (Design) desData.design;
			if (desData.isInstance)
				retorno = des.removeInstance((Instance) desData.design);
			else
				retorno = des.removeTerminal((Terminal) desData.design);

			retorno = designDataList.Remove(desData) ? retorno : false;

		}
		return retorno;
	}
	public bool removeNet(NetData netData) {
		bool retorno = false;
		if (netData != null) {

			retorno = true;
		}
		return retorno;
	}
	public GameObject resetGameObject() {
		_gameObject = null;
		_gameObject = gameObject;
		return _gameObject;
	}
}
