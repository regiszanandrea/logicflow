﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using Netlist;

public class guiController : MonoBehaviour {

	public Canvas guiCanvas;

	public RectTransform topHolderRect;
	public RectTransform gridBackground;
	Notifications notifications;
	public RectTransform interfacePanel;
    public RectTransform interfaceHolder;
	public Image interfaceMinimize;
    public bool interfaceEnabled;
    
	public RectTransform libraryScrollPanel; //scroll panel
	public RectTransform[] panels;
	public RectTransform center;
	public InputField libraryInputField;
	public Slider librarySlider;
	public Text libraryLabel;

	public RectTransform actionPanel;
	public Text actionText;
	public Image actionImage;

	//private float[] distanceLibraryRect;
	private int panelDistance; // distancia entre os panels
	private int minPanelNum; //numero do panel com menor distancia ao centro

    public bool libraryEnabled;

    public Text designNameText;
	Vector2 designNameDefaultPosition, designNameDefaultSize;
    public Button exitButton, saveButton, loadButton, deleteButton, nextButton, previousButton, simulateButton;
	public InputField designNameField;

    public float paddingInterface = -50f;
    public float paddingLibrary = 0f;

	public static float _maxScale = 5;
	public static float _minScale = 1;
	public float maxScale {
		get { return _maxScale;  }
		set { _maxScale = value; }
	}
	public float minScale {
		get { return _minScale;  }
		set { _minScale = value; }
	}
	public float maxSqrtMagnitude, minSqrtMagnitude;
	public float speedDrag = 10;

	public int minValueClock = 500;
	public int maxValueClock = 3000;
	public static guiController instance;
	private bool libraryInterfaceToggle = false;
	private LibraryItem lastKnowLibrary;

    void Start() {
		guiController.instance = this;

		notifications = Notifications.instance;
		topHolderRect = topHolderRect.GetComponent<RectTransform>();
		gridBackground = gridBackground.GetComponent<RectTransform>();

		interfacePanel = interfacePanel.GetComponent<RectTransform>();
		interfaceHolder = interfaceHolder.GetComponent<RectTransform>();
		interfaceMinimize = interfaceMinimize.GetComponent<Image>();
		libraryScrollPanel = libraryScrollPanel.GetComponent<RectTransform>();
		libraryInputField = libraryInputField.GetComponent<InputField>();
		librarySlider = librarySlider.GetComponent<Slider>();
		libraryLabel = libraryLabel.GetComponent<Text>();
		interfaceEnabled = true;

		//distanceLibraryRect = new float[panelLenght];
		panelDistance = (int) Mathf.Abs (panels [1].GetComponent<RectTransform> ().anchoredPosition.x - panels [0].GetComponent<RectTransform> ().anchoredPosition.x);
		PanelSize ();

		maxSqrtMagnitude = maxScale * maxScale + maxScale * maxScale + 1;
		minSqrtMagnitude = minScale * minScale + minScale * minScale + 1;

		designNameText = designNameText.GetComponent<Text>();
		exitButton = exitButton.GetComponent<Button>();
		saveButton = saveButton.GetComponent<Button>();
		loadButton = loadButton.GetComponent<Button>();
		deleteButton = deleteButton.GetComponent<Button>();
		nextButton = nextButton.GetComponent<Button>();
		previousButton = previousButton.GetComponent<Button>();
		simulateButton = simulateButton.GetComponent<Button> ();
		designNameField = designNameField.GetComponent<InputField>();
		designNameDefaultPosition = designNameField.GetComponent<RectTransform> ().anchoredPosition;
		designNameDefaultSize = designNameField.GetComponent<RectTransform> ().sizeDelta;

		actionPanel = actionPanel.GetComponent<RectTransform> ();
		actionText = actionText.GetComponent<Text> ();
		actionImage = actionImage.GetComponent<Image> ();
		notifications.actionPanel = actionPanel;
		notifications.actionText = actionText;
		notifications.action = "";
		notifications.actionPanelToggle = true;
		notifications.speedActionPanelToggle = 250f;
		notifications.panelOnPos = 30f;
		notifications.panelOffPos = -9f;
		notifications.notification ("");

		TopDesign.selectedAction = "";

		simulateButton.image.color = TopDesign.simulate ? Color.green : Color.white;

		buildCustomLibrary();

    }

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.LoadLevel("MenuInicio");

		lerpToLibrary(minPanelNum * -panelDistance);

		Zoom ();
		PanelSize ();

		notifications.update ();
		//interfacePanel animation
		/**/
		float interfacePanelX = interfacePanel.anchoredPosition.x;
		//float interfaceXPos = Screen.width - interfacePanel.sizeDelta.x;
		//Debug.Log ("interfaceXPos = " + interfaceXPos);
		if (interfaceEnabled) {
			interfacePanelX = interfacePanelX > 350 ? interfacePanelX - (Time.deltaTime * 250f) : 350;
			interfaceMinimize.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (new Vector3 (0,0,0f));
		} else {
			interfacePanelX = interfacePanelX < 431 ? interfacePanelX + (Time.deltaTime * 250f) : 431;
			interfaceMinimize.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (new Vector3 (0,0,180f));
		}
		interfacePanel.anchoredPosition = new Vector2 (interfacePanelX, interfacePanel.anchoredPosition.y);
		if (!notifications.actionPanelToggle)
			actionImage.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (new Vector3 (0,0,90f));
		else
			actionImage.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (new Vector3 (0,0,270f));

		bool change = libraryInterfaceToggle;
		libraryInterfaceToggle = false;
		if (LibraryItem.selectedLibraryItem != null) {
			bool libraryChange = LibraryItem.selectedLibraryItem != lastKnowLibrary;
			if (LibraryItem.selectedLibraryItem.name.Equals("CLOCK")) {
				librarySlider.minValue = minValueClock;
				librarySlider.maxValue = maxValueClock;
				if (libraryChange) {
					librarySlider.value = LibraryItem.selectedLibraryItem.clock;
					libraryInputField.text = LibraryItem.selectedLibraryItem.clock.ToString();
				}
				libraryLabel.text = "ms";
			} else {
				librarySlider.minValue = 1;
				librarySlider.maxValue = 8;
				if (libraryChange) {
					librarySlider.value = LibraryItem.selectedLibraryItem.n_inputs;
					libraryInputField.text = LibraryItem.selectedLibraryItem.n_inputs.ToString();
				}
				libraryLabel.text  = "portas";
			}
		} else {
			change = false;
			librarySlider.value = 0;
			libraryInputField.text = "";
			libraryLabel.text = "";
		}
		libraryInterfaceToggle = change;
		lastKnowLibrary = LibraryItem.selectedLibraryItem;

		libraryInputField.interactable = libraryInterfaceToggle;
		librarySlider.interactable = libraryInterfaceToggle;
	}

	/// <summary>
	/// Zoom.
	/// </summary>
	void Zoom() {
		float distance = 0;
		if (Input.touchCount > 1) {
			gridBackground.GetComponent<ScrollRect> ().horizontal = false;
			gridBackground.GetComponent<ScrollRect> ().vertical = false;
		} else {
			gridBackground.GetComponent<ScrollRect> ().horizontal = true;
			gridBackground.GetComponent<ScrollRect> ().vertical = true;
		}
		if (Input.touchCount >= 2) {
			// Store both touches.
			float speedZoom = 0.03f;
			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);
			
			// Find the position in the previous frame of each touch.
			//Debug.Log("Zero " + touchZero.position + " One " + touchOne.position);
			//Debug.Log("touchZero.deltaPosition = " + touchZero.deltaPosition + " touchOne.deltaPosition = " + touchOne.deltaPosition);
			Vector2 maior = Vector2.Max (touchZero.deltaPosition, touchOne.deltaPosition);
			if (maior.x > 1.0f || maior.y > 1.0f) {
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
				
				
				// Find the magnitude of the vector (the distance) between the touches in each frame.
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
				
				// Find the difference in the distances between each frame.
				float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
				
				//Debug.Log("deltaMagnitudeDiff = " + deltaMagnitudeDiff);
				distance = -(deltaMagnitudeDiff * speedZoom);
			}
		}

		topHolderPanel.topHolderRect.localScale += new Vector3 (distance, distance, 0);
		
		if (topHolderPanel.topHolderRect.localScale.sqrMagnitude > guiController.instance.maxSqrtMagnitude)
			topHolderPanel.topHolderRect.localScale = new Vector3 (guiController.instance.maxScale,guiController.instance.maxScale,1);
		if (topHolderPanel.topHolderRect.localScale.sqrMagnitude < guiController.instance.minSqrtMagnitude)
			topHolderPanel.topHolderRect.localScale = new Vector3 (guiController.instance.minScale,guiController.instance.minScale,1);
		
		float x = topHolderPanel.topHolderRect.localScale.x + distance;
		x = x > maxScale ? maxScale : x < 1 ? 1 : x;
		topHolderPanel.topHolderRect.localScale = new Vector3 (x, x, 0);

	}
	void PanelSize() {
		//calcula o tamanho de cada panel dependendo do numero de filhos que tem
		foreach (RectTransform panel in panels) {
			RectTransform[] children = panel.GetComponentsInChildren<RectTransform>();
			float sizeY = 0;
			//Debug.Log(panel.name);
			for( int i = 0; i < children.Length; i++ ) {
				if (children[i] != panel) {
					//Debug.Log(children[i].name);
					sizeY+=children[i].sizeDelta.y;
				}
			}
			sizeY+= (25 * children.Length);
			panel.sizeDelta = new Vector2(panel.sizeDelta.x, sizeY);
		}
		
	}
	void lerpToLibrary(int position) {
		float newX = Mathf.Lerp (libraryScrollPanel.anchoredPosition.x, position, Time.deltaTime * 10f);
		Vector2 newPosition = new Vector2 (newX, libraryScrollPanel.anchoredPosition.y);
		libraryScrollPanel.anchoredPosition = newPosition;
	}

    void lerpToSlide(RectTransform panel, float target) {
      float newX = Mathf.Lerp(panel.anchoredPosition.x, target, Time.deltaTime * 10f);
      Vector2 newPosition = new Vector2(newX, panel.anchoredPosition.y);

      panel.anchoredPosition = newPosition;
    }

    public void exitButtonPressed() {
		Application.LoadLevel("MenuInicio");
    }
    public void onDesignNameChange(string str) {
		designNameText.text = str;
    }
	public void NextLibrary() {
		if (minPanelNum < panels.Length - 1) {
			minPanelNum++;
			libraryScrollPanel.GetComponent<ScrollRect> ().content = panels [minPanelNum];
		}
	}
	public void PreviousLibrary() {
		if (minPanelNum > 0) {
			minPanelNum--;
			libraryScrollPanel.GetComponent<ScrollRect> ().content = panels [minPanelNum];
		}
	}

	public void toggleActionPanel() {
		notifications.actionPanelToggle = !notifications.actionPanelToggle;
		//.localRotation = Quaternion.Euler(new Vector3(0,0,90f));
		//} else
		//actionPanel.GetComponentInChildren<RectTransform> ().localRotation = Quaternion.Euler(new Vector3(0,0,270f));
	}

	public void DeleteAction() {
		string action = "delete";
		if (TopDesign.selectedAction != action) {
			TopDesign.selectedAction = action;
			notifications.warning (action);
		} else {
			TopDesign.selectedAction = "";
			notifications.notification("");
		}
	}
	public void SaveAction() {
		//string action = "save";
		//TopDesign.selectedAction = TopDesign.selectedAction != action ? action : "";
		//Notifications.instance.notification(action);

		if (designNameField.text == "") {
			Notifications.instance.warning ("fileNameEmpty");
		} else {
			Serializer serializer = new Serializer (Serializer.projects);
			serializer.SerializeObject (designNameField.text + ".designData", TopDesign.instance.designData);
			Notifications.instance.notification ("saveComplete");
		}
	}
	public void ExportAction() {
		if (designNameField.text == "") {
			Notifications.instance.warning ("fileNameEmpty");
		} else {
			if (TopDesign.instance.designData.isExportable) {
				Serializer serializer = new Serializer (Serializer.library);
				serializer.SerializeObject (designNameField.text + ".design", (Design) TopDesign.instance.designData.design);
				Notifications.instance.notification ("exportComplete");
			}
		}
	}

	public void fileNameInputBegin() {
		Vector2 offset = new Vector2 (24f, 0f);
		Vector2 size = new Vector2 (designNameDefaultSize.x + offset.x, designNameDefaultSize.y);
		Vector2 position = new Vector2 (designNameDefaultPosition.x - (offset.x / 2), designNameDefaultPosition.y);

		designNameField.GetComponent<RectTransform>().sizeDelta = size;
		designNameField.GetComponent<RectTransform> ().anchoredPosition = position;
	}
	public void fileNameInputEnd() {
		designNameField.GetComponent<RectTransform>().sizeDelta = designNameDefaultSize;
		designNameField.GetComponent<RectTransform> ().anchoredPosition = designNameDefaultPosition;
		((Design)TopDesign.instance.designData.design).setName (designNameField.text);
	}
	public void ToggleInterface() {
		interfaceEnabled = !interfaceEnabled;
	}
	public void librarySliderChange() {
		if (libraryInterfaceToggle) {
			if (LibraryItem.selectedLibraryItem.name.Equals("CLOCK")) {
				LibraryItem.selectedLibraryItem.clock = (int)librarySlider.value;
				libraryInputField.text = librarySlider.value.ToString();
			} else {
				LibraryItem.selectedLibraryItem.n_inputs = (int)librarySlider.value;
				libraryInputField.text = librarySlider.value.ToString();
			}
		}
	}
	public void libraryInputFieldChange() {
		if (libraryInterfaceToggle) {
			if (LibraryItem.selectedLibraryItem.name.Equals("CLOCK")) {
				if (libraryInputField.text != "" && int.Parse(libraryInputField.text) < minValueClock)
					libraryInputField.text = minValueClock.ToString();

				if (libraryInputField.text != "" && int.Parse(libraryInputField.text) > maxValueClock)
					libraryInputField.text = maxValueClock.ToString();
				LibraryItem.selectedLibraryItem.clock = int.Parse(libraryInputField.text);
				librarySlider.value = float.Parse(libraryInputField.text);

			} else {
				LibraryItem.selectedLibraryItem.n_inputs = int.Parse(libraryInputField.text);
				librarySlider.value = float.Parse(libraryInputField.text);
			}
		}
	}
	public void libraryItemChange() {
		libraryInterfaceToggle = true;
		if (LibraryItem.selectedLibraryItem == null) {
			libraryInterfaceToggle = false;
		}
	}
	public void libraryItemChangeOff() {
		libraryInterfaceToggle = false;
	}

	public void buildCustomLibrary() {
		GameObject prefab = (GameObject) Resources.Load("newGUI/libraryItemButton");
		Vector2 sizeDelta = prefab.GetComponent<RectTransform>().sizeDelta;
		RectTransform CustomDesignPanel = null;
		foreach (RectTransform rect in panels) {
			CustomDesignPanel = rect.name == "CustomDesign" ? rect : CustomDesignPanel;
		}
		if (CustomDesignPanel != null) {
			CustomDesignPanel = CustomDesignPanel.GetComponent<RectTransform>();
			int i = 0;

			foreach (string lib in TopDesign.instance.customLibrary.Keys) {
				Design customDesign = TopDesign.instance.customLibrary [lib];
				GameObject loadedDesignRect = GameObject.Instantiate(prefab) as GameObject;
				loadedDesignRect.transform.SetParent(CustomDesignPanel);
				loadedDesignRect.name = customDesign.getName();
				loadedDesignRect.GetComponent<RectTransform>().localScale = Vector3.one;
				loadedDesignRect.GetComponent<RectTransform>().localPosition = Vector3.zero;
				loadedDesignRect.GetComponent<LibraryItem>().tipo = DesignType.INSTANCE;
				loadedDesignRect.GetComponent<LibraryItem>().n_inputs = customDesign.getTerminals(TermType.IN).Count;
				loadedDesignRect.GetComponentInChildren<Text>().text = customDesign.getName();

				Vector2 pos = new Vector2(0, -30 - ((sizeDelta.y + 15) * i));
				loadedDesignRect.GetComponent<RectTransform>().anchoredPosition = pos;
				i++;
				//customDesignRect.transform.SetParent = panels
			}
		}
	}
	public void Simulate() {
		TopDesign.simulate = TopDesign.simulate ? false : true;
		simulateButton.image.color = TopDesign.simulate ? Color.green : Color.white;
		if (!TopDesign.simulate) {
			TopDesign.instance.designData.designDataList.ForEach(d => {
				if (d.isTermTypeOut) {
					d.logicValue = LogicValue.ZERO;
				}
			});
		} else {
			TopDesign.instance.OnSimulate();
		}
	}
}

/**
* Sistema de notificacao
**/
public class Notifications {
	private static Notifications _instance;

	public static Notifications instance {
		get {
			if (_instance == null) {
				_instance = new Notifications();
				_instance.action = "default";
				_instance.speedActionPanelToggle = 250f;
			}
			return _instance;
		}
	}

	Notifications() {}

	private RectTransform _actionPanel;
	private Text _actionText;

	private string _lastAction;
	private string _action;
	private bool _actionPanelToggle;
	private float _speedActionPanelToggle;
	private float _panelOnPos, _panelOffPos;
	private Dictionary<string, string> _messages;

	public RectTransform actionPanel {
		get { return _actionPanel; }
		set {  _actionPanel = value; }
	}
	public Text actionText {
		get { return _actionText; }
		set {  _actionText = value; }
	}
	public string action {
		get { return _action; }
		set {  _action = value; }
	}
	public float speedActionPanelToggle {
		get { return _speedActionPanelToggle; }
		set {  _speedActionPanelToggle = value; }
	}
	public bool actionPanelToggle {
		get { return _actionPanelToggle; }
		set {  _actionPanelToggle = value; }
	}
	public float panelOnPos {
		get { return _panelOnPos; }
		set {  _panelOnPos = value; }
	}
	public float panelOffPos {
		get { return _panelOffPos; }
		set {  _panelOffPos = value; }
	}

	public Dictionary<string, string> messages {
		get {
			if (_messages == null) {
				_messages = new Dictionary<string, string>();
				_messages.Add("default", "Selecione um item na biblioteca para inserir.");
				_messages.Add("insert", "SELECIONADO: Toque na área de edição para adicionar o item selecionado.");
				_messages.Add("delete", "DELETAR: Selecione uma conexao ou um item para remover.");
				_messages.Add("save", "SALVAR");
				_messages.Add("load", "CARREGAR");
				_messages.Add("fileNameEmpty", "Nome de arquivo vazio.");
				_messages.Add("saveComplete", "Projeto salvo com sucesso.");
				_messages.Add("exportComplete", "Projeto exportado para biblioteca com sucesso.");

			}
			return _messages;
		}
	}

	public string message(string action) {
		if (messages.ContainsKey (action)) {
			string retorno = TopDesign.selectedLibrary;
			return retorno + " " + messages [action];
		} else if (action == "") {
			return messages["default"];
		} else {
			return action;
		}
	}
	public void notification(string msg) {
		actionPanel.GetComponent<Image> ().color = Color.white;
		actionText.text = message (msg);
		//Debug.Log ("notification " + msg);

	}
	public void warning(string msg) {
		actionPanelToggle = true;
		actionPanel.GetComponent<Image> ().color = Color.red;
		//Debug.Log ("warning " + msg);
		actionText.text = message (msg);
	}

	public void update() {

		//actionPanel animation
		float newY = actionPanel.anchoredPosition.y;
		if (actionPanelToggle)
			newY = newY < panelOnPos ? newY + Time.deltaTime * speedActionPanelToggle : panelOnPos;
		else
			newY = newY > panelOffPos ? newY -(Time.deltaTime * speedActionPanelToggle) : panelOffPos;
		
		actionPanel.anchoredPosition = new Vector2 (actionPanel.anchoredPosition.x, newY);

	}
}
