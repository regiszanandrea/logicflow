﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using Netlist;

public class menuGuiController : MonoBehaviour {

	public Button novoButton;
	public Button carregarButton;
	public Button exitButton;

	public RectTransform loadPanel;
	public RectTransform bibliotecaPanel;
	public RectTransform iconsProjectRect;
	public RectTransform iconsLibraryRect;
	public bool loadProjectPanelActive;
	public bool loadLibraryPanelActive;
	public Text versionLabel;

	public static DesignData loadDesignData;
	public static Design loadDesign;
	public static string loadFileName;
	public static string loadFileLibrary;
	
	int maxHorizontal = 8;
	float offsetX = 5f;
	float offsetY = 30f;
	
	List<GameObject> iconsProject;
	List<GameObject> iconsLibrary;

	void Start () {

		LogicFlowConfig config = new LogicFlowConfig();

		string configFile = Path.Combine(Application.persistentDataPath, "config.xml");
		bool oldVersion = false;
		if (!System.IO.File.Exists(configFile)) {
			oldVersion = true;
			LogicFlowConfig.RemoveAllFiles();
		} else {
			LogicFlowConfig savedConfig = LogicFlowConfig.Load(configFile);
			if (!config.fullVersion.Equals(savedConfig.fullVersion))
				oldVersion = true;
		}

		if (oldVersion) {
			config.Save(configFile);
		} else {

		}

		//salvando versao nova

		novoButton = novoButton.GetComponent<Button> ();
		carregarButton = carregarButton.GetComponent<Button> ();
		exitButton = exitButton.GetComponent<Button> ();

		loadPanel = loadPanel.GetComponent<RectTransform> ();
		iconsProjectRect = iconsProjectRect.GetComponent<RectTransform> ();
		iconsLibraryRect = iconsLibraryRect.GetComponent<RectTransform> ();
		versionLabel = versionLabel.GetComponent<Text>();
		loadProjectPanelActive = false;
		loadLibraryPanelActive = false;

		versionLabel.text = "Versão."+config.fullVersion + (oldVersion ? "!" : "");
		createIconsProject ();
		createIconsLibrary ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void NovoProjeto() {
		TopDesign.instance.reset(new DesignData ("NovoProjeto", Vector2.zero));

		Application.LoadLevel ("newGUI");
	}
	public void CarregarProjeto() {
		if (loadDesignData != null) {
			TopDesign.instance.reset (loadDesignData);
			Application.LoadLevel ("newGUI");
		}
	}
	public void ExcluirProjeto() {
		if (loadDesignData != null) {
			string filename = loadFileName + ".designData";

			string fullPath = System.IO.Path.Combine (Serializer.projects, filename);
			Debug.Log("Removendo: " + fullPath);
			File.Delete(fullPath);
			createIconsProject();
		}
	}
	public void ExcluirBiblioteca() {
		if (loadDesign != null) {
			string filename = loadFileLibrary + ".design";
			string fullPath = System.IO.Path.Combine (Serializer.library, filename);
			Debug.Log("Removendo: " + fullPath);
			File.Delete(fullPath);
			createIconsLibrary();
		}
	}
	public void toggleLoadProject () {
		loadProjectPanelActive = !loadProjectPanelActive;
		if (loadProjectPanelActive) {
			loadPanel.anchoredPosition = new Vector2(0,0);
		} else {
			loadPanel.anchoredPosition = new Vector2(0,5000);
		}
	}
	public void toggleLoadLibrary () {
		loadLibraryPanelActive = !loadLibraryPanelActive;
		if (loadLibraryPanelActive) {
			bibliotecaPanel.anchoredPosition = new Vector2(0,0);
		} else {
			bibliotecaPanel.anchoredPosition = new Vector2(0,5000);
		}
	}

	public void Exit() {
		Application.Quit ();
	}
	
	public void createIconsProject() {
		TopDesign.reloadCustomProjects ();
		if (iconsProject != null)
			iconsProject.ForEach( x=> Destroy (x));
		iconsProject = new List<GameObject> ();
		
		GameObject prefab = (GameObject) Resources.Load ("newGUI/loadItem");
		Vector2 sizeDelta = prefab.GetComponent<RectTransform>().sizeDelta;
		int c = 0;
		
		foreach (string key in TopDesign.customProjects.Keys) {
			DesignData data = TopDesign.customProjects[key];
			GameObject go = (GameObject) GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
			go.name = data.name;
			go.GetComponent<loadItem>().designData = data;
			
			go.transform.SetParent(iconsProjectRect.transform);
			sizeDelta = go.GetComponent<RectTransform>().sizeDelta;
			go.transform.GetComponentInChildren<Text>().text = data.name;
			
			Vector2 pos = new Vector2( ((sizeDelta.x + offsetX) * (c % maxHorizontal)) + offsetX, -((sizeDelta.y + offsetY) * (Mathf.Ceil(c / maxHorizontal) ) ) - offsetX );
			
			go.GetComponent<RectTransform>().anchoredPosition = pos;
			iconsProject.Add(go);
			c++;
			
		}
		
		float totalSizeY = Mathf.Max((sizeDelta.y + offsetY) * (Mathf.Ceil ((TopDesign.customProjects.Count) / maxHorizontal)),266f);
		Vector2 thisSizeDelta = iconsProjectRect.sizeDelta;
		iconsProjectRect.sizeDelta = new Vector2 (thisSizeDelta.x, totalSizeY);
		
	}
	public void createIconsLibrary() {
		TopDesign.instance.rebuildCustomLibrary();
		if (iconsLibrary != null)
			iconsLibrary.ForEach(x => Destroy(x));
		iconsLibrary = new List<GameObject>();

		GameObject prefab = (GameObject) Resources.Load ("newGUI/loadItemLibrary");
		Vector2 sizeDelta = prefab.GetComponent<RectTransform>().sizeDelta;
		int c = 0;
		foreach (string key in TopDesign.instance.customLibrary.Keys) {
			Design data = TopDesign.instance.customLibrary[key];
			GameObject go = (GameObject) GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);
			go.name = data.getName();
			go.GetComponent<loadItemLibrary>().design = data;
			
			go.transform.SetParent(iconsLibraryRect.transform);
			sizeDelta = go.GetComponent<RectTransform>().sizeDelta;
			go.transform.GetComponentInChildren<Text>().text = data.getName();
			
			Vector2 pos = new Vector2( ((sizeDelta.x + offsetX) * (c % maxHorizontal)) + offsetX, -((sizeDelta.y + offsetY) * (Mathf.Ceil(c / maxHorizontal) ) ) - offsetX );
			
			go.GetComponent<RectTransform>().anchoredPosition = pos;
			iconsLibrary.Add(go);
			c++;

		}
	}

}
