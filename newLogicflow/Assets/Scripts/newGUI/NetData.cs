﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Netlist;

[Serializable()]
public class NetData : ISerializable {
	[SerializeField] protected Net _net;
	[SerializeField] protected DesignData _designData;
	[SerializeField] protected List<ConnectionPoint> _connectedPointList;

	[NonSerialized] protected List<TerminalItem> _connectedList;
	[NonSerialized] protected GameObject _gameObject;
	[NonSerialized] protected TerminalItem _mainTerminal;

	NetData() {}

	//loading NetData
	public NetData(SerializationInfo info, StreamingContext context) {
		net = (Net)info.GetValue ("Net", typeof(Net));
		designData = (DesignData)info.GetValue ("DesignData", typeof(DesignData));
		connectedPointList = (List<ConnectionPoint>)info.GetValue ("ConnectedPointList", typeof(List<ConnectionPoint>));
	}

	void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
		info.AddValue("Net", net, typeof(Net));
		info.AddValue("DesignData", designData, typeof(DesignData));
		info.AddValue("ConnectedPointList", connectedPointList, typeof(List<ConnectionPoint>));
	}
	public static NetData createNetData(DesignData designData, string name) {
		Net net = Net.createNet (designData.isDesign ? (Design) designData.design : TopDesign.instance.topDesign, name);
		NetData netData;
		if (!NetData.findNetData(designData, net, out netData)) {
			netData = new NetData();
			//netData.connectedList = new List<TerminalItem> ();
			netData.net = net;
			netData.designData = designData;
			//net.getConnectionPoints(TermType.IN | TermType.OUT).ForEach(x => netData.addTerminalItem(designData.findTerminalItem(x)));
			netData._gameObject = netData.gameObject;
			designData.netDataList.Add(netData);
		}
		return netData;
	}
	public Net net {
		get { return _net; }
		private set { _net = value; }
	}
	public DesignData designData {
		get { return _designData; }
		private set { _designData = value; }
	}
	public GameObject gameObject {
		get {
			if (_gameObject == null) {
				_gameObject = (GameObject) GameObject.Instantiate(Resources.Load ("newGUI/netItem"), Vector3.zero, Quaternion.identity);
				netItem.name = _net.getName();
				netItem.netData = this;
			}
			return _gameObject;
		}
		private set { _gameObject = value; }
	}

	public NetItem netItem {
		get { return gameObject.GetComponent<NetItem>();}
	}
	public List<TerminalItem> connectedList {
		get { 
			if (_connectedList == null) {
				_connectedList = new List<TerminalItem>();
				connectedPointList.ForEach( cp => {
					connectedList.Add(designData.findTerminalItem(cp));
				} );
			}
			return _connectedList;
		}
		private set { _connectedList = value; }
	}

	public List<ConnectionPoint> connectedPointList {
		get { 
			if (_connectedPointList == null)
				_connectedPointList = new List<ConnectionPoint>();
			return _connectedPointList;
		}
		private set { _connectedPointList = value; }
	}

	public TerminalItem mainTerminal {
		get {
			if (_mainTerminal == null) {
				_mainTerminal = connectedList.Find( t => (t.isTerminal && t.isTermTypeIn) || (t.isInstTerminal && t.isTermTypeOut));
				//connectedList.ForEach(x => {
				//	_mainTerminal = x.isTerminal && x.isTermTypeIn ? x : x.isInstTerminal && x.isTermTypeOut ? x : _mainTerminal;
				//});
			}
			return _mainTerminal;
		}
	}

	public bool addTerminalItem(TerminalItem term) {
		if (term != null) {
			//verifica se jah nao existe na lista
			if (!connectedList.Exists( t => t.Equals(term))) {
				//Debug.Log("ADICIONANDO terminal " + term.name + " a rede " + net.getName());
				net.connectAConnectionPoint(term.connectionPoint);
				term.netData = this;
				connectedList.Add(term);
				if (!connectedPointList.Exists( cp => cp.Equals(term.connectionPoint)))
					connectedPointList.Add(term.connectionPoint);
				if (!term.Equals(mainTerminal)) {
					//Debug.Log("Criando linha: mainterm: " + mainTerminal.name + " term: " + term.name);
					netItem.addLine(term);
				}
				return true;
			}
		}
		return false;
	}

	public bool disconnectTerminalItem(TerminalItem term) {
		bool retorno = true;
		if (term != null && term.isConnected()) {
			if (!term.Equals (mainTerminal)) {
				//Debug.Log ("REMOVENDO terminal " + term.name + " da rede " + net.getName ());
				term.netData = null;
				retorno = connectedList.Remove (term) ? retorno : false;
				retorno = connectedPointList.Remove (term.connectionPoint) ? retorno : false;
				retorno = net.disconnectConnectionPoint (term.connectionPoint) ? retorno : false;
				netItem.removeLine (term);
			} else {
				//Debug.Log ("REMOVENDO terminal principal " + term.name + " da rede " + net.getName ());
				disconnectAll ();

			}
		}
		return retorno;
	}
	public bool disconnectAll() {
		bool retorno = true;
		connectedList.ForEach (terminal => {
			if (!terminal.Equals(mainTerminal)) {
				terminal.netData = null;
				retorno = connectedPointList.Remove (terminal.connectionPoint) ? retorno : false;
				retorno = net.disconnectConnectionPoint (terminal.connectionPoint) ? retorno : false;
				netItem.removeLine (terminal);
			}
		});

		retorno = connectedPointList.Remove (mainTerminal.connectionPoint) ? retorno : false;
		retorno = net.disconnectConnectionPoint (mainTerminal.connectionPoint) ? retorno : false;
		retorno = designData.netDataList.Remove (this) ? retorno : false;
		designData = null;
		net = null;
		connectedList = new List<TerminalItem> ();
		connectedPointList = new List<ConnectionPoint> ();
		GameObject.Destroy (gameObject);
		return retorno;
	}
	public static bool findNetData(DesignData designData, Net net, out NetData netData) {
		netData = designData.netDataList.Find (x => x.net.Equals (net));
		if (netData != null)
			return true;
		return false;
	}
	public static void disconnectDesignData(DesignData des) {
		des.getTerminals ().ForEach (terminal => {
			if (terminal.isConnected() && terminal.netData != null)
				terminal.netData.disconnectTerminalItem(terminal);
		});
	}
	public GameObject resetGameObject() {
		//Debug.Log ("Acessando " + net.getName() + " limpando gameobject");
		_gameObject = null;
		_gameObject = gameObject;
		//Debug.Log ("Feito! " + gameObject.name);
		return gameObject;
	}
}
