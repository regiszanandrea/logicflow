﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menuScript : MonoBehaviour {

    public Canvas fileNameMenu;
    public InputField fileNameText;
		public Button fileNameOkText;
		public Button fileNameCancelText;

    // Use this for initialization
    void Start () {
      fileNameMenu = fileNameMenu.GetComponent<Canvas>();
			//fileNameMenu.enabled = false;
			fileNameText = fileNameText.GetComponent<InputField>();
			fileNameOkText = fileNameOkText.GetComponent<Button>();
			fileNameCancelText = fileNameCancelText.GetComponent<Button>();
    }

    public void okPress() {
			Debug.Log("Ok Pressionado");
		}
    public void cancelPress() {
			Debug.Log("Cancel Pressionado");
		}
}
