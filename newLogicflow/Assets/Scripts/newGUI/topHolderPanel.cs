﻿using UnityEngine;
using System.Collections;
using Netlist;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class topHolderPanel : MonoBehaviour, IPointerClickHandler {

	static public RectTransform topHolderRect;
	static public RectTransform Designs;
	static public RectTransform Nets;
	public Canvas guiCanvas;
	public RectTransform DesignsRect;
	public RectTransform NetsRect;

	public Text designName;
	public InputField fileNameInput;

	void Start () {
		if (TopDesign.instance.designData == null)
			TopDesign.instance.designData = new DesignData ("NovoProjeto", Vector2.zero);
		TopDesign.instance.designData.gameObject.SetActive (false);

		topHolderRect = GetComponent<RectTransform> ();
		Designs = DesignsRect.GetComponent<RectTransform> ();
		Nets = NetsRect.GetComponent<RectTransform> ();

		designName = designName.GetComponent<Text> ();
		fileNameInput = fileNameInput.GetComponent<InputField> ();

		designName.text = TopDesign.instance.designData.name;
		fileNameInput.text = TopDesign.instance.designData.name;

		TopDesign.instance.designData.designDataList.ForEach ( x=> {
			x.resetGameObject();
			x.gameObject.transform.SetParent (topHolderPanel.Designs);
			Vector3 scale = new Vector3 (1 / guiController._maxScale, 1 / guiController._maxScale, 1);
			x.gameObject.transform.localScale = scale;
			x.gameObject.transform.position = new Vector3 (x.position.x, x.position.y, 10);
			x.setLogicValueOnLoad();
		});

		TopDesign.instance.designData.netDataList.ForEach (x => {
			x.resetGameObject();
			x.gameObject.transform.SetParent(topHolderPanel.Nets);
			x.gameObject.transform.localScale = Vector3.one;
			x.gameObject.transform.localPosition = Vector3.zero;
			x.gameObject.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
			x.net.getConnectionPoints(TermType.IN | TermType.OUT).ForEach(z => {
				x.designData.findTerminalItem(z).netData = x;
			});
		});

		//TopDesign topDesignInstance = TopDesign.instance;
		//topDesignInstance.designData = new DesignData ("TopDesign", Vector2.zero, true);
	}
	void Update () {
		/**
		 * Clock Management
		 ** /
		TopDesign.instance.OnSync();
		if (TopDesign.simulate) {
			if (Time.fixedTime - TopDesign.instance.clockTimer > TopDesign.instance.clockDelay) {
				TopDesign.instance.clockHigh = TopDesign.instance.clockHigh.Equals(LogicValue.ONE) ? LogicValue.ZERO : LogicValue.ONE;
				TopDesign.instance.clockTimer = Time.fixedTime;
				TopDesign.instance.OnSync();
				TopDesign.instance.OnSimulate ();
			}
		}
		/**/
		//TopDesign.instance.designData.netDataList.ForEach( n => {
		//	TopDesign.instance.simulator.visit(n.net);
		//});
	}
	public void OnPointerClick(PointerEventData eventData) {
		//Debug.Log ("eventData.clickCount = " + eventData.clickCount + " eventData.button = " + eventData.button);
		if (eventData.clickCount == 1 && eventData.button.Equals(PointerEventData.InputButton.Left)) {
			Vector2 pos;
			//Vector2 screenPoint = (Input.touchCount > 0 ? (Vector2)Input.GetTouch (0).position : (Vector2)Input.mousePosition);
			Vector2 screenPoint = eventData.position;
			RectTransformUtility.ScreenPointToLocalPointInRectangle (guiCanvas.transform as RectTransform, screenPoint, guiCanvas.worldCamera, out pos);

			Vector3 position = guiCanvas.transform.TransformPoint (pos);

			if (TopDesign.selectedAction == "insert") {
				if (LibraryItem.selectedLibraryItem != null) {
				
					DesignData newDesign = TopDesign.instance.addItem (LibraryItem.selectedLibraryItem, new Vector2 (position.x, position.y));
				
					newDesign.gameObject.transform.SetParent (topHolderPanel.Designs);
					newDesign.gameObject.transform.localScale = new Vector3 (1 / guiController.instance.maxScale, 1 / guiController.instance.maxScale, 1);
					newDesign.gameObject.transform.position = new Vector3 (newDesign.position.x, newDesign.position.y, 10);
				
					LibraryItem.selectedLibraryItem.selected = false;
					LibraryItem.selectedLibraryItem = null;
					TopDesign.selectedAction = "";
				}
				Notifications.instance.notification("");
			}
		}
	}

}
