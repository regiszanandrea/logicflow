﻿using UnityEngine;
using System.Collections;
using Netlist;
using UnityEngine.UI.Extensions;

public class lineItem : MonoBehaviour {
	private TerminalItem _terminal;
	private TerminalItem _mainTerminal;
	public NetItem net;
	private UILineRenderer line;

	public TerminalItem mainTerminal {
		get { return _mainTerminal; }
		set { _mainTerminal = value; }
	}
	public TerminalItem terminal {
		get { return _terminal; }
		set { _terminal = value; }
	}

	void Start() {
		line = GetComponent<UILineRenderer>();
	}
	void Update() {
		if (mainTerminal != null && terminal != null) {
			lineCalc();
			line.color = TopDesign.instance.getLogicValue(net.netData.net).Equals(LogicValue.ONE) ? Color.blue : Color.grey;
		}
	}
	void lineCalc() {

		RectTransform mainRect = mainTerminal.transform.GetComponent<RectTransform>();
		RectTransform termRect = terminal.transform.GetComponent<RectTransform> ();

		RectTransform parentMainRect = mainRect.parent.GetComponent<RectTransform> ();
		RectTransform parentTermRect = termRect.parent.GetComponent<RectTransform> ();
		
		Vector2 offsetBegin = mainRect.anchoredPosition * parentMainRect.localScale.x;
		Vector2 offsetEnd = termRect.anchoredPosition * parentTermRect.localScale.x;
		
		Vector2 beginVec = parentMainRect.anchoredPosition + offsetBegin;
		Vector2 endVec =  parentTermRect.anchoredPosition + offsetEnd;
		
		RectTransform rect = GetComponent<RectTransform>();
		Vector2 distance = (beginVec - endVec);
		rect.sizeDelta = new Vector2(Mathf.Abs(distance.x), Mathf.Abs(distance.y));
		rect.anchoredPosition = beginVec - (distance * 0.5f);

		line.Points = new Vector2[4];
		float startX, endX, startY, endY;
		
		startX = beginVec.x < endVec.x ? 0 : rect.sizeDelta.x;
		endX = beginVec.x < endVec.x ? rect.sizeDelta.x : 0;
		startY = beginVec.y < endVec.y ? 0 : rect.sizeDelta.y;
		endY = beginVec.y < endVec.y ? rect.sizeDelta.y : 0;
		
		line.Points[0] = new Vector2(startX, startY);
		line.Points[1] = new Vector2(endX * 0.5f, startY);
		line.Points[2] = new Vector2(endX * 0.5f, endY);
		line.Points[3] = new Vector2(endX, endY);

	}
}
