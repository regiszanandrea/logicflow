﻿using UnityEngine;
using System.Collections.Generic;
using Netlist;
using UnityEngine.UI;
using System;

public class loadItemLibrary : MonoBehaviour {
	[NonSerialized]	public Design design = null;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (menuGuiController.loadDesign != null && menuGuiController.loadDesign.Equals (design)) {
			GetComponent<Image> ().color = Color.green;
		} else {
			GetComponent<Image> ().color = Color.white;
		}
	}
	public void OnClick() {
		if (menuGuiController.loadDesign != design) {
			menuGuiController.loadDesign = design;
			menuGuiController.loadFileLibrary = GetComponentInChildren<Text>().text;
		} else {
			menuGuiController.loadDesign = null;
			menuGuiController.loadFileLibrary = "";
		}
	}
}
