﻿using UnityEngine;
using System.Collections.Generic;
using Netlist;
using UnityEngine.UI.Extensions;

public class NetItem : MonoBehaviour {
	private NetData _netData;
	List<GameObject> lines;

	public NetData netData {
		get { return _netData; }
		set { _netData = value; }
	}

	void Awake () {
		lines = new List<GameObject> ();
	}
	void Start () {
		netData.connectedList.ForEach ( t => {
		    addLine(t);
		});
	}
	public void addLine(TerminalItem termItem) {
		if (netData.mainTerminal != null) {
			if (!termItem.Equals(netData.mainTerminal) && !lines.Exists( g => g.name.Equals(termItem.name))) {
				//Debug.Log("addLine " + termItem.name + " main: " + netData.mainTerminal.name);
				GameObject go = (GameObject)GameObject.Instantiate (Resources.Load ("newGUI/lineItem"));
				go.name = termItem.name;
				go.transform.SetParent (this.gameObject.transform);
				go.GetComponent<lineItem> ().net = this;
				go.GetComponent<lineItem> ().mainTerminal = netData.mainTerminal;
				go.GetComponent<lineItem> ().terminal = termItem;
		
				RectTransform rect = go.GetComponent<RectTransform> ();
				rect.anchorMax = Vector2.zero;
				rect.anchorMin = Vector2.zero;
				rect.localScale = Vector3.one;
		
				lines.Add (go);
			}
		}
	}
	public void removeLine(TerminalItem termItem) {
		if (termItem != null) {
			if (termItem.Equals(netData.mainTerminal)) {
				//se o terminal for o principal, remove todas linhas
				lines.ForEach( line => {
					lineItem lineItem = line.GetComponent<lineItem>();
					lineItem.mainTerminal = null;
					lineItem.terminal = null;
					Destroy(line);
				});
				lines.Clear();
			} else {
				GameObject go = lines.Find (t => t.GetComponent<lineItem> ().terminal.Equals (termItem));
				if (go != null){
					go.GetComponent<lineItem>().mainTerminal = null;
					go.GetComponent<lineItem>().terminal = null;
					lines.Remove (go);
					Destroy(go);
				}
			}
		}
	}

	void Update () {

	}
}
