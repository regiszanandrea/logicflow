﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Netlist;

/**
 * Singleton class that manages the Top Design, this is the editable design on the interface.
 **/
public class TopDesign {
	
	private static TopDesign _instance;
	public static TerminalItem terminalItemA = null, terminalItemB = null;
	private static Dictionary<string, DesignData> _customProjects;
	public static string selectedLibrary;
	public static string selectedAction;

	private DesignData _designData;
	private int id;
	private LogicSimulator _simulator;
	public static bool simulate = true;
	private Dictionary<string, Design> _library;
	private Dictionary<string, Design> _customLibrary;

	private TopDesign(){}

	public static TopDesign instance {
		get {
			if (_instance == null) {
				_instance = new TopDesign();
				_instance.id = 0;
				_instance._simulator = new LogicSimulator();
				//_instance._simulator.debug = true;
				_instance._library = new Dictionary<string, Design>();
				_instance.buildBasicLibrary();
				_instance._customLibrary = _instance.customLibrary;
			}
			return _instance;
		}
	}
	public static Dictionary<string, DesignData> customProjects {
		get {
			if (_customProjects == null) {
				_customProjects = new Dictionary<string, DesignData>();
				
				Serializer serializer = new Serializer (Serializer.projects);
				string myPath = serializer.fullPath;
				//Debug.Log("customProjects myPath = " + myPath);
				DirectoryInfo dir = new DirectoryInfo (myPath);
				FileInfo[] info = dir.GetFiles ("*.designData");
				foreach (FileInfo f in info) {
					DesignData data = null;
					data = serializer.DeSerializeDesignData (f.Name);
					if (data != null) {
						//Debug.Log("Carregando " + data.name);
						_customProjects.Add(data.name, data);

					}
				}
			}
			return _customProjects;
		}
	}
	public static void reloadCustomProjects() {
		_customProjects = null;
		_customProjects = customProjects;
	}
	public Design topDesign {
		get { return designData != null ? (Design) designData.design : null; }
	}
	public DesignData designData {
		get { return _designData; }
		set { _designData = value; }
	}
	public LogicSimulator simulator {
		get { return _simulator; }
	}
	public void reset(DesignData des) {
		instance._designData = des;
		Design designReset = (Design) des.design;
		instance.id = designReset.getInstances().Count + designReset.getNets().Count + designReset.getTerminals(TermType.IN | TermType.OUT).Count;
		instance._simulator = new LogicSimulator ();
		//instance._simulator.debug = true;
	}
	public void OnSimulate() {
		if (simulate)
			_simulator.visit(topDesign);
	}
	public void SimulateDesign(Design des) {
		if (simulate)
			_simulator.visit(des);
	}
	public Dictionary<string, Design> library {
		get { return _library;}
	}
	public LogicValue getLogicValue(DesignData dd){
		return simulator.getValue (dd.design);
	}
	public LogicValue getLogicValue(DesignObj designObj){
		return simulator.getValue (designObj);
	}
	public void setLogicValue(DesignData dd, LogicValue logicValue) {
		simulator.setValue (dd.design, logicValue);
	}
	public void setLogicValue(DesignObj des, LogicValue logicValue) {
		simulator.setValue (des, logicValue);
	}
	/**
	 * Registra o id e retorna o nome padronizado para o design
	 * Instance: I<id>.<name>
	 *  ex: I1.AND
	 *      I2.Adder *custom design*
	 * Terminal: T<id>.<name>
	 *  ex: T3.Switch
	 *      T4_Led
	 * Net: N<id>.name ou N<id>
	 *  ex: N5.I1AND.
	 *      N6
	 **/
	public string registerName(string name, string type) {
		string retorno;
		id++;
		retorno = type + id;
		if (name != "")
			retorno += "." + name;
		return retorno;
	}
	
	//cria celulas basicas que serao usadas para instanciar os elementos.
	public Design createBasicCell(string name, int n_inputs) {
		Cell cell = new Cell (name);
		Design dcell = Design.createCellDesign (cell);
		for(int i = 0; i < n_inputs; i++) {
			Terminal.createTerminal(dcell, "I"+i,TermType.IN);
		}
		Terminal.createTerminal(dcell, "O0", TermType.OUT);
		return dcell;
	}

	public DesignData addItem(LibraryItem libraryItem, Vector2 position) {
		DesignData newDesign = null;
		switch (libraryItem.tipo) {
			case DesignType.INSTANCE:
				newDesign = new DesignData(libraryItem.name, libraryItem.n_inputs, position, libraryItem.logicValue);
				break;
			case DesignType.TERMINAL:
				if (libraryItem.name == "CLOCK")
					newDesign = new DesignData(libraryItem.name, position, libraryItem.termType, libraryItem.clock);
				else
					newDesign = new DesignData(libraryItem.name, position, libraryItem.termType, libraryItem.logicValue);
				break;
		}
		if (newDesign != null)
			designData.designDataList.Add (newDesign);
		return newDesign;
	}
	public bool removeItem(DesignData item) {
		bool retorno = true;
		if (designData.designDataList.Exists(x => x.Equals(item))) {
			NetData.disconnectDesignData(item);
			Design des = (Design)designData.design;
			if (item.isInstance)
				retorno = des.removeInstance((Instance)item.design) ? retorno : false;
			else
				retorno = des.removeTerminal((Terminal)item.design) ? retorno : false;
			designData.designDataList.Remove(item);
		} else {
			Debug.Log(item.name + " nao encontrado :( ");
		}
		return retorno;
	}
	public void connectTerminalItem(TerminalItem tItem = null) {
		//Debug.Log ("connectTerminalItem " + tItem.name );
		if (tItem.Equals (terminalItemA)) {
			terminalItemA = null;
			return;
		}
		if (tItem.Equals (terminalItemB)) {
			terminalItemB = null;
			return;
		}
		//procura por uma porta livre, colocando ela de acordo -- esse algoritmo ja posiciona o item correto nos terminais A ou B
		if (terminalItemA == null && terminalItemB == null) {
			terminalItemA = tItem;
		} else if (terminalItemA == null) {
			TerminalItem novoA, novoB;
			if (TopDesign.validTerminalConnection(tItem, terminalItemB, out novoA, out novoB)) {
				terminalItemA = novoA;
				terminalItemB = novoB;
			} else {
				terminalItemB = tItem;
			}
				
		} else if (terminalItemB == null) {
			TerminalItem novoA, novoB;
			if (TopDesign.validTerminalConnection(terminalItemA, tItem, out novoA, out novoB)) {
				terminalItemA = novoA;
				terminalItemB = novoB;
			} else {
				terminalItemA = tItem;
			}

		}

		//se ambas as conexoes existirem, faz a conexao entre as portas
		if (terminalItemA && terminalItemB) {
			NetData netData;
			if (terminalItemA.isConnected()) {
				netData = terminalItemA.netData;
			} else {
				netData = NetData.createNetData(instance.designData, registerName("", "N"));
				netData.addTerminalItem(terminalItemA);
			}
			netData.addTerminalItem(terminalItemB);

			//seta o conteiner pai e reseta rect para ocupar todo o topHolderPanel
			netData.gameObject.transform.SetParent(topHolderPanel.Nets);
			netData.gameObject.transform.localScale = Vector3.one;
			netData.gameObject.transform.localPosition = Vector3.zero;
			netData.gameObject.GetComponent<RectTransform>().sizeDelta = Vector2.zero;

			//instance.designData.netDataList.Add(netData);

			terminalItemA = null;
			terminalItemB = null;

			OnSimulate ();
		}
	}
	//retorna verdadeiro se a conexao entra o conector a e b sao validas
	static public bool validConnectionPoint(ConnectionPoint a, ConnectionPoint b) {
		
		if ((isInstTerminal (a) && isInstTerminal (b)) || (isTerminal (a) && isTerminal (b))) {
			if (sameTermType (a, b))
				return false;
			return true;
		} else {
			if (sameTermType (a, b))
				return true;
			return false;
		}
	}
	public static bool validTerminalConnection(TerminalItem a, TerminalItem b, out TerminalItem tFirst, out TerminalItem tSecond) {

		//Terminal(IN) - Terminal(OUT)
		//InstTerm(OUT) - InstTerm(IN)

		//InstTerm(OUT) - Terminal(OUT)
		//Terminal(IN) - InstTerm(IN)
		tFirst = a;
		tSecond = b;
		//Debug.Log ("validTerminalConnection = " + a.name + ", " + b.name);
		if (a.parentDesignData.Equals(b.parentDesignData))
			return false;

		if (a.isTerminal && b.isTerminal) {
			if ((a.isTermTypeIn && b.isTermTypeIn) || (a.isTermTypeOut && b.isTermTypeOut)) {
				//Debug.Log ("validTerminalConnection A");
				return false;
			}
			tFirst = a.isTermTypeIn ? a : b;
			tSecond = a.isTermTypeIn ? b : a;

		} else if (a.isInstTerminal && b.isInstTerminal) {
			if ((a.isTermTypeIn && b.isTermTypeIn) || (a.isTermTypeOut && b.isTermTypeOut)) {
				//Debug.Log ("validTerminalConnection B");
				return false;
			}
			tFirst = a.isTermTypeOut ? a : b;
			tSecond = a.isTermTypeOut ? b : a;
		} else {
			if ((a.isTermTypeIn && b.isTermTypeOut) || (a.isTermTypeOut && b.isTermTypeIn)) {
				//Debug.Log ("a.isTermTypeIn && b.isTermTypeIn = " + a.isTermTypeIn + "&&" + b.isTermTypeIn);
				//Debug.Log ("a.isTermTypeOut && b.isTermTypeOut = " + a.isTermTypeOut + "&&" + b.isTermTypeOut);
				return false;
			}
			tFirst = ((a.isInstTerminal && a.isTermTypeOut) || (a.isTerminal && a.isTermTypeIn)) ? a : b;
			tSecond = ((a.isInstTerminal && a.isTermTypeOut) || (a.isTerminal && a.isTermTypeIn)) ? b : a;
		}
		return true;

	}

	private void buildBasicLibrary() {
		for (int i = 2; i <= 8; i++) {
			library.Add("AND" + i, createBasicCell("AND",i));
			library.Add("OR" + i, createBasicCell("OR",i));
			library.Add("XOR" + i, createBasicCell("XOR",i));
			library.Add("NAND" + i, createBasicCell("NAND",i));
			library.Add("NOR" + i, createBasicCell("NOR",i));
			library.Add("XNOR" + i, createBasicCell("XNOR",i));
		}
		library.Add("INV" + "1", createBasicCell("INV",1));

	}
	public Dictionary<string, Design> customLibrary {
		get {
			if (_customLibrary == null) {
				_customLibrary = new Dictionary<string, Design>();
				
				Serializer serializer = new Serializer (Serializer.library);
				string myPath = serializer.fullPath;
				
				DirectoryInfo dir = new DirectoryInfo (myPath);
				FileInfo[] info = dir.GetFiles ("*.design");
				foreach (FileInfo f in info) {
					Design data = serializer.DeSerializeDesign(f.Name);
					_customLibrary.Add(data.getName(), data);
				}
			}
			return _customLibrary;
		}

	}
	public void rebuildCustomLibrary() {
		_customLibrary = null;
		_customLibrary = customLibrary;
	}

	static public bool isInstance(DesignObj _design) {
		return _design.GetType ().Name.Equals ("Instance");
	}
	static public bool isNet(DesignObj _design) {
		return _design.GetType ().Name.Equals ("Net");
	}
	static public bool isTerminal(DesignObj _design) {
		return _design.GetType ().Name.Equals ("Terminal");
	}
	static public bool isInstTerminal(DesignObj _design) {
		return _design.GetType ().Name.Equals ("InstTerminal");
	}
	static public bool isTermTypeIn(ConnectionPoint cp) {
		if (isInstTerminal (cp)) {
			InstTerminal instterm = (InstTerminal)cp;
			return instterm.getTerminal ().getTerminalType ().Equals(TermType.IN);
		} else {
			Terminal term = (Terminal)cp;
			return term.getTerminalType ().Equals(TermType.IN);
		}
	}
	static public bool isTermTypeOut(ConnectionPoint cp) {
		if (isInstTerminal (cp)) {
			InstTerminal instterm = (InstTerminal)cp;
			return instterm.getTerminal ().getTerminalType ().Equals(TermType.OUT);
		} else {
			Terminal term = (Terminal)cp;
			return term.getTerminalType ().Equals(TermType.OUT);
		}
	}
	static public bool sameTermType(ConnectionPoint a, ConnectionPoint b) {
		return (isTermTypeIn(a) && isTermTypeIn(b)) || (isTermTypeOut(a) && isTermTypeOut(b)) ;
	}
}