﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netlist;
using UnityEngine.UI;

public class TerminalItem : MonoBehaviour {
	private ConnectionPoint _connectionPoint = null;
	private DesignData _parentDesignData;
	private NetData _netData;
	public Button pin;
	public int pinId;
	public int totalPins;

	Image image;
	
	public ConnectionPoint connectionPoint {
		get { return _connectionPoint; }
		set { _connectionPoint = value;}
	}
	public InstTerminal instTerminal {
		get {
			if (isInstTerminal) return (InstTerminal) connectionPoint;
			return null;
		}
	}
	public Terminal terminal {
		get {
			if (isTerminal) return (Terminal) connectionPoint;
			return null;
		}
	}
	public DesignData parentDesignData {
		get { return _parentDesignData; }
		set { _parentDesignData = value; }
	}
	public NetData netData {
		get { return _netData; }
		set { _netData = value; }
	}
	void Awake () {
		tag = "TerminalItem";
		image = GetComponent<Image> ();
		pin = GetComponentInChildren<Button> ();
	}
	void Start () {
		RectTransform rect = GetComponent<RectTransform> ();

		if (isInstTerminal) {
			if (isTermTypeOut) {
				//inverte o lado do pino
				pin.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (rect.sizeDelta.x, 0);
			} else {
				pin.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-rect.sizeDelta.x, 0);
			}
		} else if (isTerminal) {
			if (isTermTypeOut) {
				pin.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (-rect.sizeDelta.x, 0);
			} else {
				pin.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (rect.sizeDelta.x, 0);
			}
		}
		if (_parentDesignData.designName.Equals("LIGHT")) {
			rect.anchoredPosition = new Vector2(0, -35f);
			rect.sizeDelta = new Vector2(5f, 35f);
			pin.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -rect.sizeDelta.y);
		}
	}

	// Update is called once per frame
	void Update () {
		if (this.Equals (TopDesign.terminalItemA) || this.Equals (TopDesign.terminalItemB)) {
			image.color = Color.yellow;
		} else {
			if (((isInstTerminal && isTermTypeIn) || (isTerminal && isTermTypeOut)) && isConnected())
				image.color = Color.red;
			else
				image.color = Color.green;
			if (TopDesign.terminalItemA != null) {
				if (!TopDesign.validConnectionPoint(TopDesign.terminalItemA.connectionPoint, connectionPoint)) {
					image.color = Color.red;
				}
			}
			if (TopDesign.terminalItemB != null) {
				if (!TopDesign.validConnectionPoint(TopDesign.terminalItemB.connectionPoint, connectionPoint)) {
					image.color = Color.red;
				}
			}
		}
	}
	public void OnClick() {
		if (TopDesign.selectedAction == "") {
			if (!isConnected () || ((isTerminal && isTermTypeIn) || (isInstTerminal && isTermTypeOut))) {
				TopDesign.instance.connectTerminalItem (this);
			}
		} else if (TopDesign.selectedAction == "delete") {
			if (netData != null)
				netData.disconnectTerminalItem(this);

			Notifications.instance.notification("");
			TopDesign.selectedAction = "";
		}

	}
	public bool isConnected() { return isTerminal ? terminal.isConnected () : instTerminal.isConnected (); }
	public bool isTerminal { get { return _connectionPoint.GetType ().Name.Equals ("Terminal"); } }
	public bool isInstTerminal { get { return _connectionPoint.GetType ().Name.Equals ("InstTerminal"); } }
	public bool isTermTypeIn { get { return isInstTerminal ? instTerminal.getTerminal().getTerminalType().Equals(TermType.IN) : isTerminal ? terminal.getTerminalType().Equals(TermType.IN) : false; } }
	public bool isTermTypeOut { get { return isInstTerminal ? instTerminal.getTerminal().getTerminalType().Equals(TermType.OUT) : isTerminal ? terminal.getTerminalType().Equals(TermType.OUT) : false; } }
}
