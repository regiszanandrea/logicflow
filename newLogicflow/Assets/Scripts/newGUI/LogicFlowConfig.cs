﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

[XmlRoot("LogicFlowConfig")]
public class LogicFlowConfig {

	[XmlAttribute("Version")]
	public int Version = 1;
	[XmlAttribute("Minor")]
	public int Minor = 1;
	[XmlAttribute("Day")]
	public int Day = 25;
	[XmlAttribute("Month")]
	public int Month = 11;
	[XmlAttribute("Year")]
	public int Year = 2015;

	public string fullVersion {
		get {
			return Version + "." + Minor + "." + Year + Month + Day;
		}
	}

	public void Save(string path)
	{
		var serializer = new XmlSerializer(typeof(LogicFlowConfig));
		using(var stream = new FileStream(path, FileMode.Create))
		{
			serializer.Serialize(stream, this);
		}
	}
	
	public static LogicFlowConfig Load(string path)
	{
		var serializer = new XmlSerializer(typeof(LogicFlowConfig));
		using(var stream = new FileStream(path, FileMode.Open))
		{
			return serializer.Deserialize(stream) as LogicFlowConfig;
		}
	}
	
	//Loads the xml directly from the given string. Useful in combination with www.text.
	public static LogicFlowConfig LoadFromText(string text) 
	{
		var serializer = new XmlSerializer(typeof(LogicFlowConfig));
		return serializer.Deserialize(new StringReader(text)) as LogicFlowConfig;
	}
	public static void RemoveAllFiles() {
		DirectoryInfo dir = new DirectoryInfo (Serializer.projects);
		FileInfo[] info = dir.GetFiles ("*.*");
		foreach (FileInfo f in info)
			f.Delete();
		dir = new DirectoryInfo (Serializer.library);
		info = dir.GetFiles ("*.*");
		foreach (FileInfo f in info)
			f.Delete();
	}
}
