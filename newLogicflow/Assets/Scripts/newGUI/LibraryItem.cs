using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Netlist;

public enum DesignType {
	DESIGN,
	TERMINAL,
	INSTANCE,
	INSTTERMINAL,
	NET
};

public class LibraryItem : MonoBehaviour {
	public bool selected = false;
	public static LibraryItem selectedLibraryItem;

	public DesignType tipo;
	public TermType termType;
	public int n_inputs;
	public LogicValue logicValue;
	public int clock;
	public bool custom = true;

	Image image;

	void Awake () {

		image = GetComponent<Image> ();
		/** feito direto no editor
		n_inputs = 2;
		logicValue = LogicValue.ZERO;
		switch (name) {
			case "AND":
				break;
			case "OR":
				break;
			case "XOR":
				break;
			case "INV":
				n_inputs = 1;
				break;
			case "NAND":
				break;
			case "NOR":
				break;
			case "XNOR":
				break;
			case "SWITCH":
				termType = TermType.IN;
				n_inputs = 1;
				break;
			case "CLOCK":
				termType = TermType.IN;
				n_inputs = 1;
				break;
			case "ONE":
				termType = TermType.IN;
				logicValue = LogicValue.ONE;
				n_inputs = 1;
				break;
			case "ZERO":
				termType = TermType.IN;
				n_inputs = 1;
				break;
			case "LIGHT":
				termType = TermType.OUT;
				n_inputs = 1;
				break;
		}
		**/

	}
	void Update (){
		image.color = selected ? Color.green : Color.white;
	}

	public void OnClick() {
		string action = "insert";
		//permite a selecao do item apenas quando nenhuma outra acao estiver selecionada ou quando a acao selecionada for inserir deseleciona o item

		//TopDesign.selectedAction = TopDesign.selectedAction != action ? action : "";
		if (TopDesign.selectedAction != action) {
			Notifications.instance.notification(action);
			TopDesign.selectedAction = action;
			selected = true;
			LibraryItem.selectedLibraryItem = this;
		} else {
			if (selected) {
				selected = false;
				TopDesign.selectedAction = "";
				Notifications.instance.notification("");
				LibraryItem.selectedLibraryItem = null;
			} else {
				if (LibraryItem.selectedLibraryItem != null)
					LibraryItem.selectedLibraryItem.selected = false;
				selected = true;
				LibraryItem.selectedLibraryItem = this;
			}
		}
	}
}
