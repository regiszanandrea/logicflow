using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netlist;
using UnityEngine.UI;

public class instanceItem : MonoBehaviour {

	public static Dictionary<string, Sprite> spriteAtlas {
		get {
			if (_spriteAtlas == null) {
				_spriteAtlas = new Dictionary<string, Sprite>();

				Image[] images = Resources.LoadAll<Image> ("newGUI/Instances");
				foreach (Image img in images) {
					//Debug.Log("img Loaded: " + img.name );
					instanceItem._spriteAtlas.Add(img.name, img.sprite);
				}
			}
			return _spriteAtlas;
		}
	}

	private static Dictionary<string, Sprite> _spriteAtlas;

	private DesignData _designData = null;
	//private TopDesign topDesigner = TopDesign.instance;
	public List<GameObject> terminals = new List<GameObject>();
	private bool dragging = false;
	public bool selected = false;
	Vector2 currentMousePosition = Vector2.zero;
	Vector3 deltaMousePosition = Vector2.zero;
	public float pinOffsetXIN = -35, pinOffsetXOUT = 35;

	public float clockTimer = 0f;
	public float clockDelay = 1.5f;

	//private bool reseted = false;

	//public Shadow shadow;

	private Vector2 lastMousePosition;

	private Image image;
	// Use this for initialization
	void Awake () {
		tag = "InstanceItem";
		image = GetComponent<Image> ();
	}
	void Start() {
		updateTextures ();
		clockDelay = (designData.clockTimer / 1000);
		//designData.logicValue = TopDesign.instance.getLogicValue(designData);
	}
	void Update () {

		currentMousePosition = Input.mousePosition;
		deltaMousePosition = currentMousePosition - lastMousePosition;
		lastMousePosition = Input.mousePosition;

		if (dragging)
			OnMovement ();

		if (designData != null) {
			designData.position = transform.position;
		}
		if (designData.designName.Equals("CLOCK"))
			updateClock();
		updateTextures ();
	}

	private void updateClock() {
		if (TopDesign.simulate) {
			if (Time.fixedTime - clockTimer > clockDelay) {
				designData.logicValue = TopDesign.instance.getLogicValue(designData).Equals(LogicValue.ONE) ? LogicValue.ZERO : LogicValue.ONE ;
				clockTimer = Time.fixedTime;
				TopDesign.instance.OnSimulate();
			}
		}
	}

	private void OnMovement() {
		transform.position += deltaMousePosition * Time.fixedDeltaTime * 10f;
	}
	public DesignData designData {
		get { return _designData;}
		set { 
			_designData = value;
			CreateConnectionPins();
		}
	}

	public void CreateConnectionPins() {
		//Debug.Log ("Criando pinos");
		foreach (GameObject go in terminals)
			Destroy (go);
		terminals = new List<GameObject> ();

		if (_designData.isInstance) {
			Instance inst = (Instance) _designData.design;
			List<InstTerminal> inTerms = inst.getInstTerminals(TermType.IN);
			int i = inTerms.Count;

			foreach(InstTerminal inTerm in inTerms) {
				//Debug.Log("interm = " + inTerm.getName());
				GameObject inClone = createPinClone(inTerm, pinOffsetXIN, i, inTerms.Count);
				terminals.Add(inClone);
				i--;
			}
			//out connection
			List<InstTerminal> outTerms = inst.getInstTerminals(TermType.OUT);
			i = outTerms.Count;
			foreach(InstTerminal outTerm in outTerms) {
				//Debug.Log("outTerm = " + outTerm.getName());
				GameObject outClone = createPinClone(outTerm, pinOffsetXOUT, i, outTerms.Count);
				terminals.Add(outClone);
				i--;
			}

		} else if (_designData.isTerminal) {
			Terminal term = (Terminal) _designData.design;
			float offset = term.getTerminalType().Equals(TermType.IN) ? pinOffsetXOUT : pinOffsetXIN;
			GameObject clone = createPinClone(term, offset, 1, 1);
			terminals.Add(clone);
		}
	}
	private GameObject createPinClone(ConnectionPoint cp, float offset, int counter, int totalCounter) {

		RectTransform rectParent = GetComponent<RectTransform>();

		GameObject clone = (GameObject) GameObject.Instantiate(Resources.Load("newGUI/TerminalItem"));
		clone.name = cp.getName();
		clone.GetComponent<TerminalItem> ().connectionPoint = cp;
		clone.GetComponent<TerminalItem> ().parentDesignData = designData;
		clone.GetComponent<TerminalItem> ().pinId = counter;
		clone.GetComponent<TerminalItem> ().totalPins = totalCounter;

		clone.transform.SetParent(this.transform);
		clone.transform.position = new Vector3 (clone.transform.position.x, clone.transform.position.y, this.transform.position.z + 1);
		clone.transform.localScale = new Vector3(1,1,1);
		RectTransform rect = clone.GetComponent<RectTransform>();
		float newY;
		//calcula a posicao de cada terminal, deus sabe como cheguei a essa matematica...
		float maxY = rectParent.sizeDelta.y / 2 * totalCounter / 2; //calcula um tamanho maximo que cada terminal pode ocupar
		newY = maxY - (6 * ( 6 * (totalCounter - counter)+3)); //calcula o offset relativo a posicao maxima, com uma matematica meio obscura, mas oq importa que funciona
		//in connect
		rect.anchoredPosition = new Vector2(offset, newY);

		return clone;
	}

	private void updateTextures() {
		//newGUI/Textures/
		//Debug.Log (designData.designName);

		if (designData.designName.Equals ("LIGHT")) {
			if (designData.logicValue.Equals (LogicValue.ONE))
				image.sprite = instanceItem.spriteAtlas ["LIGHT_ON"];
			else
				image.sprite = instanceItem.spriteAtlas ["LIGHT_OFF"];
		} else if (designData.designName.Equals("SWITCH")) {
			if (designData.logicValue.Equals (LogicValue.ONE))
				image.sprite = instanceItem.spriteAtlas ["SWITCH_ON"];
			else
				image.sprite = instanceItem.spriteAtlas ["SWITCH_OFF"];
		} else if (designData.designName.Equals("CLOCK")) {
			if (designData.logicValue.Equals (LogicValue.ONE))
				image.sprite = instanceItem.spriteAtlas ["CLOCK_ON"];
			else
				image.sprite = instanceItem.spriteAtlas ["CLOCK_OFF"];
		} else {
			if (instanceItem.spriteAtlas.ContainsKey(designData.designName))
				image.sprite = instanceItem.spriteAtlas [designData.designName];
			else {
				GetComponentInChildren<Text>().enabled = true;
				GetComponentInChildren<Text>().text = designData.designName;
				image.sprite = instanceItem.spriteAtlas ["CUSTOM"];
			}
		}
	}

	public void OnClick() {
		if (!dragging) {
			if (TopDesign.selectedAction == "") {
				if (designData.designName.Equals("SWITCH")) {
					designData.logicValue = designData.logicValue.Equals(LogicValue.ONE) ? LogicValue.ZERO : LogicValue.ONE;
					TopDesign.instance.OnSimulate();
				}
			} else if (TopDesign.selectedAction == "delete") {
				TopDesign.instance.removeItem(designData);
				Destroy(this.gameObject);
				TopDesign.selectedAction = "";
				Notifications.instance.notification("");
			}
		}
	}

	public void OnDragBegin() {
		dragging = true;
	}
	public void OnDragEnd() {
		dragging = false;
	}
	public void OnDrag() {
		//transform.position = Input.mousePosition;
	}
}
