﻿using UnityEngine;
using System.Collections.Generic;
using Netlist;
using UnityEngine.UI;
using System;

public class loadItem : MonoBehaviour {
	[NonSerialized]	public DesignData designData = null;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (menuGuiController.loadDesignData != null && menuGuiController.loadDesignData.Equals (designData)) {
			GetComponent<Image> ().color = Color.green;
		} else {
			GetComponent<Image> ().color = Color.white;
		}
	}
	public void OnClick() {
		if (menuGuiController.loadDesignData != designData) {
			menuGuiController.loadDesignData = designData;
			menuGuiController.loadFileName = GetComponentInChildren<Text>().text;
		} else {
			menuGuiController.loadDesignData = null;
			menuGuiController.loadFileName = "";
		}
	}
}
