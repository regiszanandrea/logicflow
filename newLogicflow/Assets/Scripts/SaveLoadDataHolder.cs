﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Netlist;

[Serializable]
public class Pos2 {
	[SerializeField] public float x;
	[SerializeField] public float y;

	public Pos2(float x, float y) {
		this.x = x;
		this.y = y;
	}
}

[Serializable()]
public class SaveLoadDataHolder : ISerializable {

	[SerializeField] private List<string> library;
	[SerializeField] private List<Pos2> pos;

	public SaveLoadDataHolder() {
		library = new List<string>();
		pos = new List<Pos2>();
	}
	public SaveLoadDataHolder(SerializationInfo info, StreamingContext context) {
		library = (List<string>) info.GetValue("Library", typeof(List<string>));
		pos = (List<Pos2>)info.GetValue ("Pos", typeof(List<Pos2>));

	}
	void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
		info.AddValue("Library", library, typeof(string));
		info.AddValue("Pos", pos, typeof(Pos2));
	}
	public void AddItem(string nameComponent, Vector2 posComponent) {
		library.Add (nameComponent);
		pos.Add (new Pos2(posComponent.x, posComponent.y));
	}
	public string GetLibrary(int position) {
		return library [position];
	}
	public Vector2 GetPosition(int position) {
		return new Vector2(pos [position].x, pos [position].y);
	}
	public int CountItem {
		get {return library.Count;}
	}
	public void RemoveItem(int position) {
		library.RemoveAt (position);
		pos.RemoveAt (position);
	}
	public int SearchItem() {
		return 0;
	}
}
