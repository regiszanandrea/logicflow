﻿using UnityEngine;
using System.Collections;

public class InterfaceHolder : MonoBehaviour {
	Bounds bounds;
	public Vector2 padding;
	// Use this for initialization
	void Start () {
		padding = new Vector2 (-0.65f,0.05f);
		bounds = TopHolder.OrthographicBounds();
	}
	
	// Update is called once per frame
	void Update () {
		bounds = TopHolder.OrthographicBounds();
		Vector3 pos = new Vector3 (bounds.max.x - (transform.localScale.x / 2) + padding.x, bounds.max.y - (transform.localScale.y / 2) + padding.y, transform.position.z);
		transform.position = pos;
		
	}
}
