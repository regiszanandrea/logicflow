﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

using Netlist;

public enum BasicTypes {
	NET,
	INSTANCE,
	TERMINAL,
	INSTTERMINAL,
	DESIGN
}

public class DesignHolder {

	private Design topDesign;

	private int id;
	private LogicSimulator simulator;
	public bool simulateEnabled = true;

	private Dictionary<string, Design> _library;
	public Dictionary<string, Design> library {
		get { return _library;}
		set { _library = value;}
	}
	private DesignHolder() {}

	private static DesignHolder _instance;
	public static DesignHolder instance {
		get {
			if (_instance == null) {
				_instance = new DesignHolder();
				_instance.id = 0;
				_instance.simulator = new LogicSimulator();
				_instance.simulateEnabled = true;
				_instance.library = new Dictionary<string, Design>();
				_instance.createLibrary();
			}
			return _instance;
		}
	}
	public void reset(Design design) {
		instance.topDesign = design;
		instance.id = design.getInstances().Count + design.getNets().Count + design.getTerminals(TermType.IN | TermType.OUT).Count;
		instance.simulator = new LogicSimulator ();
		instance.simulateEnabled = true;
	}
	public void setTopDesign(Design design) {
		topDesign = design;
	}
	
	public Design getTopDesign() {
		return topDesign;
	}
	public void simulate() {
		if (simulateEnabled)
			simulator.visit (topDesign);
	}
	public void setValue(DesignObj obj, LogicValue value) {
		simulator.setValue(obj, value);
	}
	
	public LogicValue getValue(DesignObj obj) {
		return simulator.getValue(obj);
	}

	public Design createBaseCell(string name, int n_inputs) {
		Cell cell = new Cell (name);
		Design dcell = Design.createCellDesign (cell);
		for(int i = 0; i < n_inputs; i++) {
			Terminal.createTerminal(dcell, "I"+i,TermType.IN);
		}
		Terminal.createTerminal(dcell, "O0", TermType.OUT);
		return dcell;
	}
	public string createName(string name) {
		id++;
		return name + id;
	}
	public Instance createInstance(string cellname, int n_inputs) {
		Design cell = library [cellname];
		Instance inst = Instance.createInstance (cell, topDesign, createName ("I") + "_" + cellname + n_inputs);
		Terminal term;
		for (int i = 0; i < n_inputs; i++) {
			if (Terminal.find(cell, "I"+i, out term))
				InstTerminal.createInstTerminal(inst, term);
		}
		if (Terminal.find(cell, "O0", out term))
			InstTerminal.createInstTerminal(inst, term);		
		return inst;

	}
	public Terminal createInput(string name, LogicValue value = LogicValue.ZERO) {
		Terminal retorno = Terminal.createTerminal (topDesign, createName (name), TermType.IN, name);
		setValue (retorno, value);
		return retorno;
	}
	
	public Terminal createOutput(string name) {
		return Terminal.createTerminal(topDesign, createName(name), TermType.OUT);
	}
	public InstTerminal getInstTerminal(Instance inst, TermType termType, int i = 0 ){
		List<InstTerminal> instterminals = inst.getInstTerminals (termType);
		return instterminals[i];
	}

	public static TermType termType(DesignObj _design) {
		if (basicType(_design) == BasicTypes.TERMINAL) {
			Terminal term = (Terminal) _design;
			return term.getTerminalType();
		}
		if (basicType(_design) == BasicTypes.INSTTERMINAL) {
			InstTerminal instterm = (InstTerminal) _design;
			return instterm.getTerminal().getTerminalType();
		}
		return TermType.INOUT;
	}
	public Net disconnectPointFromNet(ConnectionPoint _point) {
		Net _net = null;
		InstTerminal instterm;
		Terminal term;
		if (_point != null) {
			if (basicType(_point) == BasicTypes.INSTTERMINAL) {
				instterm = (InstTerminal) _point;
				_net = instterm.getNet();
				if (instterm.isConnected()){
					if (termType(_point) == TermType.OUT) {
						disconnectAllFromNet(_net);
					} else
						_net.disconnectInstTerminal((InstTerminal) _point);
				}
			} else if (basicType(_point) == BasicTypes.TERMINAL) {
				term = (Terminal) _point;
				_net = term.getNet();
				if (term.isConnected()) {
					if (termType(_point) == TermType.IN) {
						disconnectAllFromNet(_net);
					} else
						_net.disconnectTerminal((Terminal) _point);
				}
			}
		}
		//DesignHolder.instance.simulate ();
		return _net;
	}
	public void disconnectAllFromNet(Net _net) {
		if (_net != null) {
			foreach(ConnectionPoint cp in _net.getConnectionPoints(TermType.IN | TermType.OUT)) {
				if (basicType(cp) == BasicTypes.INSTTERMINAL) {
					_net.disconnectInstTerminal((InstTerminal) cp);
				} else if (basicType(cp) == BasicTypes.TERMINAL) {
					_net.disconnectTerminal((Terminal) cp);
				}
			}
		}
	}
	public static BasicTypes basicType(DesignObj _design) {
		if (_design.GetType().Name.Equals("Net")) {
			return BasicTypes.NET;
		} else if (_design.GetType().Name.Equals("Instance")) {
			return BasicTypes.INSTANCE;
		} else if (_design.GetType().Name.Equals("Terminal")) {
			return BasicTypes.TERMINAL;
		} else if (_design.GetType().Name.Equals("InstTerminal")) {
			return BasicTypes.INSTTERMINAL;
		} else {
			return BasicTypes.DESIGN;
		}
	}


	public void createConnection(DesignObj source, int iSource, DesignObj target, int iTarget) {
		Net net;
		if (source.GetType().Name.Equals("Instance")) {
			InstTerminal instterm;
			InstTerminal.find((Instance) source, source.getName()+".O"+iSource, out instterm);
			if (instterm.isConnected())
				net = instterm.getNet();
			else
				net = Net.createNet(topDesign, createName("n"));
			net.connectInstTerminal(instterm);
		} else {
			Terminal term = (Terminal) source;
			if (term.isConnected())
				net = term.getNet();
			else
				net = Net.createNet(topDesign, createName("n"));
			net.connectTerminal(term);
		}
		if(target.GetType().Name.Equals("Instance")) {
			InstTerminal instterm;
			InstTerminal.find((Instance) target, target.getName()+".I"+iTarget, out instterm);
			net.connectInstTerminal(instterm);
		} else {
			net.connectTerminal((Terminal) target);
		}
	}
	public Net createConnectionCP(ConnectionPoint a, ConnectionPoint b) {
		Net net = null;
		InstTerminal insttermIN, insttermOUT;
		Terminal termIN, termOUT;
		if (basicType(a) == BasicTypes.INSTTERMINAL && basicType(b) == BasicTypes.INSTTERMINAL) {
			//se ambos forem instterm, eles devem ter tipos difentes
			if (termType(a) == termType(b))
				return net; //sai do metodo caso eles sejam iguais
			if (termType(a) == TermType.IN) {
				insttermIN = (InstTerminal) a;
				insttermOUT = (InstTerminal) b;
			} else {
				insttermIN = (InstTerminal) b;
				insttermOUT = (InstTerminal) a;
			}
			if (insttermOUT.isConnected())
				net = insttermOUT.getNet();
			else
				net = Net.createNet(topDesign, createName("n"));
			net.connectInstTerminal(insttermOUT);
			net.connectInstTerminal(insttermIN);
		} else if (basicType(a) == BasicTypes.TERMINAL && basicType(b) == BasicTypes.TERMINAL) {

			if (termType(a) == termType(b))
				return net; //sai do metodo caso eles sejam iguais
			if (termType(a) == TermType.IN) {
				termIN = (Terminal) a;
				termOUT = (Terminal) b;
			} else {
				termIN = (Terminal) b;
				termOUT = (Terminal) a;
			}
			if (termIN.isConnected())
				net = termIN.getNet();
			else
				net = Net.createNet(topDesign, createName("n"));
			net.connectTerminal(termIN);
			net.connectTerminal(termOUT);
		} else {
			InstTerminal instterm;
			Terminal term;
			//Term(IN) -> InstTerm(IN)
			//InstTerm(OUT) -> Term(OUT)
			ConnectPointSwitch(a,b,out instterm, out term);
			if (termType(term) == TermType.IN) {
				if (term.isConnected())
					net = term.getNet();
				else
					net = Net.createNet(topDesign, createName("n"));
				net.connectTerminal(term);
				net.connectInstTerminal(instterm);
			} else {
				if (instterm.isConnected())
					net = instterm.getNet();
				else
					net = Net.createNet(topDesign, createName("n"));
				net.connectInstTerminal(instterm);
				net.connectTerminal(term);
			}

		}
		return net;
	}

	public void ConnectPointSwitch(ConnectionPoint a, ConnectionPoint b, out InstTerminal instterm, out Terminal term ) {
		if (basicType(a) == BasicTypes.INSTTERMINAL) {
			instterm = (InstTerminal) a;
			term = (Terminal) b;
		} else {
			instterm = (InstTerminal) b;
			term = (Terminal) a;
		}
	}
	public void createLibrary() {
		library.Add("AND2",createBaseCell("AND",2));
		library.Add("AND3",createBaseCell("AND",3));
		library.Add("AND4",createBaseCell("AND",4));
		library.Add("AND5",createBaseCell("AND",5));
		library.Add("AND6",createBaseCell("AND",6));
		library.Add("AND7",createBaseCell("AND",7));
		library.Add("AND8",createBaseCell("AND",8));

		library.Add("OR2",createBaseCell("OR",2));
		library.Add("OR3",createBaseCell("OR",3));
		library.Add("OR4",createBaseCell("OR",4));
		library.Add("OR5",createBaseCell("OR",5));
		library.Add("OR6",createBaseCell("OR",6));
		library.Add("OR7",createBaseCell("OR",7));
		library.Add("OR8",createBaseCell("OR",8));

		library.Add("XOR2",createBaseCell("XOR",2));
		library.Add("XOR3",createBaseCell("XOR",3));
		library.Add("XOR4",createBaseCell("XOR",4));
		library.Add("XOR5",createBaseCell("XOR",5));
		library.Add("XOR6",createBaseCell("XOR",6));
		library.Add("XOR7",createBaseCell("XOR",7));
		library.Add("XOR8",createBaseCell("XOR",8));
		
		library.Add("NAND2",createBaseCell("NAND",2));
		library.Add("NAND3",createBaseCell("NAND",3));
		library.Add("NAND4",createBaseCell("NAND",4));
		library.Add("NAND5",createBaseCell("NAND",5));
		library.Add("NAND6",createBaseCell("NAND",6));
		library.Add("NAND7",createBaseCell("NAND",7));
		library.Add("NAND8",createBaseCell("XOR",8));
		
		library.Add("NOR2",createBaseCell("NOR",2));
		library.Add("NOR3",createBaseCell("NOR",3));
		library.Add("NOR4",createBaseCell("NOR",4));
		library.Add("NOR5",createBaseCell("NOR",5));
		library.Add("NOR6",createBaseCell("NOR",6));
		library.Add("NOR7",createBaseCell("NOR",7));
		library.Add("NOR8",createBaseCell("NOR",8));
		
		library.Add("XNOR2",createBaseCell("XNOR",2));
		library.Add("XNOR3",createBaseCell("XNOR",3));
		library.Add("XNOR4",createBaseCell("XNOR",4));
		library.Add("XNOR5",createBaseCell("XNOR",5));
		library.Add("XNOR6",createBaseCell("XNOR",6));
		library.Add("XNOR7",createBaseCell("XNOR",7));
		library.Add("XNOR8",createBaseCell("XOR",8));
		
		library.Add("AND",createBaseCell("AND",2));
		library.Add("OR",createBaseCell("OR",2));
		library.Add("XOR",createBaseCell("XOR",2));
		library.Add("INV",createBaseCell("INV",1));
		library.Add("NAND",createBaseCell("NAND",2));
		library.Add("NOR",createBaseCell("NOR",2));
		library.Add("XNOR",createBaseCell("XNOR",2));

	}
}