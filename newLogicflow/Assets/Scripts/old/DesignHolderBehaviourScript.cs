﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Netlist;

public class DesignHolderBehaviourScript : MonoBehaviour {

	//position
	private Vector3 offset;
	private Vector3 curScreenPoint;

	//state
	private bool drag = false;
	private bool over = false;
	private bool inserted = false;
	private bool connected = false;

	//objects
	//private Design topDesign;
	//private int id;
	//private LogicSimulator simulator;
	//private static DesignHolderBehaviourScript Instance;

	private GameObject topHolder = null;
	private TopHolderBehaviourScript topHolderScript = null;
	public GameObject termOut = null;
	public GameObject termIn = null;
	public List<GameObject> termInList;

	//behaviour
	public Behaviour halo = null;
	private LineRenderer lineRenderer = null;

	void Start() {
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
		halo = GetComponent ("Halo") as Behaviour;
		lineRenderer = GetComponent<LineRenderer> () as LineRenderer;
	}

	void Update() {

		//default behaviours
		GetComponent<Renderer>().material.color = Color.green;
		lineRenderer.SetPosition(0, transform.position);
		if (termOut == null) lineRenderer.enabled = false;
		else {
			lineRenderer.SetPosition(1, termOut.transform.position);
			lineRenderer.enabled = true;
		}
		//seta o topHolderScript
		if (topHolder != null) topHolderScript = topHolder.GetComponent<TopHolderBehaviourScript> ();

		//mouse/touch behaviour
		if (Input.GetMouseButtonDown (0)) {
			//offset precisa ser calculado apenas uma vez, por isso eh calculado apenas quando o 1o click e registrado
			offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
		}
		if (topHolderScript != null) {
			if (!cellOver ()) {
				connected = false;
			} else {
				if (topHolderScript.touchLeft) {
					//Debug.Log("Name: " + name);
				} else {
					drag = false;
				}
			}
		} else if (Input.GetMouseButtonUp(0)) {
			Destroy(this.gameObject);
		}
		cellDrag ();

	}
	void cellDrag() {
		if (drag) {
			curScreenPoint = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z);
			Vector3 curPosition = Camera.main.ScreenToWorldPoint (curScreenPoint) + offset;
			GetComponent<Renderer>().material.color = Color.blue;
			GetComponent<Rigidbody2D>().velocity = (curPosition - transform.position) / Time.deltaTime;
		} else {
			GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		}

	}
	bool cellOver() {
		if (!over) return false;
		GetComponent<Renderer>().material.color = Color.red;
		if (topHolderScript.Hold ()) {
			drag = true;
		} else if (!drag && topHolderScript.DoubleClick ()) {
			if (!connected) {
				if (topHolderScript.connectA == null) {
					Debug.Log("Setando A");
					topHolderScript.connectA = this.gameObject;
				} else {
					Debug.Log("Setando B");
					topHolderScript.connectB = this.gameObject;
				}
				connected = true;
			}
		} else if (topHolderScript.clickTimerOver ()) {
			//Debug.Log("clickTimerOver");
		}
		if (!inserted) {
			//insere o objeto no topDesign
			if (this.gameObject.tag == "Or") {
				this.gameObject.name = "Or";
				topHolderScript.lib.Add ("Or" + topHolderScript.lib.Count, this.gameObject);
			}
			if (this.gameObject.tag == "And") {
				this.gameObject.name = "And";
				topHolderScript.lib.Add ("And" + topHolderScript.lib.Count, this.gameObject);
			}
			inserted = true;
		}
		return true;
	}

	public void StartDrag() {
		drag = true;
	}

	void OnMouseOver() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
		connected = false;
	}
	void OnMouseDrag() {

	}
	void OnMouseDown() {
	}
	void OnMouseUp() {
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "TopDesigner") {
			this.transform.parent = other.gameObject.transform;
			topHolder = other.gameObject;
		}
	}
	void OnTriggerExit2D(Collider2D other) {
		if (other.tag == "TopDesigner") {
			topHolder = null;
		}
	}
}