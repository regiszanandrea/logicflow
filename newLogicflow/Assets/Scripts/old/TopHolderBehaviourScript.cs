﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netlist;

public class TopHolderBehaviourScript : MonoBehaviour {
	public GameObject connectA = null, connectB = null;

	DesignHolderBehaviourScript designA = null;
	DesignHolderBehaviourScript designB = null;
	public bool touchLeft; //botao esquerdo ou singletouch
	//public bool touchRight; //botao direito ou multitouch?
	//public bool touchMiddle; //botao do meio ou tripletouch?

	//timers
	//public float dragDelay = 3.0f;
	//public float dragTimer = 0f;
	public int clickCount = 1;
	public float clickTime = 0f;
	public float clickDelay = 0.5f;

	public Dictionary<string, GameObject> lib;

	void Start () {
		lib = new Dictionary<string, GameObject> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (connectA != null) {
			designA = connectA.GetComponent<DesignHolderBehaviourScript> ();
		}
		if (connectB != null) {
			designB = connectB.GetComponent<DesignHolderBehaviourScript> ();
		}

		//Touch Event controllers: DoubleClick, Hold, clickTimerEarly, clickTimerOver
		if (Input.GetMouseButtonDown (0)) {
			touchLeft = true;
			clickTime = Time.time;
			clickCount+= 1;
		}
		if (Input.GetMouseButtonUp (0)) {
			touchLeft = false;

			if (clickCount > 1) {
				clickCount = 0;
			} else if (clickTimerOver()) {
				clickCount = 0;
			}
			//faz a conexao quando detecta que existem dois nodos setados e o mouse eh solto
			if (connectA != null && connectB != null) {
				//Debug.Log("Fazendo conexao");
				designA.termOut = connectB;
				designB.termIn = connectA;
				connectA.GetComponent<DesignHolderBehaviourScript> ().termOut = connectB;
				connectB.GetComponent<DesignHolderBehaviourScript> ().termIn = connectA;
				
			}
			if (designA != null) designA.halo.enabled = false;
			if (designB != null) designA.halo.enabled = false;
			designA = designB = null;
			connectA = connectB = null;

		}
		if (touchLeft) {
			if (designA != null) designA.halo.enabled = true;
			if (designB != null) designA.halo.enabled = true;
		}

	}

	public bool DoubleClick() {
		return (clickCount > 1 && touchLeft && clickTimerEarly());
	}
	public bool Hold() {
		return (clickCount == 1 && touchLeft && clickTimerOver());
	}
	public bool clickTimerOver() {
		return (Time.time - clickTime > clickDelay);
	}
	public bool clickTimerEarly() {
		return (Time.time - clickTime < clickDelay);
	}

	void OnTriggerEnter2D(Collider2D other) {
	}
	void OnTriggerExit2D(Collider2D other) {
	}

	void OnMouseOver() {
	}
	void OnMouseExit() {
	}
	void OnMouseDown() {
	}
	void OnMouseUp() {
	}
	void OnMouseDrag() {
	}
}
