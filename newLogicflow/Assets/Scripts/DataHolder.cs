﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Netlist;

[Serializable()]
public class DataHolder : ISerializable {

	[SerializeField] 
	public List<DataHolder> listDataHolder = new List<DataHolder> ();
	[SerializeField]
	protected DesignObj _design;
	private DesignHolder topDesigner = DesignHolder.instance;

	public string name { get { return _design.getName (); } }
	
	[SerializeField]
	protected Vector2 _position;
	[NonSerialized]
	public GameObject _gameObject;
	[SerializeField]
	private bool usingUI = false;
	
	public DataHolder(DesignObj des, Vector2 pos) {
		_design = des;
		_position = pos;
		_gameObject = gameObject;
		
	}
	public DataHolder(DesignObj des, Vector2 pos, bool usingUI) {
		_design = des;
		_position = pos;
		this.usingUI = usingUI;
		_gameObject = gameObject;
	}
	public DataHolder(string design, int connections, Vector2 pos) {
		_design = topDesigner.createInstance (design + connections.ToString (), connections);
		_position = pos;
		this.usingUI = true;
		_gameObject = gameObject;
	}

	public DataHolder() {}

	public DataHolder(SerializationInfo info, StreamingContext context) {
		listDataHolder 	= (List<DataHolder>)info.GetValue ("ListDataHolder", typeof(List<DataHolder>));
		_design 		= (DesignObj)info.GetValue ("Design", typeof(DesignObj));
		float posX 		= (float)info.GetValue ("PosX", typeof(float));
		float posY 		= (float)info.GetValue ("PosY", typeof(float));
		_position = new Vector2 (posX,posY);
		usingUI = (bool)info.GetValue ("UsingUI", typeof(bool));
	}

	void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
		info.AddValue("ListDataHolder", listDataHolder, typeof(List<DataHolder>));
		info.AddValue("Design", _design, typeof(DesignObj));
		info.AddValue ("PosX", _position.x, typeof(float));
		info.AddValue ("PosY", _position.y, typeof(float));
		info.AddValue ("UsingUI", usingUI, typeof(bool));
	}

	public GameObject gameObject {
		get {
			if (!_gameObject) {
				_gameObject = (GameObject) GameObject.Instantiate(Resources.Load ("DesignGameObject"), new Vector3(_position.x,_position.y,-1), Quaternion.identity);
				_gameObject.transform.localScale = new Vector3(_gameObject.transform.localScale.x * 0.5f, _gameObject.transform.localScale.y, _gameObject.transform.localScale.z);
				
				DesignGameObject instanceScript = gameObject.GetComponent<DesignGameObject>();
				instanceScript.name = _design.getName();
				instanceScript.designObject = this;
				
				instanceScript.createConnectionObjects();
			}
			return _gameObject;
		}
		//set { _gameObject = value; }
	}
	public DesignObj design {
		get { return _design; }
	}
	public Vector2 position {
		get { return _position; }
		set { _position = value; }
	}
	public Vector3 position3 {
		get { return new Vector3(_position.x, _position.y, 0); }
		set { _position = new Vector2(value.x, value.y); }
	}
	public string designName {
		get {
			if (basicType == BasicTypes.INSTANCE) {
				Instance inst = (Instance) design;
				return inst.getComponent().getName();
			} else if (basicType == BasicTypes.TERMINAL) {
				Terminal term = (Terminal) design;
				return term.getComponentName();
			}
			else if (basicType == BasicTypes.DESIGN) {
				Design des = (Design) design;
				return des.getName();
			}
			
			return name;
		}
	}
	public BasicTypes basicType {
		get {
			if (_design.GetType().Name.Equals("Net")) {
				return BasicTypes.NET;
			} else if (_design.GetType().Name.Equals("Instance")) {
				return BasicTypes.INSTANCE;
			} else if (_design.GetType().Name.Equals("InstTerminal")) {
				return BasicTypes.INSTTERMINAL;
			} else if (_design.GetType().Name.Equals("Terminal")) {
				return BasicTypes.TERMINAL;
			}
			return BasicTypes.DESIGN;
		}
	}
	//----------------------------------------------------------------------------------------------------------------//

	public ConnectionPoint getConnectionPoint(TermType insttermType = (TermType.IN | TermType.OUT), int t = 0) {
		ConnectionPoint retorno = null;
		if (DesignHolder.basicType(design) == BasicTypes.INSTANCE) {
			Instance inst = (Instance) design;
			List<InstTerminal> listInstTerm = inst.getInstTerminals(insttermType);
			if (listInstTerm.Count > t)
				retorno = (ConnectionPoint) listInstTerm[t];
		}
		if (DesignHolder.basicType(design) == BasicTypes.TERMINAL) {
			retorno = (ConnectionPoint) design;
		}
		return retorno;
	}
	public Vector3 getConnectionPointPosition(ConnectionPoint cp){
		return gameObject.GetComponent<DesignGameObject> ().findConnectionGameObject (cp).transform.position;
	}
	public static DataHolder findByDesign(DesignObj pattern, List<DataHolder> lista) {
		return lista.Find (x => x.design == pattern);
	}
	public static GameObject createNetGameObject(Net net, GameObject topHolder) {
		GameObject netGO = null;
		Component[] components = TopHolder.netContainer.GetComponentsInChildren<NetGameObject>();
		
		bool notFound = true;
		foreach (Component comp in components) {
			if (comp.name.Equals(net.getName())) {
				netGO = comp.gameObject;
				//Debug.Log("NET ENCONTRADO = " + comp.name + " " + net.getName());
				notFound = false;
				break;
			}
		}
		if (notFound) {
			netGO = (GameObject) GameObject.Instantiate(Resources.Load("NetGameObject"));
			netGO.name = net.getName();
			//Debug.Log("NET CRIADO = " + net.getName());
		}
		NetGameObject instacedScript = netGO.GetComponent<NetGameObject>();
		
		TermType termType;
		
		foreach (ConnectionPoint cp in net.getConnectionPoints(TermType.IN | TermType.OUT) ) {
			if (cp.GetType().Name.Equals("Terminal")) {
				Terminal term = (Terminal) cp;
				termType = term.getTerminalType();
				if (termType.Equals(TermType.IN)) {
					//conectado a um terminal de entrada (ex switch)
					instacedScript.outConnection = (ConnectionPoint) term;
				} else if (termType.Equals(TermType.OUT)) {
					if (instacedScript.inConnections.Find(x => x.Equals((ConnectionPoint) term)) == null)
						instacedScript.inConnections.Add((ConnectionPoint) term);
				}
			} else if (cp.GetType().Name.Equals("InstTerminal")) {
				InstTerminal instterm = (InstTerminal) cp;
				termType = instterm.getTerminal().getTerminalType();
				if (termType.Equals(TermType.IN)) {
					if (instacedScript.inConnections.Find(x => x.Equals((ConnectionPoint) instterm)) == null)
						instacedScript.inConnections.Add((ConnectionPoint) instterm);
				} else if(termType.Equals(TermType.OUT)){
					instacedScript.outConnection = (ConnectionPoint) instterm;
				}
			}
		}
		return netGO;
	}
	public GameObject findConnectPointGameObject(ConnectionPoint cp) {
		GameObject retorno = null;
		
		foreach (TerminalGameObject t  in gameObject.GetComponentsInChildren<TerminalGameObject>()) {
			if (t.connectionPoint.Equals(cp)) {
				return t.gameObject;
			}
		}
		return retorno;
	}
	public void ChangeConnections(int connections){

	}
}
