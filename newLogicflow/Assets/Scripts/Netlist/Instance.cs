﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Represent the instance of a component on a given design.
	 * Vector instances can be retrieved if present on the original
	 * design, but they cannot be created.
	 */ 
	[Serializable()]
	public class Instance : DesignObj {

		[SerializeField] protected Design component; // refers the intantiated design
		[SerializeField] protected Design design; // design where the instance is in
		[SerializeField] protected string name; // name of the instance
		[SerializeField] protected List<InstTerminal> instTerminals;

		/*
		 * Gets the design where the instance is instantiated
		 */
		public Design getDesign() {
			return design;
		}
		
		/*
		 * Gets the component instantiated by this instance
		 */ 
		public Design getComponent() {
			return component;
		}
					
		/*
		 * Gets all the inst terminal of this instance with the given type.
		 * If no type is provided, all inst terminals are returned. The
		 * types can be combined with the OR bit operator.
		 * <param name = "type"> type of the inst terminals to return.</param>
		 */ 
		public List<InstTerminal> getInstTerminals(TermType type) {
			List<InstTerminal> temp = new List<InstTerminal> ();
			foreach (InstTerminal instterm in instTerminals) {
				if ((instterm.getTerminal().getTerminalType() & type) != 0)
					temp.Add(instterm);
			}
			return temp;		
		}
					
		/*
		 * Gets the name of this instance.
		 */ 
		public override string getName() {
			return name;
		}

		/*
		 * Visistor concrete method definition.
		 */
		public override void accept(NetlistVisitor netlistVisitor) {
			netlistVisitor.visit(this);
		}
			
		/*
		 * Adds an instance terminal to the instance.
		 */ 
		public void addInstTerminal(InstTerminal instterm) {
			this.instTerminals.Add(instterm);
		}
			
		/*
		 * Finds an instance inside a given design. Searches hierarchically
		 * using Verilog standard names
		 * <param name = "design"> design to search for the instance.</param>
		 * <param name = "fullName"> full name of the instance to search for (Verilog standard name).</param>
		 */ 
		public static bool find(Design design, string fullName, out Instance instance) { // TODO: implemented using a single nane and not the full name (hierarquical)
			instance = null;
			foreach(Instance inst in design.getInstances()) {
				if (inst.getName() == fullName) {
					instance = inst;
					return true;
				}
			}
			return false;
		}
		
		/*
		 * Instantiate the component design inside a given design with the
		 * given name.
		 * <param name = "component"> design to instantiate.</param>
		 * <param name = "design"> design to instantiate the component inside.</param>
		 * <param name = "name"> name of the new instance.</param>
		 */ 
		public static Instance createInstance(Design component, Design design, string name) {
			Instance instance;
			if (!Instance.find(design, name, out instance)) { // Creates a new instance if there is no instance with the 'name' into the design
				instance = new Instance ();
				instance.name = name;
				instance.component = component;
				instance.design = design;
				instance.instTerminals = new List<InstTerminal> ();
				design.addInstance(instance);
			}
			return instance;
		}

		public override string ToString () {
			string str = "[Instance] : "+this.name+"\n";
			foreach (InstTerminal instterm in instTerminals) {
				str += "\t- ["+instterm.getName()+"] : "+instterm.getTerminal().getTerminalType()+"\n";
			}
			return str;
		}

	}

} // namespace Netlist