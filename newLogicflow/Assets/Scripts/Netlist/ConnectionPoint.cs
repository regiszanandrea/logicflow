﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Abstract class that represents a point of connectivity in
	 * the design. Can be either a Terminal or a InstTerminal.
	 */
	[Serializable()]
	public abstract class ConnectionPoint : DesignObj {

	}

} // namespace Netlist