﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Represents a cell terminal.
	 */ 
	[Serializable()]
	public class Pin : DesignObj {

		[SerializeField] protected string name;

		/*
		 * Returns the name of this pin.
		 */
		public override string getName() {
			return name;
		}

		/*
		 * Visistor concrete method definition.
		 */
		public override void accept(NetlistVisitor netlistVisitor) {
			netlistVisitor.visit(this);
		}
			
	}

} // namespace Netlist