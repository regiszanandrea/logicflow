﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Netlist {
	
	public enum LogicValue {
		ZERO = 0,
		ONE = 1,
		UNDEFINED = 3,
		Z = -1
	};

	public class LogicValueFunctions {
		public static LogicValue and(LogicValue op1, LogicValue op2) {
			if (op1 == LogicValue.UNDEFINED || op2 == LogicValue.UNDEFINED) {
				return LogicValue.UNDEFINED;
			}
			else if (op1 == LogicValue.Z || op2 == LogicValue.Z) {
				return LogicValue.ZERO;
			}
			return op1 & op2;
		}

		public static LogicValue or(LogicValue op1, LogicValue op2) {
			if (op1 == LogicValue.UNDEFINED || op2 == LogicValue.UNDEFINED) {
				return LogicValue.UNDEFINED;
			}
			else if (op1 == LogicValue.Z || op2 == LogicValue.Z) {
				return (op1 == LogicValue.Z ? op2 : op1);
			}
			return op1 | op2;
		}

		public static LogicValue not(LogicValue op1) {
			if (op1 == LogicValue.UNDEFINED) {
				return LogicValue.UNDEFINED;
			}
			else if (op1 == LogicValue.Z) {
				return LogicValue.Z;
			}
			return op1 == LogicValue.ZERO ? LogicValue.ONE : LogicValue.ZERO;
		}

		public static LogicValue xor(LogicValue op1, LogicValue op2) {
			if (op1 == LogicValue.UNDEFINED || op2 == LogicValue.UNDEFINED) {
				return LogicValue.UNDEFINED;
			}
			else if (op1 == LogicValue.Z || op2 == LogicValue.Z) {
				return LogicValue.Z;
			}
			return op1 ^ op2;
		}
	}
} // namespace Netlist