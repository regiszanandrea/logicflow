﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Represent a module of the gate-level netlist. A Design
	 * object is composed by Terminals, Instances and Nets and
	 * allows the representation of hierarchy.
	 */ 
	[Serializable()]
	public class Design : DesignObj, ISerializable {

		[SerializeField] protected string name;
		[SerializeField] protected bool ismodule;
		[SerializeField] protected Cell cell;
		[SerializeField] protected List<Instance> instances;
		[SerializeField] protected List<Net> nets;
		[SerializeField] protected List<Terminal> terminals;
		
		public Design() {}

		public Design(SerializationInfo info, StreamingContext context) {
			name 		= (string)info.GetValue ("Name", typeof(string));
			ismodule 	= (bool)info.GetValue ("IsModule", typeof(bool));
			cell 		= (Cell)info.GetValue ("Cell", typeof(Cell));
			instances 	= (List<Instance>)info.GetValue ("Instances", typeof(List<Instance>));
			nets 		= (List<Net>)info.GetValue ("Nets", typeof(List<Net>));
			terminals 	= (List<Terminal>)info.GetValue ("Terminals", typeof(List<Terminal>));

		}
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
			info.AddValue("Name", name, typeof(string));
			info.AddValue("IsModule", ismodule, typeof(bool));
			info.AddValue("Cell", cell, typeof(Cell));
			info.AddValue("Instances", instances, typeof(List<Instance>));
			info.AddValue("Nets", nets, typeof(List<Net>));
			info.AddValue("Terminals", terminals, typeof(List<Terminal>));
		}


		/*
		 * This function returns a boolean that indicates if this
		 * design is a module. When false, indicates that the design
		 * is a cell.
		 */
		public bool isModule() {
			return ismodule;
		}

		/*
		 * Returns the design name.
		 */
		public override string getName() {
			return name;
		}
		
		public void setName(string name) {
			this.name = name;
		}

		/*
		 * Visistor concrete method definition.
		 */
		public override void accept(NetlistVisitor netlistVisitor) {
			netlistVisitor.visit(this);
		}
			
		/*
		 * Returns a pointer to the correspondent cell if the design
		 * is not a module.
		 */
		public Cell getCell() {
			if (this.ismodule)
				return null; // it has subdesigns into it
			else
				return cell; // leaf cell
		}

		/*
		 * Returns a list with pointers to all the instances in the design.
		 */
		public List<Instance> getInstances() {
			if (this.ismodule)
				return instances;
			else
	            return null;
		}

		/*
		 * Returns a list with pointers all the nets in the design.
		 */
		public List<Net> getNets() {
			if (this.ismodule)
				return nets;
			else
				return null;
		}

		/*
		 * Adds a terminal into the design.
		 * <param name = "term"> the terminal to be added.</param>
		 */
		public bool addTerminal(Terminal term) {
			if (term != null) {
				terminals.Add (term);
				return true;
			}
			return false;
		}
		public bool removeTerminal(Terminal term) {
			if (term != null) {
				terminals.Remove(term);
				return true;
			}
			return false;
		}	

		/*
		 * Adds a instance into the instances list.
		 * <param name = "inst"> the instance to be added.</param>
		 */
		public bool addInstance(Instance inst) {
			if (inst != null) {
				instances.Add(inst);
				return true;
			}
			return false;
		}
		public bool removeInstance(Instance inst) {
			if (inst != null) {
				instances.Remove(inst);
				return true;
			}
			return false;
		}	
		/*
		 * Adds a net into the nets list.
		 * <param name = "net"> the net to be added.</param>
		 */
		public bool addNet(Net net) {
			if (net != null) {
				nets.Add (net);
				return true;
			}
			return false;
		}
		public bool removeNet(Net net) {
			if (net != null) {
				nets.Remove(net);
				return true;
			}
			return false;
		}	

		/*
		 * Returns a list with pointer to all the terminals with the
		 * given type on the design. If no type is provided, all
		 * terminals are returned.
		 * <param name = "type"> type of the terminals to be returned.
		 * The types can be combined with a bit OR.</param>
		 */ 
		public List<Terminal> getTerminals(TermType type) {
			List<Terminal> temp = new List<Terminal> ();
			//Debug.Log("Design.getTerminals ["+this.name+"] : size = "+terminals.Count+" : type = "+type);
			
			//List<Terminal> temp = terminals.FindAll(term => term.getTerminalType() <= type);

	        foreach (Terminal term in terminals) {
				if ((term.getTerminalType() & type) != 0)
					temp.Add(term);
			}

			return temp;
		}

		/* 
		 * Creates a design with the provided name.
		 * <param name = "name"> name of the design to be created</param>
		 */
		public static Design createDesign(string name) {
			Design design = new Design ();
			design.name = name;
			design.ismodule = true;
			design.cell = null;
			design.instances = new List<Instance> ();
			design.nets = new List<Net> ();
			design.terminals = new List<Terminal> ();
				
			return design;
		}

		/* 
		 * Creates a design with the provided name.
		 * <param name = "cell"> library cell object</param>
		 */
		public static Design createCellDesign(Cell cell) {
			Design design = new Design ();
			design.name = cell.getName();
			design.ismodule = false;
			design.cell = cell;
			design.instances = null;
			design.nets = null;
			design.terminals = new List<Terminal> ();
			
			return design;
		}

		public override string ToString () {
			string str = "[Design] : "+this.name+"\n";
			str += "* I/O Terminals *\n";
			foreach (Terminal term in terminals) {
				str += "\t- ["+term.getName()+"] : "+term.getTerminalType()+"\n";
			}
			if (this.ismodule)	{
				str += "* Instances *\n";
				foreach (Instance inst in instances) {
					str += inst.ToString();
				}			
				str += "* Nets *\n";
				foreach (Net net in nets) {
					str += net.ToString();
				}			
			}
			return str;
		}

		/*
		 * It is just for debug purposes !!!!!!!!!!!!!!!!!
		 */
		public static void sandBox1(string name) {
			// Creating the cell library and their designs ---
			Library library = new Library("BasicLibrary");
			Cell c_and2 = library.createCell("AND2");
			Design and2 = Design.createCellDesign(c_and2);
			Terminal.createTerminal(and2, "A", TermType.IN);
			Terminal.createTerminal(and2, "B", TermType.IN);
			Terminal.createTerminal(and2, "Y", TermType.OUT);
				
			// Creating topdesign ----------------------------
			Debug.Log("Creating the top design object '" + name + "'\n");
			Design topdesign = Design.createDesign(name);
			// Creating primary inputs -----------------------
			Net net1 = Net.createNet(topdesign, "n1");
			net1.connectTerminal(Terminal.createTerminal(topdesign, "INPUT1", TermType.IN));
			Net net2 = Net.createNet(topdesign, "n2");
			net2.connectTerminal(Terminal.createTerminal(topdesign, "INPUT2", TermType.IN));
			Net net4 = Net.createNet(topdesign, "n4");
			net4.connectTerminal(Terminal.createTerminal(topdesign, "INPUT3", TermType.IN));
			// Creating primary outputs ----------------------
			Net net3 = Net.createNet(topdesign, "n3");
			net3.connectTerminal(Terminal.createTerminal(topdesign, "OUTPUT1", TermType.OUT));
			net4.connectTerminal(Terminal.createTerminal(topdesign, "OUTPUT2", TermType.OUT));
			
			// Instantiating designs -------------------------
			Instance inst = Instance.createInstance(and2, topdesign, "I1_AND2");
			Terminal term;
			if (Terminal.find(and2, "A", out term))
				InstTerminal.createInstTerminalAndConnect(net1, inst, term);
			if (Terminal.find(and2, "B", out term))
				InstTerminal.createInstTerminalAndConnect(net2, inst, term);
			if (Terminal.find(and2, "Y", out term))
				InstTerminal.createInstTerminalAndConnect(net3, inst, term);


			// Starting Simulation ---------------------------
			Debug.Log("Traversing the top design ...");
			LogicSimulator ls = new LogicSimulator();

			Terminal input;
			Terminal.find(topdesign, "INPUT1", out input);
			ls.setValue(input, LogicValue.ONE);

			Terminal.find(topdesign, "INPUT2", out input);
			ls.setValue(input, LogicValue.ONE);
				
			Terminal.find(topdesign, "INPUT3", out input);
			ls.setValue(input, LogicValue.ONE);
			
			ls.visit(topdesign);
				
		}

		public static void sandBox2(string name) {
			// Creating the cell library and their designs ---
			Library library = new Library("BasicLibrary");
			Cell c_and2 = library.createCell("AND");
			Design and2 = Design.createCellDesign(c_and2);
			Terminal.createTerminal(and2, "A", TermType.IN);
			Terminal.createTerminal(and2, "B", TermType.IN);
			Terminal.createTerminal(and2, "Y", TermType.OUT);
			
			Cell c_or2 = library.createCell("OR");
			Design or2 = Design.createCellDesign(c_or2);
			Terminal.createTerminal(or2, "A", TermType.IN);
			Terminal.createTerminal(or2, "B", TermType.IN);
			Terminal.createTerminal(or2, "Y", TermType.OUT);
			
			// Creating topdesign ----------------------------
			Debug.Log("Creating the top design object '" + name + "'\n");
			Design topdesign = Design.createDesign(name);
			// Creating primary inputs -----------------------
			Net net1 = Net.createNet(topdesign, "n1");
			net1.connectTerminal(Terminal.createTerminal(topdesign, "INPUT1", TermType.IN));
			Net net2 = Net.createNet(topdesign, "n2");
			net2.connectTerminal(Terminal.createTerminal(topdesign, "INPUT2", TermType.IN));
			Net net4 = Net.createNet(topdesign, "n4");
			net4.connectTerminal(Terminal.createTerminal(topdesign, "INPUT3", TermType.IN));
			// Creating primary outputs ----------------------
			Net net3 = Net.createNet(topdesign, "n3");
			net3.connectTerminal(Terminal.createTerminal(topdesign, "OUTPUT1", TermType.OUT));
			Net net5 = Net.createNet(topdesign, "n5");
			net5.connectTerminal(Terminal.createTerminal(topdesign, "OUTPUT2", TermType.OUT));
			
			// Instantiating designs -------------------------
			Instance inst = Instance.createInstance(and2, topdesign, "I1_AND2");
			Terminal term;
			if (Terminal.find(and2, "A", out term))
				InstTerminal.createInstTerminalAndConnect(net1, inst, term);
			if (Terminal.find(and2, "B", out term))
				InstTerminal.createInstTerminalAndConnect(net2, inst, term);
			if (Terminal.find(and2, "Y", out term))
				InstTerminal.createInstTerminalAndConnect(net3, inst, term);
			
			inst = Instance.createInstance(or2, topdesign, "I2_OR2");
			if (Terminal.find(or2, "A", out term))
				InstTerminal.createInstTerminalAndConnect(net3, inst, term);
			if (Terminal.find(or2, "B", out term))
				InstTerminal.createInstTerminalAndConnect(net4, inst, term);
			if (Terminal.find(or2, "Y", out term))
				InstTerminal.createInstTerminalAndConnect(net5, inst, term);
				
			// Starting Simulation ---------------------------
			Debug.Log("Traversing the top design ...");
			LogicSimulator ls = new LogicSimulator();
			
			Terminal input;
			Terminal.find(topdesign, "INPUT1", out input);
			ls.setValue(input, LogicValue.ZERO);
			
			Terminal.find(topdesign, "INPUT2", out input);
			ls.setValue(input, LogicValue.ONE);
			
			Terminal.find(topdesign, "INPUT3", out input);
			ls.setValue(input, LogicValue.ONE);
			
			ls.visit(topdesign);
				
		}

		public static void sandBox3(string name) {
			// Creating the cell library and their designs ---
			//Library library = new Library("BasicLibrary");
			//Cell c_and2 = library.createCell("AND");
			List<string> inputnames = new List<string>();
			inputnames.Add("A");
			inputnames.Add("B");
			inputnames.Add("C");
			inputnames.Add("D");
			inputnames.Add("E");
			inputnames.Add("F");
			inputnames.Add("G");
			SortedDictionary<string, BRC> function = BRCBuilder.getBasicRepresentationCodes(7, inputnames);
			function.Add("Y", BRCHandler.and(function["A"], function["B"]));
		
			Debug.Log("BRC: "+BRCHandler.toBinaryString(function["G"]));
			
		}
		
	}

} // namespace Netlist
