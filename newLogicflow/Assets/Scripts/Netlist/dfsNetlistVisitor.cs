﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Netlist {

	public abstract class DfsNetlistVisitor : NetlistVisitor {

		protected List<Net> visited;
		protected List<Net> revisit;

		public DfsNetlistVisitor() {
			visited = new List<Net>();
			revisit = new List<Net>();
		}

		public override void visit(Design design) {
			if (debug) Debug.Log("DOWN [Design]: "+design.getName());

			if (design.isModule()) {
				foreach(Terminal term in design.getTerminals(TermType.OUT)) {
					//Debug.Log("Visit.Design["+design.getName()+"]: Term = "+term.getName());
					term.accept(this);
				}
				//revisita as nets caso seja necessario
				if (revisit.Count > 0) {
					revisit.ForEach( n => {
						visited.Remove(n);
					});
					design.getTerminals(TermType.OUT).ForEach( t => t.accept(this));
				}
			}
			this.action(design);

		}
		
		public override void visit(Net net) {
			if (debug) Debug.Log("DOWN [Net]: "+net.getName());

			if (!visited.Exists(x => x.getName().Equals(net.getName()))) {
				visited.Add(net);
				foreach (InstTerminal instterm in net.getInstTerminals(TermType.OUT)) {
					//Debug.Log(" -> InstTerminal: "+instterm.getName()+" - "+instterm.getTerminal().getTerminalType());
					instterm.accept(this);
				}
				foreach (Terminal term in net.getTerminals(TermType.IN)) {
					//Debug.Log(" -> Terminal: "+term.getName()+" - "+term.getTerminalType());
					term.accept(this);
				}
			} else if (!revisit.Exists(x => x.Equals(net))) {
				revisit.Add(net);
			}

			this.action(net);
		}

		public override void visit(Instance instance) {
			if (debug) Debug.Log("DOWN [Instance]: "+instance.getName());

			foreach (InstTerminal instterm in instance.getInstTerminals(TermType.IN)) {
				//Debug.Log(" -> InstTerminal: "+instterm.getName()+" - "+instterm.getTerminal().getTerminalType());
				instterm.accept(this);
			}
			this.action(instance);
		}

		public override void visit(InstTerminal instterm) {
			if (debug) Debug.Log("DOWN [InstTerminal]: "+instterm.getName());

			if (instterm.getTerminal().getTerminalType() == TermType.IN && instterm.isConnected()) {
				//Debug.Log(" -> InstTerminal: "+instterm.getName()+" - "+instterm.getTerminal().getTerminalType());
				instterm.getNet().accept(this);
			}
			else if (instterm.getTerminal().getTerminalType() == TermType.OUT) {
				//Debug.Log(" -> InstTerminal: "+instterm.getName()+" - "+instterm.getTerminal().getTerminalType());
				//a proxima linha era como era antes, porem isso soh aceita uma saida, alem de nao percorrer internamente o design
				instterm.getInstance().accept(this);
				//instterm.getTerminal().accept(this);
			}
			this.action(instterm);
		}

		public override void visit(Terminal terminal) {
			if (debug) Debug.Log("DOWN [Terminal]: "+terminal.getName());

			if (terminal.getTerminalType() == TermType.OUT && terminal.isConnected()) {
				//Debug.Log(" -> Terminal: "+terminal.getName()+" - "+terminal.getTerminalType());
				
				terminal.getNet().accept(this);
			}
			this.action(terminal);
		}

		public override void visit(Cell cell) {
		}

		public override void visit(Library library) {
		}

		public override void visit(Pin pin) {
		}

		// When used, it has to be implemented in the child classes.
		public abstract void action(DesignObj designObject);
		
	}

} // namespace Netlist