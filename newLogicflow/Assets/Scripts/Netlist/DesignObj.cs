﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/* 
	 * The DesignObj is the base netlist class, containing functions that
	 * will be required by all objects and also defining common interfaces.
	 * All design objects will inherit from it.
	 */
	[Serializable()]
	public abstract class DesignObj {

		/*
		 * Retrieves and returns the name of the given design object.
		 */
		public virtual string getName() {
			return "";
		}

		/*
		 * Creates a copy of a given design object.
		 */
	//	public virtual DesignObj copy() {
	//		return null;
	//	}

		/*
		 * General visitor method for the netlist objects.
		 */
		public abstract void accept(NetlistVisitor netlistVisitor);
	}

} // namespace Netlist