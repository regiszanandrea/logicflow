﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Represents a cell component.
	 */ 
	[Serializable()]
	public class Cell : DesignObj {

		[SerializeField]protected string name;

		/*
		 * Cell constructor.
		 */
		public Cell(string name) {
			this.name = name;
		}
			
		/*
		 * Returns the name of this cell.
		 */
		public override string getName() {
			return name;
		}
			
		/*
		 * Visistor concrete method definition.
		 */
		public override void accept(NetlistVisitor netlistVisitor) {
			netlistVisitor.visit(this);
		}
			
		/*
	     * Gets all the pins with the given type.
	     */
		//public Dictionary<string, BRC> getPins() {
		//	return pins;
		//}
		
		/*
		 * Adds a pin in the cell.
		 */
		//public void addPin(string name) {
	//		this.pins.Add(name, null);
	//	}

	}

} // namespace Netlist
