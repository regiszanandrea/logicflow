﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Represents a terminal on the design. Multi-bit terminals
	 * can be retrieved if present on the original design but
	 * they cannot be created.
	 */
	[Serializable()]
	public class Terminal : ConnectionPoint {

		[SerializeField] protected string name;
		[SerializeField] protected string component;
		[SerializeField] protected TermType type;
		[SerializeField] protected Net net;
		[SerializeField] protected bool connected;

		/*
		 * Returns the type of the terminal.
		 */ 
		public TermType getTerminalType() {
			return type;
		}
		
		/*
		 * Returns the terminal name.
		 */ 
		public override string getName() {
			return name;
		}
		public void setName(string novoName) {
			this.name = novoName;
		}
		public string getComponentName() {
			return component;
		}
		/*
		 * Returns true if the InstTerminal is connected to a net
		 */
		public bool isConnected() {
			return connected;
		}
			
		/*
		 * Visistor concrete method definition.
		 */
		public override void accept(NetlistVisitor netlistVisitor) {
			netlistVisitor.visit(this);
		}
			
		/*
		 * Gets the design which contains the terminal.
		 */ 
		public Design getDesign() {
			return net.getDesign();
		}
		
		/*
		 * Gets the net connected to the terminal.
		 */ 
		public Net getNet() {
			return net;
		}

		/*
		 * Sets a net to the terminal.
		 */ 
		public void setNet(Net net) {
			this.net = net;
			this.connected = true;
		}

		/*
		* Disconnect the instance terminal from the net.
		*/
		public bool disconnect() {
			this.connected = false;
			bool result = this.net.disconnectTerminal (this);
			this.net = null;
			return result;
		}

		/*
		 * Disconnect the terminal from the current net and connect
		 * it to the new net.
		 * <param name = "net"> net to move the terminal connection to.</param>
		 */ 
		public bool moveToNet(Net net) {
			if (this.net != null) {
				if (!this.net.disconnectTerminal(this))
					return false;
			}
			if (net != null)
				net.connectTerminal(this);
			this.net = net;
			return true;
		}

		/*
		 * Finds a terminal in a given design.
		 * <param name = "design"> design to search for the terminal.</param>
		 * <param name = "name"> name of the terminal to search for.</param>
		 */ 
		public static bool find(Design design, string name, out Terminal terminal) {
			//Debug.Log("Looking for the terminal '"+name+"' into the design ["+design.getName()+"]");
			terminal = null;
			foreach (Terminal term in design.getTerminals(TermType.IN | TermType.OUT | TermType.INOUT)) {
				//Debug.Log("Terminal.find: term = '"+term.getName()+"' with type ["+term.getTerminalType()+"] --> name: "+name);
				if (term.getName().Equals(name)) {
					//Debug.Log("This name already exists!!!!! -> "+term);
					terminal = term;
					return true;
				}
			}
			return false;
		}
		
		/*
		 * Creates a terminal in a design.
		 * <param name = "design"> design to create the terminal into.</param>
		 * <param name = "name"> name of the new terminal.</param>
		 * <param name = "type"> type of the new terminal.</param>
		 */ 
		public static Terminal createTerminal(Design design, string name, TermType type, string component = "") {
			//Debug.Log("Creating the terminal '"+name+"' with type ["+type+"]");
			Terminal terminal = null;
			if (!Terminal.find(design, name, out terminal)) {
				//Debug.Log("There is no terminal with name: '"+name+"'");
				terminal = new Terminal ();
				terminal.name = name;
				terminal.type = type;
				terminal.component = component.Equals("") ? name : component;
				terminal.net = null;
				design.addTerminal(terminal);
				//Debug.Log("createTerminal - terminals count = " + design.getTerminals(type).Count);
			}
			//Debug.Log(" Terminal: " + terminal + " -> name: "+terminal.getName()+" -> type: "+terminal.getTerminalType());
			return terminal;
		}
								
	}

} //namespace Netlist