﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Netlist {

	/*
	 * Represents the connectivity element on the design. No
	 * buses are present on this API. Multi-bit nets can be
	 * retrieved if they are present on the original Verilog,
	 * but they cannot be created.
	 */
	[Serializable()]
	public class Net : DesignObj {

		[SerializeField] protected Design design;
		[SerializeField] protected string name;
		[SerializeField] protected List<InstTerminal> instTerminals;
		[SerializeField] protected List<Terminal> terminals;
		
		/*
		 * Gets the design where the net is instantiated.
		 */ 
		public Design getDesign() {
			return design;
		}
				
		/*
		 * Gets the name of this instance.
		 */
		public override string getName() {
			return name;
		}

		/*
	 	 * Visistor concrete method definition.
	 	 */
		public override void accept(NetlistVisitor netlistVisitor) {
			netlistVisitor.visit(this);
		}
					
		/*
		 * Connects an instance terminal to the net.
		 */ 
		public void connectInstTerminal(InstTerminal instterm) {
			/** - codigo antigo q usava nome, novo codigo usa referencia, a mudanca foi necessaria pq o nome dos terminais pode mudar;
			if (!instTerminals.Exists(x => x.getName().Equals(instterm.getName()))) {
				this.instTerminals.Add(instterm);
				instterm.setNet(this);
			}
			**/
			if (!instTerminals.Exists( x => x.Equals(instterm))) {
				this.instTerminals.Add(instterm);
				instterm.setNet(this);
			}
		}
			
		/*
		 * Connects a terminal to the net.
		 */ 
		public void connectTerminal(Terminal term) {
			/**
			if (!terminals.Exists(x => x.getName().Equals(term.getName()))) {
				this.terminals.Add(term);
				term.setNet(this);
			}
			**/
			if (!terminals.Exists(x => x.Equals(term))) {
				this.terminals.Add(term);
				term.setNet(this);
			}

		}
					
		/*
		 * Connects a connection point to the net.
		 */ 
		public void connectAConnectionPoint(ConnectionPoint point) {
			if (point.GetType().Name.Equals("Terminal"))
				this.connectTerminal((Terminal) point);
			else if (point.GetType().Name.Equals("InstTerminal"))
				this.connectInstTerminal((InstTerminal) point);
		}
			
		/*
		 * Disconnects an instance terminal from the net.
		 */ 
		public bool disconnectInstTerminal(InstTerminal instterm) {

			if (instterm != null) {
				if (instterm.isConnected())
					instterm.disconnect();
				return this.instTerminals.Remove(instterm);
			}
			return false;
		}
		
		/*
		 * Disconnects a terminal from the net.
		 */ 
		public bool disconnectTerminal(Terminal term) {
			if (term != null) {
				if (term.isConnected())
					term.disconnect();
				return this.terminals.Remove(term);
			}
			return false;
		}
		/**
		 * Disconnects a connection point from the net.
		 */
		public bool disconnectConnectionPoint(ConnectionPoint point) {
			if (point != null) {
				if (point.GetType().Name.Equals("Terminal")) {
					Terminal term = (Terminal) point;
					return disconnectTerminal(term);
				} else if (point.GetType().Name.Equals("InstTerminal")) {
					InstTerminal instterm = (InstTerminal) point;
					return disconnectInstTerminal(instterm);
					//return this.instTerminals.Remove(instterm);
				}
			}
			return false;
		}	
		/*
		 * Gets all the inst terminals connected to this net with
		 * the given type. If no type is provided, all inst terminals
		 * are returned. The types can be combined with the OR bit
		 * operator.
		 * <param name = "type"> type of the inst terminals to return.</param>
		 */
		public List<InstTerminal> getInstTerminals(TermType type) {
			List<InstTerminal> temp = new List<InstTerminal> ();
			foreach (InstTerminal instterm in instTerminals) {
				if ((instterm.getTerminal().getTerminalType() & type) != 0)
					temp.Add(instterm);
			}
			return temp;		
		}
				
		/*
		 * Returns the list of all the terminals connected to this
		 * net with the given type. If no type is provided, all
		 * terminals are returned
		 * <param name = "type"> type of the terminals to be returned. The types can be combined with a bit OR.</param>
		 */ 
		public List<Terminal> getTerminals(TermType type) {
			List<Terminal> temp = new List<Terminal> ();
			foreach (Terminal term in terminals) {
				if ((term.getTerminalType() & type) != 0)
					temp.Add(term);
			}
			return temp;		
		}

		/*
		 * Returns the list of all the connection point (inst terminal and
		 * terminals) connected to this net with the given type. If no
		 * type is provided, all terminals are returned.
		 * <param name = "type"> type of the terminals to be returned. The types can be combined with a bit OR.</param>
		 */ 
		public List<ConnectionPoint> getConnectionPoints (TermType type) {
			List<ConnectionPoint> temp = new List<ConnectionPoint> ();
			foreach (InstTerminal instterm in instTerminals) {
				if ((instterm.getTerminal().getTerminalType() & type) != 0)
					temp.Add((ConnectionPoint) instterm);
			}
			foreach (Terminal term in terminals) {
				if ((term.getTerminalType() & type) != 0)
					temp.Add((ConnectionPoint) term);
			}
			return temp;
		}

		/*
		 * Find a net inside the given design.
		 * <param name = "design"> design to find the net.</param>
		 * <param name = "name"> name of the net to be find.</param>
		 */ 
		public static bool find(Design design, string name, out Net netReturn) {
			netReturn = null;
			foreach (Net net in design.getNets()) {
				if (net.getName() == name) {
					netReturn = net;
					return true;
				}
			}
			return false;
		}
				
		/*
		 * Create a net inside the given design.
		 * <param name = "design"> design to create the net.</param>
		 * <param name = "name"> ame of the new net.</param>
		 */ 
		public static Net createNet(Design design, string name) {
			Net net;
			if (!Net.find(design, name, out net)) {
				net = new Net ();
				net.name = name;
				net.design = design;
				net.instTerminals = new List<InstTerminal> ();
				net.terminals = new List<Terminal> ();
				design.addNet(net);
			}
			return net;
		}

		public override string ToString () {
			string str = "[Net] : "+this.name+"\n";
			foreach (InstTerminal instterm in instTerminals) {
				str += "\t- ["+instterm.getName()+"] : "+instterm.getTerminal().getTerminalType()+"\n";
			}
			foreach (Terminal term in terminals) {
				str += "\t- ["+term.getName()+"] : "+term.getTerminalType()+"\n";
			}
			return str;
		}
			
	}
} // namespace Netlist
