﻿using UnityEngine;
using System.Collections;

public class LibraryGameObject : MonoBehaviour {

	private bool over = false;
	private bool selected = false;

	SpriteRenderer spriteRenderer;
	private Sprite[] sprites, spriteZeroOne, spriteClock;
	private Behaviour halo;

	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		sprites = Resources.LoadAll<Sprite> ("Textures/gate_symbols");
		spriteZeroOne = Resources.LoadAll<Sprite> ("Textures/ZeroOne");
		spriteClock = Resources.LoadAll<Sprite> ("Textures/clock");

		//renderer.material.shader = Shader.Find ("Transparent/Diffuse");
		halo = (Behaviour) GetComponent("Halo");
	}
	
	void Update () {
		updateTexture ();
		if (over) {
			if (Input.GetMouseButtonDown(0)) {
				if (!selected) {
					selected = true;
					TopHolder.selectedLibrary = this;
					Interface.interfaceSelect = "Library";
				} else {
					selected = false;
					TopHolder.selectedLibrary = null;
					Interface.interfaceSelect = "";
				}
			}
		}
		if (TopHolder.selectedLibrary != this)
			selected = false;
		halo.enabled = selected;

	}
	void updateTexture() {
		if (name.Equals("AND")) {
			spriteRenderer.sprite = sprites[0];
		} else if (name.Equals("NAND")) {
			spriteRenderer.sprite = sprites[1];
		} else if (name.Equals("OR")) {
			spriteRenderer.sprite = sprites[2];
		} else if (name.Equals("XOR")) {
			spriteRenderer.sprite = sprites[4];
		} else if (name.Equals("XNOR")) {
			spriteRenderer.sprite = sprites[5];
		} else if (name.Equals("INV")) {
			spriteRenderer.sprite = sprites[6];
		} else if (name.Equals("Switch")) {
			spriteRenderer.sprite = sprites[9];
		} else if (name.Equals("Light")) {
			spriteRenderer.sprite = sprites[7];
		} else if (name.Equals("Zero")) {
			spriteRenderer.sprite = spriteZeroOne[0];
		} else if (name.Equals("One")) {
			spriteRenderer.sprite = spriteZeroOne[1];
		} else if (name.Equals("Clock")) {
			spriteRenderer.sprite = spriteClock[1];
		}
	}
	void OnMouseEnter() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
	}

	void OnMouseDown() {
	}
	void OnMouseUp() {
	}
}
