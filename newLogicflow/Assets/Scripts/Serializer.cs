﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Netlist;

public class Serializer {

	private static string path = Application.persistentDataPath;
	public static string projects {
		get {
			string fullPath = System.IO.Path.Combine(path, "projects");
			if (!System.IO.Directory.Exists(fullPath))
				System.IO.Directory.CreateDirectory(fullPath);
			return fullPath;
		}
	}
	public static string library {
		get {
			string fullPath = System.IO.Path.Combine(path, "library");
			if (!System.IO.Directory.Exists(fullPath))
				System.IO.Directory.CreateDirectory(fullPath);
			return fullPath;

		}
	}

	private string _fullPath;
	public string fullPath {
		get { return _fullPath; }
	}
	public Serializer() {
		_fullPath = Serializer.path;
	}
	public Serializer(string _path) {
		_fullPath = _path;
	}

	public void SerializeObject(string filename, Design objectToSerialize) {
		Stream stream = File.Open(System.IO.Path.Combine (fullPath, filename), FileMode.Create);
		BinaryFormatter bFormatter = new BinaryFormatter();
		bFormatter.Serialize(stream, objectToSerialize);
		stream.Close();
	}
	public void SerializeObject(string filename, DesignData objectToSerialize) {
		string fullFilePath = System.IO.Path.Combine (fullPath, filename);
		Stream stream = File.Open(fullFilePath, FileMode.Create);
		BinaryFormatter bFormatter = new BinaryFormatter();
		bFormatter.Serialize(stream, objectToSerialize);
		stream.Close();
	}

	public DesignData DeSerializeDesignData(string filename) {
		DesignData objectToSerialize;
		Stream stream = File.Open(System.IO.Path.Combine (fullPath, filename), FileMode.Open);
		BinaryFormatter bFormatter = new BinaryFormatter();
		objectToSerialize = (DesignData)bFormatter.Deserialize(stream);
		stream.Close();
		return objectToSerialize;
	}
	public Design DeSerializeDesign(string filename) {
		Design objectToSerialize;
		Stream stream = File.Open(System.IO.Path.Combine (fullPath, filename), FileMode.Open);
		BinaryFormatter bFormatter = new BinaryFormatter();
		objectToSerialize = (Design)bFormatter.Deserialize(stream);
		stream.Close();
		return objectToSerialize;
	}

	/**
	 * Legacy Mode
	 **/
	public void SerializeObject(string filename, DataHolder objectToSerialize) {
		Stream stream = File.Open(System.IO.Path.Combine (path, filename), FileMode.Create);
		BinaryFormatter bFormatter = new BinaryFormatter();
		bFormatter.Serialize(stream, objectToSerialize);
		stream.Close();
	}
	public void SerializeObject(string filename, SaveLoadDataHolder objectToSerialize) {
		Stream stream = File.Open(System.IO.Path.Combine (path, filename), FileMode.Create);
		BinaryFormatter bFormatter = new BinaryFormatter();
		bFormatter.Serialize(stream, objectToSerialize);
		stream.Close();
	}
	public DataHolder DeSerializeObject(string filename) {
		DataHolder objectToSerialize;
		Stream stream = File.Open(System.IO.Path.Combine (path, filename), FileMode.Open);
		BinaryFormatter bFormatter = new BinaryFormatter();
		objectToSerialize = (DataHolder)bFormatter.Deserialize(stream);
		stream.Close();
		return objectToSerialize;
	}
	
	public SaveLoadDataHolder DeSerializeSaveLoad(string filename) {
		SaveLoadDataHolder objectToSerialize;
		Stream stream = File.Open(System.IO.Path.Combine (path, filename), FileMode.Open);
		BinaryFormatter bFormatter = new BinaryFormatter();
		objectToSerialize = (SaveLoadDataHolder)bFormatter.Deserialize(stream);
		stream.Close();
		return objectToSerialize;
	}
}