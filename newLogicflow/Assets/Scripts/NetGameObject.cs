﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netlist;

public class NetGameObject : MonoBehaviour {

	private DesignHolder topDesigner = DesignHolder.instance;
	public List<ConnectionPoint> inConnections = new List<ConnectionPoint>();
	public ConnectionPoint outConnection;
	public List<GameObject> connections = new List<GameObject>();
	public GameObject netGO = null;
	//private Net _net;

	LogicValue lv = LogicValue.ZERO;

	void Start () {
		tag = "NetGameObject";
		/**
		if (outConnection.GetType().Name.Equals("Terminal")) {
			Terminal term = (Terminal) outConnection;
			_net = term.getNet();
		} else if (outConnection.GetType().Name.Equals("InstTerminal")) {
			InstTerminal instterm = (InstTerminal) outConnection;
			_net = instterm.getNet();
		}
		**/
	}
	void Update () {

		if (outConnection != null) {
			Vector3 posOut = getDataHolder(outConnection).getConnectionPointPosition(outConnection);

			lv = getConnectPointValue (outConnection);

			GameObject netGameObject;

			foreach(ConnectionPoint cp in inConnections) {
				//verifica se ainda esta conectado

				Vector3 pos = getDataHolder(cp).getConnectionPointPosition(cp);
				netGameObject = null;
				netGameObject = connections.Find(x => x.name.Equals("C_" + outConnection.getName() + "_" + cp.getName()));
				if (netGameObject == null) {
					netGameObject = (GameObject) Instantiate(Resources.Load ("BezierCurve/BezierCurve"));
					netGameObject.name = "C_" + outConnection.getName() + "_" + cp.getName();
					netGameObject.transform.parent = this.transform;
					connections.Add(netGameObject);
				}
				Component[] comp = netGameObject.gameObject.GetComponentsInChildren(typeof(BezierWaypoint));
				if (comp.Length > 0) {
					comp[0].transform.position = pos;
					comp[comp.Length-1].transform.position = posOut;
				}
				LineRenderer lr = netGameObject.GetComponent<LineRenderer>();
				Color c = lv.Equals(LogicValue.ONE) ? Color.blue : Color.white;
				lr.SetColors(c,c);
			}
		} else
			Destroy(this.gameObject);
	}
	public void removeConnection(ConnectionPoint cp) {
		Debug.Log ("Removendo: " + cp.getName());
		List<GameObject> removedGO = connections.FindAll(x => x.name.Contains(cp.getName()));
		foreach (GameObject go in removedGO) {
			Debug.Log("Encontrado: " + go.name);
			connections.Remove(go);
			Destroy(go);
		}

		//Debug.Log("Desconectando = " + cp.getName() + " inConnections.count = " + inConnections.Count );
		inConnections.Remove (cp);
		//Debug.Log("removed: inConnections.count = " + inConnections.Count );
		outConnection = cp == outConnection ? null : outConnection;
	}
	public void removeNet() {
		//GameObject go = connections.Find(x => x.name.Contains(cp.getName()));
		foreach(GameObject go in connections)
			Destroy(go);
		connections.Clear ();
		inConnections.Clear ();
		outConnection = null;
		Destroy (this);
	}

	DataHolder getDataHolder(ConnectionPoint _point) {
		DataHolder ret = null;
		
		if (_point.GetType().Name.Equals("Terminal")) {
			ret = DataHolder.findByDesign((DesignObj) _point, TopHolder.mainDT.listDataHolder);
		} else if (_point.GetType().Name.Equals("InstTerminal")) {
			InstTerminal instterm;
			instterm = (InstTerminal) _point;
			ret = DataHolder.findByDesign((DesignObj) instterm.getInstance(), TopHolder.mainDT.listDataHolder);
		}
		return ret;
	}
	LogicValue getConnectPointValue(ConnectionPoint _point) {
		LogicValue ret = LogicValue.ZERO;
		if (_point.GetType().Name.Equals("Terminal")) {
			ret = topDesigner.getValue((DesignObj) _point);
		} else if (_point.GetType().Name.Equals("InstTerminal")) {
			InstTerminal instterm = (InstTerminal) _point;
			ret = topDesigner.getValue((DesignObj) instterm.getInstance());
		}
		return ret;
	}
}
