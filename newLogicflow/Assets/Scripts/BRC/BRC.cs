﻿using System.Collections;
using System.Collections.Generic;

public class BRC {

	private List<long> variable; //array para armazenar os inteiros que formarao o BRC
	private int numberBits;


	public BRC(int numberBits) {
		this.variable = new List<long>();
		this.numberBits = numberBits;
	}

	public void addBRC(long value) {
		this.variable.Add(value);
	}
	
	public void setBRC(int pos, long value) {
		this.variable.Insert (pos, value);
	}
	
	public long getBRC(int pos) {
		return variable[pos];
	}
	
	public List<long> getBRC() {
		return variable;
	}
	
	public int getNumberBits() {
		return numberBits;
	}
	
	public void setNumberBits(int numberBits) {
		this.numberBits = numberBits;
	}        
	
	public int sizeBRC(){
		return variable.Count;
	}
}
