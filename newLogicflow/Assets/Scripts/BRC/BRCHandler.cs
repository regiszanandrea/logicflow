﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BRCHandler  {
	public BRCHandler() {
	}

	/* Constroi o BRC da funcao */
	public static BRC builFunctionRepresentationCode(List<List<string>> cubes, SortedDictionary<string, BRC> basicRepresentationCodes) {

		BRC first_code;
		BRC second_code;
		List<BRC> cubesCode = new List<BRC>();



		
		// Cria o BRC de cada cubo
		foreach (List<string> cube in cubes) {
			cubesCode.Add(buildCubesRepresentationCode(cube, basicRepresentationCodes));
		}

		
		// Calcula o BRC da funcao
		first_code = cubesCode[0];
		for (int i = 0; i < cubesCode.Count; i++) {
			second_code = cubesCode[i];
			first_code = BRCHandler.or(first_code, second_code);
		}
		
		return first_code;
	}
	/* Cria o BRC de cada cubo */
	public static BRC buildCubesRepresentationCode(List<string> cube, SortedDictionary<string, BRC> basicRepresentationCodes) {
	
		BRC first_code = basicRepresentationCodes[cube[0]];
		BRC second_code;
		for (int i = 1; i < cube.Count; i++) {
				second_code = basicRepresentationCodes[cube[i]];
			first_code = BRCHandler.and(first_code, second_code);
		}
		
		return first_code;
	}
	public static BRC and(BRC obj1, BRC obj2) {
		long and;
		BRC brc = new BRC(obj1.getNumberBits());
		
		for (int i = 0; i < obj1.sizeBRC(); i++) {
			and = (obj1.getBRC(i)) & (obj2.getBRC(i));
			brc.setBRC(i, and);
		}
		
		return brc;
	}
	public static BRC or(BRC obj1, BRC obj2) {
		long or;
		BRC brc = new BRC(obj1.getNumberBits());
		
		for (int i = 0; i < obj1.sizeBRC(); i++) {
			or = (obj1.getBRC(i)) | (obj2.getBRC(i));
			brc.setBRC(i, or);
		}
		
		return brc;
	}
	public static BRC not(BRC obj) {
		long inverter;
		BRC brc = new BRC(obj.getNumberBits());
		
		for (int i = 0; i < obj.sizeBRC(); i++) {
			inverter = ~(obj.getBRC(i));
			
			switch (obj.getNumberBits()) {
			case 2 :
				inverter = inverter & 3L;
				break;
			case 4:
				inverter = inverter & 15L;
				break;
			case 8:
				inverter = inverter & 255L;
				break;
			case 16:
				inverter = inverter & 65535L;
				break;
			case 32:
				inverter = inverter & 4294967295L;
				break;
			default:
				break;
			}
			
			brc.setBRC(i, inverter);
		}
		return brc ;
	}
	public static bool equal(BRC obj1, BRC obj2) {
		
		for (int i = 0; i < obj1.sizeBRC(); i++) {
			if (obj1.getBRC(i) != obj2.getBRC(i)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static string toHexaString(BRC brc) {
		
		string hexa = "";
		
		for(int i = 0; i < brc.sizeBRC(); i++) {

			hexa += brc.getBRC(i).ToString("X");
		}
		
		return hexa;
		
	}
	
	public static string toIntegerString(BRC brc) {
		
		string str = "";
		
		for(int i = 0; i < brc.sizeBRC(); i++) {
			str += brc.getBRC(i) + " ";
		}
		
		return str;
		
	}
	
	public static string toBinaryString(BRC brc) {
		
		long value;
		string binary = "";
		
		for(int i=0; i < brc.sizeBRC(); i++) {
			
			value = brc.getBRC(i);
			
			/*cria um valor inteiro com 1 no bit mais a esquerda e 0s em outros locais*/
			long displayMask = 1L << 63;
			
			/*para cada bit exibe 0 ou 1*/
			for (int bit = 1; bit <= 64; bit++) {
				/*utiliza displayMask para isolar o bit*/
				binary += ((value & displayMask) == 0 ? '0' : '1');
				
				value = value << 1; //desloca o valor uma posição para a esquerda
			}
		}
		
		return binary;
	}
	/*
	public static void displayInteger(BRC brc) {
		
		for(int i=0; i < brc.sizeBRC(); i++) {
			System.out.print(brc.getBRC(i) + " ");
		}
		
		System.out.println();        
	}*/



	public static void displayBinary(BRC brc) {  
		Debug.Log (toBinaryString(brc));	
	
	}
}
