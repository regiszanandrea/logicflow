﻿using UnityEngine;
using System.Collections;
using Netlist;

public class Interface : MonoBehaviour {

	private bool over = false;
	private Color originalColor;
	private Sprite[] interfaceTexture;
	SpriteRenderer spriteRenderer;

	public static string interfaceSelect;
	void Start () {

		originalColor = GetComponent<Renderer>().material.color;
		interfaceTexture = Resources.LoadAll<Sprite> ("Textures/Interface_icons");

		spriteRenderer = GetComponent<SpriteRenderer> ();

		/**
		if (name == "New") {
			spriteRenderer.sprite = interfaceTexture [6];
		} else if (name == "ZoomMinus") {
			spriteRenderer.sprite = interfaceTexture [0];
		} else if (name == "ZoomPlus") {
			spriteRenderer.sprite = interfaceTexture [1];
		} else if (name == "Connect") {
			spriteRenderer.sprite = interfaceTexture [5];
		} else if (name == "Disconnect") {
			spriteRenderer.sprite = interfaceTexture [4];
		} else if (name == "Move") {
			spriteRenderer.sprite = interfaceTexture [3];
		} else if (name == "Delete") {
			spriteRenderer.sprite = interfaceTexture [2];
		}
		**/
		//Debug.Log ("Count: " + interfaceTexture.Length);
	}

	void Update () {
		if (over) {
			GetComponent<Renderer>().material.color = Color.green;
			if (Input.GetMouseButtonDown(0)) {
				//TopHolder.ClearInterface();
				interfaceSelect = interfaceSelect == name ? "" : name;
			}
		} else if (interfaceSelect == name) {
			GetComponent<Renderer>().material.color = Color.blue;
		} else {
			GetComponent<Renderer>().material.color = originalColor;
		}
		if (name == "Reset") {
			if (DesignHolder.instance.simulateEnabled)
				spriteRenderer.sprite = interfaceTexture [5];
			else
				spriteRenderer.sprite = interfaceTexture [4];
		}
	}
	void OnMouseEnter() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
	}
}
