using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

using Netlist;

public class TopHolder : MonoBehaviour {
	public static GameObject netContainer;
	public static List<GameObject> listGameObjects = new List<GameObject> ();

	private DesignHolder topDesigner = DesignHolder.instance;
	public static DataHolder mainDT;

	public static GameObject topHolder;
	public static LibraryGameObject selectedLibrary = null;
	public static GameObject selectedGameObject = null;
	public static GameObject inConnect = null, outConnect = null;
	private bool over = false;

	public int clickCount = 1;
	private float clickTime = 0f;
	public bool touchOne;
	public bool touchTwo;
	public bool touchThree;

	private bool drag = false;
	private Vector3 initialPos;
	//private Vector3 endPos;
	private Vector2 dragDistance;

	private Vector3 resetHolderPosition;

	void Start () {

		resetHolderPosition = transform.position;
		topHolder = GameObject.Find ("topHolder");
		topDesigner.setTopDesign(Design.createDesign ("TopDesigner"));
		mainDT = new DataHolder (topDesigner.getTopDesign (), Vector2.zero);
		netContainer = new GameObject ("Nets");
		netContainer.transform.parent = this.transform;

	}
	void SaveDataHolder(String namefile) {
		Serializer serializer = new Serializer ();
		serializer.SerializeObject (namefile, mainDT);

	}
	void LoadDataHolder(String filename) {
		ResetDT ();
		ClearInterface();

		Serializer serializer = new Serializer ();
		mainDT = serializer.DeSerializeObject (filename);

		Design des = (Design) mainDT.design;
		DesignHolder.instance.reset(des);

		foreach (DataHolder dh in mainDT.listDataHolder) {
			//Debug.Log("DH = " + dh.name + " POS " + dh.position + " DesignName " + dh.designName );
			dh.gameObject.transform.parent = this.gameObject.transform;
		}

		List<Net> nets = des.getNets ();
		foreach (Net net in nets) {
			//Debug.Log(net.getName());
			GameObject netGO = DataHolder.createNetGameObject(net, this.gameObject);
			netGO.transform.parent = TopHolder.netContainer.transform;
			TopHolder.listGameObjects.Add(netGO);

			foreach (ConnectionPoint cp in net.getConnectionPoints(TermType.IN | TermType.OUT)) {

				DataHolder dh = mainDT.listDataHolder.Find(obj => obj.findConnectPointGameObject(cp));
				GameObject goTerm = dh.findConnectPointGameObject(cp);

				if (goTerm != null)
					goTerm.GetComponent<TerminalGameObject>().netGO = netGO;
			}

		}

		DesignHolder.instance.simulate();
	}
	void ResetDT() {
		DesignHolder.instance.reset(Design.createDesign ("TopDesigner"));

		foreach(GameObject go in TopHolder.listGameObjects) {
			Destroy(go);
		}
		TopHolder.listGameObjects.Clear();

		foreach(DataHolder dh in TopHolder.mainDT.listDataHolder) {
			Destroy(dh.gameObject);
		}
		TopHolder.mainDT.listDataHolder.Clear();

		Destroy (TopHolder.netContainer);
		TopHolder.netContainer = new GameObject ("Nets");
		TopHolder.netContainer.transform.parent = this.transform;
		TopHolder.inConnect = null;
		TopHolder.outConnect = null;
		Interface.interfaceSelect = "";
		selectedLibrary = null;
	}
	void Update () {

		/**
		 * Event handler
		 **/
		if (Input.GetMouseButtonDown (0)) {
			touchOne = true;
			clickTime = Time.time;
			clickCount+= 1;
		}
		if (Input.GetMouseButtonUp(0)) {
			touchOne = false;
			if (clickCount > 1)
				clickCount = 0;
		}
		//Tempo para resetar o duplo click
		if (clickTimerOver (1.5f))
			clickCount = 0;
		if (Input.GetMouseButtonDown (1)) {
			touchTwo = true;
		}
		if (Input.GetMouseButtonUp (1)) {
			touchTwo = false;
		}
		if (Input.GetMouseButtonDown (2)) {
			touchThree = true;
		}
		if (Input.GetMouseButtonUp (2)) {
			touchThree = false;
		}


		if (Interface.interfaceSelect == "Library") {
			this.GetComponent<Renderer>().material.color = Color.cyan;
		} else if (Interface.interfaceSelect == "New") {
			ResetDT();
		} else if (Interface.interfaceSelect == "Delete") {
			this.GetComponent<Renderer>().material.color = Color.magenta;
			if (selectedGameObject != null) {
				if (selectedGameObject.tag == "DesignGameObject") {
					Debug.Log("Removendo DesignGameObject");
					selectedGameObject.GetComponent<DesignGameObject>().RemoveObject();
				} else if (selectedGameObject.tag == "TerminalGameObject") {
					Debug.Log("Removendo TerminalGameObject");
					selectedGameObject.GetComponent<TerminalGameObject>().DisconnectTerminal();
				} else if (selectedGameObject.tag == "NetGameObject") {
					Debug.Log("Removendo NetGameObject");

				}
				ClearInterface();
			}
		} else if (Interface.interfaceSelect == "Load") {
			LoadDataHolder("TopDesign.dh");
			ClearInterface ();
		} else if (Interface.interfaceSelect == "Save") {
			SaveDataHolder("TopDesign.dh");
			ClearInterface ();
		} else if (Interface.interfaceSelect == "Reset") {
			topDesigner.simulateEnabled = !topDesigner.simulateEnabled;
			DesignHolder.instance.simulate ();
			ClearInterface ();
		} else {
			this.GetComponent<Renderer>().material.color = Color.gray;
			//ClearInterface ();
		}
		if (Interface.interfaceSelect == "")
			selectedGameObject = null;

		if (TopHolder.inConnect != null && TopHolder.outConnect != null) {
			//connect
			Net net = DesignHolder.instance.createConnectionCP(TopHolder.inConnect.GetComponent<TerminalGameObject>().connectionPoint, TopHolder.outConnect.GetComponent<TerminalGameObject>().connectionPoint);
			GameObject netGO = DataHolder.createNetGameObject(net, this.gameObject);
			netGO.transform.parent = TopHolder.netContainer.transform;
			TopHolder.listGameObjects.Add(netGO);

			TopHolder.inConnect.GetComponent<TerminalGameObject>().netGO = netGO;
			TopHolder.outConnect.GetComponent<TerminalGameObject>().netGO = netGO;

			TopHolder.inConnect.GetComponent<TerminalGameObject>().selected = false;
			TopHolder.outConnect.GetComponent<TerminalGameObject>().selected = false;
			ClearInterface();
			DesignHolder.instance.simulate();

		}

		if (over) {
			/**
			 * TopHolder Movement
			 * TODO: Melhorar a mobilidade do movimento;
			 **/
			if (Input.GetMouseButtonDown(1)) {
				initialPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			}
			//if (Input.GetMouseButtonUp(1)) {
			//	endPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			//}
			drag = Input.GetMouseButton(1);
			Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			if (drag) {
				dragDistance = new Vector2(initialPos.x - pos.x, initialPos.y - pos.y);
				if (transform.position != pos) {
					//impede que o topHolder deslize alem das bordas
					//bordas laterais (esquerda, direita = eixo X);
					Bounds bounds = OrthographicBounds();
					//Debug.Log(" min = " + bounds.min + " max = " + bounds.max + " size = " + bounds.size + " extents = " + bounds.extents );
					float posXmin = transform.position.x - (transform.localScale.x / 2);
					float posXmax = transform.position.x + (transform.localScale.x / 2);
					float posYmin = transform.position.y - (transform.localScale.y / 2);
					float posYmax = transform.position.y + (transform.localScale.y / 2);
					//Debug.Log("posX = " + posX + " posY = " + posY);
					Vector3 accel = Vector3.Normalize(new Vector3(dragDistance.x, dragDistance.y, 0)) * Time.fixedDeltaTime * 5.0f;
					if (accel.y + posYmin > bounds.min.y || accel.y + posYmax < bounds.max.y)
						accel.y = 0;
					if (accel.x + posXmin > bounds.min.x || accel.x + posXmax < bounds.max.x)
						accel.x = 0;
					transform.Translate(accel);
				}
			}
			/**
			 * Reseta posicao do Holder: teclado R, Three fingers on mobile
			 **/
			if (Input.GetKey(KeyCode.R) || Input.touchCount == 3) {
				transform.position = resetHolderPosition;
			}
			if (Input.GetMouseButtonDown(0)) {
				Debug.Log("Click");
			}
			/**
			 * Insere um item selecionado da biblioteca
			 **/
			if (selectedLibrary != null) {
				if (Input.GetMouseButtonDown(0)) {
					Vector3 touchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					DesignObj des = null;
					if (selectedLibrary.name.Equals("AND")) {
						des = topDesigner.createInstance("AND", 2);
					} else if (selectedLibrary.name.Equals("NAND")) {
						des = topDesigner.createInstance("NAND", 2);
					} else if (selectedLibrary.name.Equals("OR")) {
						des = topDesigner.createInstance("OR", 2);
					} else if (selectedLibrary.name.Equals("XOR")) {
						des = topDesigner.createInstance("XOR", 2);
					} else if (selectedLibrary.name.Equals("XNOR")) {
						des = topDesigner.createInstance("XNOR", 2);
					} else if (selectedLibrary.name.Equals("INV")) {
						des = topDesigner.createInstance("INV", 1);
					} else if (selectedLibrary.name.Equals("Light")) {
						des = topDesigner.createOutput("Light");
					} else if (selectedLibrary.name.Equals("Switch")) {
						des = topDesigner.createInput("Switch", LogicValue.ZERO);
					} else if (selectedLibrary.name.Equals("Zero")) {
						des = topDesigner.createInput("Zero", LogicValue.ZERO);
					} else if (selectedLibrary.name.Equals("One")) {
						des = topDesigner.createInput("One", LogicValue.ONE);
					} else if (selectedLibrary.name.Equals("Clock")) {
						des = topDesigner.createInput("Clock", LogicValue.ZERO);
					}
					DataHolder dh = new DataHolder(des, new Vector2 (touchPos.x, touchPos.y));

					mainDT.listDataHolder.Add (dh);
					dh.gameObject.transform.parent = this.gameObject.transform;
					ClearInterface();
				}
			}
		}
	}

	public static void ClearInterface() {
		TopHolder.selectedLibrary = null;
		TopHolder.selectedGameObject = null;
		if (TopHolder.inConnect) {
			TopHolder.inConnect.GetComponent<TerminalGameObject>().selected = false;
			TopHolder.inConnect = null;
		}
		if (TopHolder.outConnect) {
			TopHolder.outConnect.GetComponent<TerminalGameObject>().selected = false;
			TopHolder.outConnect = null;
		}
		Interface.interfaceSelect = "";
	}

	void OnMouseEnter() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
	}

	public bool DoubleClick(float clickDelay) {
		if (clickCount > 1 && touchOne && clickTimerEarly(0.5f)) {
			clickCount = 0;
			return true;
		} else if (clickTimerOver(0.5f))
			clickCount = 0;
		return false;
	}
	public bool Hold(float clickDelay) {
		//Debug.Log ("clickCount = " + clickCount + " touchOne = " + touchOne + " clickTimerOver = " + clickTimerOver(clickDelay));
		return (clickCount == 1 && touchOne && clickTimerOver(clickDelay));
	}
	public bool clickTimerOver(float clickDelay) {
		return (Time.time - clickTime > clickDelay);
	}
	public bool clickTimerEarly(float clickDelay) {
		return (Time.time - clickTime < clickDelay);
	}


	public static Bounds OrthographicBounds() {
		float cameraHeight = Camera.main.GetComponent<Camera>().orthographicSize * 2;

		Bounds bounds = new Bounds(
			Camera.main.transform.position,
			new Vector3(cameraHeight * Camera.main.aspect, cameraHeight, 0));
		return bounds;
	}
}
