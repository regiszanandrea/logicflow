﻿using UnityEngine;
using System.Collections;

public class LibraryHolder : MonoBehaviour {
	Bounds bounds;
	public Vector2 offset;

	private bool drag = false;
	private bool over = false;

	private Vector3 initialPos;
	//private Vector3 endPos;
	private Vector2 dragDistance;
	
	//private Vector3 resetHolderPosition;

	// Use this for initialization
	void Start () {
		bounds = TopHolder.OrthographicBounds();
		Vector3 pos = new Vector3 (bounds.min.x + (transform.localScale.x / 2) + offset.x, bounds.max.y - (transform.localScale.y / 2) + offset.y, transform.position.z);
		transform.position = pos;
		//resetHolderPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		bounds = TopHolder.OrthographicBounds();
		if (over) {
			if (Input.GetMouseButtonDown(0)) {
				initialPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			}
			drag = Input.GetMouseButton(0);
			Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			if (drag) {
				float dragSpeed = initialPos.y - pos.y;
				//transform.Translate(new Vector3(0,dragSpeed * Time.fixedDeltaTime * 5.0f,0));

				if (dragSpeed != 0f) {
					//impede que o topHolder deslize alem das bordas
					//bordas laterais (esquerda, direita = eixo X);
					//Debug.Log(" min = " + bounds.min + " max = " + bounds.max + " size = " + bounds.size + " extents = " + bounds.extents );
					float posYmin = transform.position.y - (transform.localScale.y / 2);
					float posYmax = transform.position.y + (transform.localScale.y / 2);
					//Debug.Log("posX = " + posX + " posY = " + posY);
					Vector3 accel = Vector3.Normalize(new Vector3(0, -dragSpeed, 0)) * Time.fixedDeltaTime * 5.0f;
					if (accel.y + posYmin > bounds.min.y || accel.y + posYmax < bounds.max.y)
						accel.y = 0;
					transform.Translate(accel);
				}
			}
		}

	}
	void OnMouseEnter() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
	}
}
