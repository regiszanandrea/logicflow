using UnityEngine;
using System.Collections.Generic;
using Netlist;

public class DesignGameObject : MonoBehaviour {

	private DataHolder _designObject = null;
	private DesignHolder topDesigner = DesignHolder.instance;
	private TopHolder topHolderScript;
	public List<GameObject> terminals = new List<GameObject>();

	SpriteRenderer spriteRenderer;
	private Sprite[] sprites, spriteZeroOne, spriteClock;

	//position
	private Vector3 offset;
	private Vector3 curScreenPoint;

	//state
	//private bool drag = false;
	private bool over = false;

	public bool selected = false;
	private bool drag = false;
	private bool reseted = false;

	public Behaviour halo = null;
	public LogicValue defaultLogicValue;

	//temporizacao do clock
	private float clockTimer;
	public float clockDelay;
	//tempora

	public DataHolder designObject {
		get { return _designObject;}
		set { _designObject = value;}
	}

	void Start() {

		tag = "DesignGameObject";
		spriteRenderer = GetComponent<SpriteRenderer> ();
		sprites = Resources.LoadAll<Sprite> ("Textures/gate_symbols");
		spriteZeroOne = Resources.LoadAll<Sprite> ("Textures/ZeroOne");
		spriteClock = Resources.LoadAll<Sprite> ("Textures/clock");

		halo = GetComponent ("Halo") as Behaviour;
		if (TopHolder.topHolder)
			topHolderScript = TopHolder.topHolder.GetComponent<TopHolder> ();
		defaultLogicValue = LogicValue.ZERO;
		if (designObject.designName == "One") {
			defaultLogicValue = LogicValue.ONE;
		}
		//Debug.Log (defaultLogicValue);
		topDesigner.setValue(designObject.design, defaultLogicValue);
		//topDesigner.simulate();
		//updateTexture ();
		clockDelay = 1.5f;
		ResetClock ();

	}
	public void RemoveObject() {
		Component[] comps = designObject.gameObject.GetComponentsInChildren<TerminalGameObject>();
		foreach(TerminalGameObject tgo in comps) {
			DesignHolder.instance.disconnectPointFromNet(tgo.connectionPoint);
			if (tgo.netGO != null) {
				tgo.netGO.GetComponent<NetGameObject>().removeConnection(tgo.connectionPoint);
			}
		}
		Destroy(designObject.gameObject);
		TopHolder.mainDT.listDataHolder.Remove(designObject);
	}
	void Update() {
		designObject.position = transform.position;

		if (topHolderScript) {
			if (over && topHolderScript.Hold (0.5f)) {
				drag = true;
			}
			if (!topHolderScript.touchOne)
				drag = false;
		}
		if (drag) {
			Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			transform.position = new Vector3(pos.x,pos.y,transform.position.z);
		}
		halo.enabled = drag;

		/**
		 * Gerencia de Clock
		 **/
		if (designObject.basicType == BasicTypes.TERMINAL) {
			Terminal term = (Terminal) designObject.design;
			if (!topDesigner.simulateEnabled && !reseted) {
				reseted = true;
				ResetClock();
				//topDesigner.setValue(designObject.design, defaultLogicValue);
			} else if (topDesigner.simulateEnabled) {
				reseted = false;
				if (term.getName().Contains("Clock")) {
					//Debug.Log("Clock");
					clockTimer -= Time.deltaTime;

					if (clockTimer < 0) {
						topDesigner.setValue(designObject.design, topDesigner.getValue(term) == LogicValue.ZERO ? LogicValue.ONE : LogicValue.ZERO );
						topDesigner.simulate();
						ResetClock();
					}
				}
			}
		}

		if (over && Input.GetMouseButtonDown(0)) {
			if (TopHolder.topHolder)
				TopHolder.selectedGameObject = this.gameObject;
			switch(designObject.basicType) {
			case BasicTypes.NET:
				break;
			case BasicTypes.INSTANCE:
				break;
			case BasicTypes.TERMINAL:
				Terminal term = (Terminal) designObject.design;
				if (term.getName().Contains("Light")) {

				} else if (term.getName().Contains("Switch")) {
					if (topDesigner.getValue(term) == LogicValue.ONE) {
						topDesigner.setValue(designObject.design, LogicValue.ZERO);
						topDesigner.simulate();
					} else {
						topDesigner.setValue(designObject.design, LogicValue.ONE);
						topDesigner.simulate();
					}
				}
				break;
			case BasicTypes.DESIGN:
				break;
			}

		}
		updateTexture ();
	}
	void OnMouseOver() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
	}

	void movement(Vector3 vel) {
		Vector3 oldPos = transform.position;
		Vector3 newPos = new Vector3 (oldPos.x + (vel.x * Time.fixedDeltaTime), oldPos.y + (vel.y * Time.fixedDeltaTime), oldPos.z);
		transform.position = newPos;
	}

	public GameObject findConnectionGameObject(ConnectionPoint cp){
		//Debug.Log ("findConnectionGameObject" + cp.getName() + " count: " + terminals.Count);
		return terminals.Find (x => x.GetComponent<TerminalGameObject> ().connectionPoint.getName().Equals(cp.getName()));
	}

	public void createConnectionObjects() {
		if (designObject != null) {

			GameObject clone;
			Vector3 position;
			TerminalGameObject terminalScript;

			switch(designObject.basicType) {
			case BasicTypes.INSTANCE:
				Instance inst = (Instance) designObject.design;
				List<InstTerminal> instterms = inst.getInstTerminals(TermType.IN);
				int i = 1;
				foreach(InstTerminal instterm in instterms) {
					clone = (GameObject)GameObject.Instantiate(Resources.Load ("TerminalGameObject"), designObject.position, Quaternion.identity);
					clone.gameObject.transform.parent = this.transform;
					clone.name = instterm.getName();
					terminalScript = clone.GetComponent<TerminalGameObject>();
					terminalScript.connectionPoint = (ConnectionPoint) instterm;
					float x = designObject.position.x -( (this.transform.localScale.x / 2) + (clone.transform.localScale.x / 2));
					float yOffset = designObject.position.y + (this.transform.localScale.y / 2);
					float y = yOffset - (this.transform.localScale.y / (instterms.Count*2)*i);
					position = (new Vector3(x, y,this.transform.position.z-1));
					clone.transform.position = position;
					terminals.Add (clone);
					i+=2;
				}
				instterms = inst.getInstTerminals(TermType.OUT);
				i = 1;
				foreach(InstTerminal instterm in instterms) {
					clone = (GameObject)GameObject.Instantiate(Resources.Load ("TerminalGameObject"), designObject.position, Quaternion.identity);
					clone.gameObject.transform.parent = this.transform;
					clone.name = instterm.getName();
					terminalScript = clone.GetComponent<TerminalGameObject>();
					terminalScript.connectionPoint = (ConnectionPoint) instterm;
					float x = designObject.position.x +( (this.transform.localScale.x / 2) + (clone.transform.localScale.x / 2));
					float yOffset = designObject.position.y + (this.transform.localScale.y / 2);
					float y = yOffset - (this.transform.localScale.y / (instterms.Count*2)*i);
					position = (new Vector3(x, y,this.transform.position.z-1));
					clone.transform.position = position;
					terminals.Add(clone);
					i+=2;
				}
				break;
			case BasicTypes.TERMINAL:
				Terminal term = (Terminal) designObject.getConnectionPoint();
				clone = (GameObject)GameObject.Instantiate(Resources.Load ("TerminalGameObject"), designObject.position, Quaternion.identity);
				clone.gameObject.transform.parent = this.transform;
				terminalScript = clone.GetComponent<TerminalGameObject>();
				terminalScript.connectionPoint = (ConnectionPoint) term;
				Quaternion rotation = Quaternion.identity;
				if (term.getTerminalType().Equals(TermType.IN)) {
					clone.name = "IN";
					float x = designObject.position.x +( (this.transform.localScale.x / 2) + (clone.transform.localScale.x / 2));
					float y = designObject.position.y;
					position = (new Vector3(x, y,this.transform.position.z));
				} else {
					clone.name = "OUT";
					float x = designObject.position.x;
					float y = designObject.position.y - (this.transform.localScale.y / 2);
					position = (new Vector3(x, y,this.transform.position.z-1));
					rotation = Quaternion.AngleAxis(90,new Vector3(0,0,1));
				}
				clone.transform.position = position;
				clone.transform.rotation = rotation;
				terminals.Add(clone);
				break;
			}
		}
	}

	public void ResetClock() {
		clockTimer = clockDelay;
	}

	void updateTexture() {

		switch(designObject.basicType) {
		case BasicTypes.NET:
			break;
		case BasicTypes.INSTANCE:
			Instance inst = (Instance) designObject.design;
			Design component = inst.getComponent();

			if (component.getName().Equals("AND")) {
				spriteRenderer.sprite = sprites[0];
			} else if (component.getName().Equals("NAND")) {
				spriteRenderer.sprite = sprites[1];
			} else if (component.getName().Equals("OR")) {
				spriteRenderer.sprite = sprites[2];
			} else if (component.getName().Equals("XOR")) {
				spriteRenderer.sprite = sprites[4];
			} else if (component.getName().Equals("XNOR")) {
				spriteRenderer.sprite = sprites[5];
			} else if (component.getName().Equals("INV")) {
				spriteRenderer.sprite = sprites[6];
			}
			break;
		case BasicTypes.TERMINAL:
			Terminal term = (Terminal) designObject.design;
			if (term.getName().Contains("Light")) {
				//Debug.Log(topDesigner.getValue(term));
				if (topDesigner.getValue(term) == LogicValue.ONE) {
					spriteRenderer.sprite = sprites[8];
				} else {
					spriteRenderer.sprite = sprites[7];
				}
			} else if (term.getName().Contains("Switch")) {
				if (topDesigner.getValue(term) == LogicValue.ONE) {
					spriteRenderer.sprite = sprites[10];
				} else {
					spriteRenderer.sprite = sprites[9];
				}
			} else if (term.getName().Contains("Zero")) {
				spriteRenderer.sprite = spriteZeroOne[0];
			} else if (term.getName().Contains("One")) {
				spriteRenderer.sprite = spriteZeroOne[1];
			} else if (term.getName().Contains("Clock")) {
				if (topDesigner.getValue(term) == LogicValue.ONE) {
					spriteRenderer.sprite = spriteClock[1];
				} else {
					spriteRenderer.sprite = spriteClock[0];
				}
			}


			break;
		case BasicTypes.DESIGN:
			break;
		}

	}
}
