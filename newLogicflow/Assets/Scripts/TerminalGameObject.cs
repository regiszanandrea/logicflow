﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netlist;

public class TerminalGameObject : MonoBehaviour {
	private ConnectionPoint _connectionPoint = null;
	public GameObject netGO = null;
	//private DesignHolder topDesigner = DesignHolder.instance;
	//private LogicValue value;

	private bool over = false;
	private Behaviour halo = null;
	public bool selected = false;

	public ConnectionPoint connectionPoint {
		get { return _connectionPoint;}
		set { _connectionPoint = value;}
	}
	void Start() {
		halo = (Behaviour)GetComponent ("Halo");
		tag = "TerminalGameObject";
	}
	public void DisconnectTerminal() {
		//Debug.Log("Removendo " + _connectionPoint.getName());
		DesignHolder.instance.disconnectPointFromNet(_connectionPoint);
		if (netGO != null)
			netGO.GetComponent<NetGameObject>().removeConnection(_connectionPoint);
		else
			Debug.Log("netGO null");
	}
	void Update() {

		//DesignGameObject parentScript = transform.parent.gameObject.GetComponent<DesignGameObject> ();
		InstTerminal instterm = null;
		Terminal term = null;
		if (over && Input.GetMouseButtonDown (0)) {
			if (TopHolder.topHolder)
				TopHolder.selectedGameObject = this.gameObject;
		}
		
		if (!selected) {
			switch(switchConnectionPoint) {
			case 1:
				instterm = (InstTerminal) connectionPoint;
				if (instterm.isConnected())
					GetComponent<Renderer>().material.color = Color.red;
				else {
					if (TopHolder.inConnect != null)
						GetComponent<Renderer>().material.color = Color.red;
					else
						GetComponent<Renderer>().material.color = Color.green;
				}
				break;
			case 2:
				if (TopHolder.outConnect != null)
					GetComponent<Renderer>().material.color = Color.red;
				else
					GetComponent<Renderer>().material.color = Color.green;
				break;
			case 3:
				if (TopHolder.outConnect != null)
					GetComponent<Renderer>().material.color = Color.red;
				else
					GetComponent<Renderer>().material.color = Color.green;
				break;
			case 4:
				term = (Terminal) connectionPoint;
				if (term.isConnected())
					GetComponent<Renderer>().material.color = Color.red;
				else {
					if (TopHolder.inConnect != null)
						GetComponent<Renderer>().material.color = Color.red;
					else
						GetComponent<Renderer>().material.color = Color.green;
				}
				break;
			}
		}
		
		if (over && Input.GetMouseButtonDown(0)) {
			if (!selected) {
				switch(switchConnectionPoint) {
				case 1: //BasicTypes.INSTTERMINAL & TermType.IN
					instterm = (InstTerminal) connectionPoint;
					if (!instterm.isConnected()) {
						if (TopHolder.inConnect != null)
							TopHolder.inConnect.GetComponent<TerminalGameObject>().selected = false;
						TopHolder.inConnect = this.gameObject;
						selected = true;
					} else {
						if (TopHolder.outConnect != null) {
							TopHolder.outConnect.GetComponent<TerminalGameObject>().selected = false;
							TopHolder.outConnect = null;
							//Debug.Log("CONEXAO NAO PERMITIDA");
						}
					}
					break;
				case 2: //BasicTypes.INSTTERMINAL & TermType.OUT
					if (TopHolder.outConnect != null)
						TopHolder.outConnect.GetComponent<TerminalGameObject>().selected = false;
					TopHolder.outConnect = this.gameObject;
					selected = true;
					break;
				case 3: //BasicTypes.TERMINAL & TermType.IN
					if (TopHolder.outConnect != null)
						TopHolder.outConnect.GetComponent<TerminalGameObject>().selected = false;
					TopHolder.outConnect = this.gameObject;
					selected = true;
					break;
				case 4: //BasicTypes.TERMINAL & TermType.OUT
					term = (Terminal) connectionPoint;
					if (!term.isConnected()) {
						if (TopHolder.inConnect != null)
							TopHolder.inConnect.GetComponent<TerminalGameObject>().selected = false;
						TopHolder.inConnect = this.gameObject;
						selected = true;
					} else {
						if (TopHolder.inConnect != null) {
							TopHolder.inConnect.GetComponent<TerminalGameObject>().selected = false;
							TopHolder.inConnect = null;
							//Debug.Log("CONEXAO NAO PERMITIDA");
						}
					}
					break;
				}
			} else {
				if (TopHolder.inConnect == this.gameObject) {
					TopHolder.inConnect = null;
				} else {
					TopHolder.outConnect = null;
				}
				selected = false;
			}
		}
		halo.enabled = selected;
	}
	int switchConnectionPoint {
		get {
			if (DesignHolder.basicType(connectionPoint) == BasicTypes.INSTTERMINAL) {
				if (DesignHolder.termType(connectionPoint) == TermType.IN)
					return 1;
				else
					return 2;
			} else {
				if (DesignHolder.termType(connectionPoint) == TermType.IN)
					return 3;
				else
					return 4;
			}
		}

	}
	void OnMouseEnter() {
		over = true;
	}
	void OnMouseExit() {
		over = false;
	}
}
