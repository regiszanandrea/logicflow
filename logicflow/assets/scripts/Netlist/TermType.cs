﻿using System.Collections;
using System;
namespace Netlist {

public enum TermType {
	IN = 1,
	OUT = 2,
	INOUT = 4
};

} // namespace Netlist