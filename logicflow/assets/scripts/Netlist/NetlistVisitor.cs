﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Netlist {

public abstract class NetlistVisitor : MonoBehaviour {

	public abstract void visit(Design design);
	public abstract void visit(Net net);
	public abstract void visit(Instance instance);
	public abstract void visit(InstTerminal instterm);
	public abstract void visit(Terminal terminal);
	public abstract void visit(Cell cell);
	public abstract void visit(Library library);
	public abstract void visit(Pin pin);
}

} // namespace Netlist