﻿using System.Collections;

namespace Netlist {

/*
 * Abstract class that represents a point of connectivity in
 * the design. Can be either a Terminal or a InstTerminal.
 */ 
public abstract class ConnectionPoint : DesignObj {

}

} // namespace Netlist