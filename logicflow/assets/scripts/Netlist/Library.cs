﻿using System.Collections;
using System.Collections.Generic;

namespace Netlist {

public class Library : DesignObj {

	protected string name;
	protected List<Cell> cells;

	/*
	 * Library constructor.
	 */
	public Library(string name) {
		this.name = name;
		this.cells = new List<Cell>();
	}
	
	/*
	 * Returns the name of the library as it is in the Liberty file.
	 */ 
	public override string getName() {
		return name;
	}

	/*
	 * Visistor concrete method definition.
	 */
	public override void accept(NetlistVisitor netlistVisitor) {
		netlistVisitor.visit(this);
	}
		
	/*
	 * Returns a list of all the cells in the library.
	 */ 
	public List<Cell> getCells() {
		return cells;
	}

	/*
	 * Adds a cell in the library.
	 * <param name = "cell"> the cell to add in the library.</param>
	 */ 
	public void addCell(Cell cell) { // TODO: it does not verify if there is a cell with a name
		cells.Add(cell);
	}
		
	/*
	 * Adds a cell in the library thorugh its name.
	 * <param name = "name"> name of the cell to add in the library.</param>
	 */ 
	public Cell createCell(string cellname) { // TODO: it does not verify if there is a cell with a name
		Cell cell = new Cell(cellname);
		cells.Add(cell);
		return cell;
	}		
		
	/*
	 * Gets the cell with the given name inside the library.
	 * <param name = "name"> name of the cell to get from the library.</param>
	 */ 
	public Cell getCell(string name) {
		foreach (Cell cell in cells) {
			if (cell.getName() == name)
				return cell;
		}
		return null;
	}

}

} //namespace Netlist