﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Netlist {

public class LogicSimulator : DfsNetlistVisitor {

	protected Dictionary<string, LogicValue> values;

	public LogicSimulator() {
		values = new Dictionary<string, LogicValue>();
	}

	public void setValue(DesignObj obj, LogicValue v) {
		if (values.ContainsKey (obj.getName ())) {			
			values [obj.getName ()] = v;
		} else {
			values.Add(obj.getName(), v);
		}
	}
	
	public LogicValue getValue(DesignObj obj) {
		if (values.ContainsKey(obj.getName())){
			return values[obj.getName()];
		}
		return LogicValue.UNDEFINED;
	}

	public override void action(DesignObj obj) {
	
		Debug.Log("UP: "+obj.getName() + " | obj.name: "+obj.GetType().Name);
		
		if (obj.GetType().Name.Equals("Net")) {
			Net net = (Net) obj;
			foreach (InstTerminal instterm in net.getInstTerminals(TermType.OUT)) {
				this.setValue(obj, this.getValue(instterm));
			}
			foreach (Terminal term in net.getTerminals(TermType.IN)) {
				this.setValue(obj, this.getValue(term));
			}
		}
		else if (obj.GetType().Name.Equals("InstTerminal")) {
			InstTerminal instterm = (InstTerminal) obj;
			if (instterm.getTerminal().getTerminalType().Equals(TermType.IN) && instterm.isConnected())
				this.setValue(obj, this.getValue(instterm.getNet()));
			else if (instterm.getTerminal().getTerminalType().Equals(TermType.OUT))
				this.setValue(obj, this.getValue(instterm.getInstance()));
			else
				this.setValue(obj, LogicValue.Z);				
		}
		else if (obj.GetType().Name.Equals("Instance")) {
			Instance inst = (Instance) obj;
			Design component = inst.getComponent();
			LogicValue result = LogicValue.Z;
			if (component.getName().Equals("AND")) {
				result = LogicValue.ONE; 
				foreach (InstTerminal instterm in inst.getInstTerminals(TermType.IN)) {
					//Debug.Log("Result: "+result+" - InstTerm: "+this.getValue(instterm));
					result = LogicValueFunctions.and(result, this.getValue(instterm));
				}
			}
			else if (component.getName().Equals("OR")) {
				result = LogicValue.ZERO; 
				foreach (InstTerminal instterm in inst.getInstTerminals(TermType.IN)) {
					//Debug.Log("Result: "+result+" - InstTerm: "+this.getValue(instterm));
					result = LogicValueFunctions.or(result, this.getValue(instterm));
				}
			}
			else if (component.getName().Equals("INV")) {
				foreach (InstTerminal instterm in inst.getInstTerminals(TermType.IN)) {
					//Debug.Log("Result: "+result+" - InstTerm: "+this.getValue(instterm));
					result = LogicValueFunctions.not(this.getValue(instterm));
				}
			}
			else if (component.getName().Equals("XOR")) {
				result = LogicValue.ZERO; 
				foreach (InstTerminal instterm in inst.getInstTerminals(TermType.IN)) {
					//Debug.Log("Result: "+result+" - InstTerm: "+this.getValue(instterm));
					result = LogicValueFunctions.xor(result, this.getValue(instterm));
				}
			}
			this.setValue(obj, result);
		}
		else if (obj.GetType().Name.Equals("Terminal")) {
			Terminal term = (Terminal) obj;
			if (term.getTerminalType().Equals(TermType.OUT)  && term.isConnected()) {
				this.setValue(obj, this.getValue(term.getNet()));
			} else if (!values.ContainsKey(term.getName())) {
				this.setValue(obj, LogicValue.Z);
			}
		}
		else if (obj.GetType().Name.Equals("Design")) {
			this.visited.Clear();
		}

		Debug.Log("Value of ["+obj.getName()+"] = "+this.getValue(obj));
	}
	

}

} // namespace Netlist