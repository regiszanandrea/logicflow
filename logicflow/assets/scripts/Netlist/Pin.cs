﻿using System.Collections;

namespace Netlist {

/*
 * Represents a cell terminal.
 */ 
public class Pin : DesignObj {

	protected string name;

	/*
	 * Returns the name of this pin.
	 */
	public override string getName() {
		return name;
	}

	/*
	 * Visistor concrete method definition.
	 */
	public override void accept(NetlistVisitor netlistVisitor) {
		netlistVisitor.visit(this);
	}
		
}

} // namespace Netlist