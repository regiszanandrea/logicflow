﻿using UnityEngine;
using System.Collections;

namespace Netlist {

/*
 * Represents the terminal of an instance. Multi-bit instance
 * terminals can be retrieved if present on the design, but
 * they cannot be created.
 */ 

public class InstTerminal : ConnectionPoint {

	protected string name;
	protected Instance instance;
	protected Net net;
	protected Terminal terminal;
	protected bool connected;
				
	/*
	 * Gets the design where the instance terminal is instantiated.
	 */ 
	public Design getDesign() {
		return instance.getDesign();
	}

	/*
	 * Gets the name of the instance terminal.
	 */ 
	public override string getName() {
		return name;
	}

	/*
	 * Returns true if the InstTerminal is connected to a net
	 */
	public bool isConnected() {
		return connected;
	}

	/*
	 * Visistor concrete method definition.
	 */
	public override void accept(NetlistVisitor netlistVisitor) {
		netlistVisitor.visit(this);
	}
		
	/*
	 * Gets the net connected to the instance terminal.
	 */
	public Net getNet() {
		return net;
	}

	/*
	 * Set a net to the instance terminal.
	 */
	public void setNet(Net net) {
		this.net = net;
		this.connected = true;
	}
		
	/*
	 * Disconnect the instance terminal from the net.
	 */
	public bool disconnect() {
		bool result = this.net.disconnectInstTerminal(this);
		if (result) {
			this.net = null;
			this.connected = false;
		}
		return result;
	}

	/*
	 * Gets the terminal that the instance terminal instantiates.
	 */ 
	public Terminal getTerminal() {
		return terminal;
	}

	/*
	 * Gets the instance that contains the instance terminal.
	 */ 
	public Instance getInstance() {
		return instance;
	}

	/*
	 * Disconnect the instance terminal from the current net and
	 * connect it to the new net.
	 * <param name = "net"> net to move the terminal connection to.</param>
	 * 
	 */ 
	public bool moveToNet(Net net) {
		if (this.net != null) {
			if (!this.net.disconnectInstTerminal(this))
				return false;
		}
		if (net != null)
			net.connectInstTerminal(this);
		this.net = net;
		return true;
	}

	/*
	 * Finds an instance terminal inside a given design. Searches
	 * hierarchically using Verilog standard names.
	 * <param name = "design"> design to search for the instance.</param>
	 * <param name = "fullName"> full name of the instance terminal to search for (Verilog standard name).</param>
	 */ 
	//public static InstTerminal find(Design design, string fullName) {
		// TODO: It has to be implemented
	//	return new InstTerminal ();
	//}

	/*
	 * Find an instance terminal given an instance.
	 * <param name = "instance"> instance to search for the inst terminal.</param>
	 * <param name = "name"> name of the inst terminal to search for.</param>
	 */ 
	public static bool find(Instance instance, string name, out InstTerminal instterminal) {
		instterminal = null;
		Debug.Log("Terminal.find : ["+instance.getName()+"] - Looking for: ["+name+"]");
		foreach (InstTerminal instterm in instance.getInstTerminals(TermType.IN | TermType.OUT | TermType.INOUT)) {
			if (instterm.getName() == name) {
				instterminal = instterm;
				return true;
			}
		}
		return false;
	}

	/*
	 * Creates an inst terminal in the given instance connected to the given
	 * net and linked to the given terminal.
	 * <param name = "net"> net to connect the new inst terminal.</param>
	 * <param name = "instance"> instance in which the inst terminal should be created.</param>
	 * <param name = "terminal"> terminal to link the new inst terminal.</param>
	 */ 
	public static InstTerminal createInstTerminalAndConnect(Net net, Instance instance, Terminal terminal) {
		InstTerminal instterm;

		if (!InstTerminal.find(instance, instance.getName()+"."+terminal.getName(), out instterm)) {
			instterm = new InstTerminal ();
			instterm.name = instance.getName()+"."+terminal.getName();
			instterm.instance = instance;
			instterm.terminal = terminal;
			net.connectInstTerminal(instterm);
			instance.addInstTerminal(instterm);
		}
		return instterm;
	}									
	
	/*
	 * Creates an inst terminal in the given instance linked to the given terminal.
	 * <param name = "instance"> instance in which the inst terminal should be created.</param>
	 * <param name = "terminal"> terminal to link the new inst terminal.</param>
	 */ 
		public static InstTerminal createInstTerminal(Instance instance, Terminal terminal) {
			InstTerminal instterm;
			
			if (!InstTerminal.find(instance, instance.getName()+"."+terminal.getName(), out instterm)) {
				instterm = new InstTerminal ();
				instterm.name = instance.getName()+"."+terminal.getName();
				instterm.instance = instance;
				instterm.terminal = terminal;
				instance.addInstTerminal(instterm);
			}
			return instterm;
		}									
	}

} //namespace Netlist