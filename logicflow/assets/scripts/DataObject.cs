using UnityEngine;
using Netlist;
using System.Collections;
using System.Collections.Generic;

public class DataObject : GUIDraggableObject
// This class just has the capability of being dragged in GUI - it could be any type of generic data class
{
	private string m_Name;
	public int m_Value;
	private int numConnecteds;
	private Texture2D gate, gate1, gate2;
	private Texture buttonStyle,buttonStyle2, outputButton;
	private GUIStyle style;
	private static bool paint = false;
	private static DataObject data;

	private bool first = false, switchOut = false;
	public float gateWidth, gateHeight;
	public Texture2D lineTex;
	// Netlist
	private DesignObj designObj;
	public DataObject[] input, output ;

	public DataObject (string name, int value, Vector2 position,Texture2D gateType,GUIStyle noneStyle, Texture btn, Texture btn2 ) : base (position)
	{
		m_Name = name;
		m_Value = value;
		gate = gateType;
		style = noneStyle;
		buttonStyle = btn;
		buttonStyle2 = btn2;
		outputButton = btn;
		input = new DataObject[2];
		output = new DataObject[1];
		 
	}
	public DataObject (string name, int value, Vector2 position,Texture2D gateType, Texture2D gateTypeAux,  GUIStyle noneStyle, Texture btn, Texture btn2 ) : base (position)
	{
		m_Name = name;
		m_Value = value;
		gate = gateType;
		gate1 = gateType;
		gate2 = gateTypeAux;
		style = noneStyle;
		buttonStyle = btn;
		buttonStyle2 = btn2;
		outputButton = btn;
		input = new DataObject[2];
		output = new DataObject[100];
	}

	public void setDesignObj(DesignObj obj) {
		designObj = obj;
	}

	public DesignObj getDesignObj() {
		return designObj;
	}
	
	// Set Name
	public void setName(string name){
		m_Name = name;
	}
	// Get Name
	public string getName(){
		return m_Name;
	}

	// Set value
	public void setValue(int value){
		m_Value = value;
	}

	// Set Texture
	public void setTexture(Texture2D gateType){
		gate = gateType;
	}

	// Change Switch
	public void changeSwitch (){
		if (switchOut) {				
				gate = gate2;
				switchOut = false;
		} else {				
				gate = gate1;
				switchOut = true;
		}
	}
	
	// Get Switch
	public bool getSwitch(){
		return switchOut;
	}

	// Get numConnecteds
	public int getNumConnecteds(){
		return input.Length;
	}

	// Set output Texture
	public void setOutputTexture(Texture output){
		this.outputButton = output;	
	}


	public void OnGUI ()
	{

				gateWidth = Screen.width * 0.10f;
				gateHeight = Screen.height * 0.15f;


		switch (m_Value) {
			case 5:
				gateHeight = Screen.width * 0.15f;
				gateWidth = Screen.height * 0.11f;
				break;
			case 6:
				gateHeight = Screen.width * 0.120f;
				gateWidth = Screen.height * 0.195f;
				break;
			case 7:
				gateHeight = Screen.width * 0.13f;
				gateWidth = Screen.height * 0.14f;
				break;
			case 8:
				gateHeight = Screen.width * 0.13f;
				gateWidth = Screen.height * 0.14f;
				break;
		}


		
		Rect drawRect = new Rect (m_Position.x, m_Position.y, gateWidth,  gateHeight), dragRect;

		GUILayout.BeginArea (drawRect, style);
			GUILayout.Label (gate, style, GUILayout.ExpandWidth (true));
		
		
		dragRect = GUILayoutUtility.GetLastRect ();
		dragRect = new Rect (m_Position.x,m_Position.y, drawRect.width, drawRect.height);
		
		// Dragging
		if (Dragging)
		{
			
		}
		
		GUILayout.EndArea ();

		int buttonSize = 20;

		switch (m_Value) {				
				// Or
				case 1:
						// In
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y+(gateHeight/20), buttonSize, buttonSize), buttonStyle, style)) {							
							connect(0);
							first = true;
						}
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y + (gateHeight/2.9f), buttonSize, buttonSize), buttonStyle, style)) {							
							connect(1);
							first = true;
						}
						// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-4, m_Position.y + (gateHeight/4.5f)-2, buttonSize, buttonSize), outputButton, style)) {
							outputButton = buttonStyle2;						
							DataObject.paint = true;
							DataObject.data = this;
							first = true;
						}
						break;
				
				// And
				case 2:
					// In
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y+(gateHeight/20), buttonSize, buttonSize), buttonStyle, style)) {						
						connect(0);		
						first = true;
					}
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y + (gateHeight/2.9f), buttonSize, buttonSize), buttonStyle, style)) {						
						connect(1);		
						first = true;
					}
					// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-4, m_Position.y + (gateHeight/4.5f)-2, buttonSize, buttonSize), outputButton, style)) {
						outputButton = buttonStyle2;						
						DataObject.paint = true;
						DataObject.data = this;
						first = true;
					}
					break;
				
				// Not
				case 3:
					// In
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y+(gateHeight/6), buttonSize, buttonSize), buttonStyle, style)) {						
						connect(0);		
						first = true;
					}
					// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-4, m_Position.y + (gateHeight/4.5f)-2, buttonSize, buttonSize), outputButton, style)) {
						outputButton = buttonStyle2;						
						DataObject.paint = true;
						DataObject.data = this;	
						first = true;
					}
					break;

				// Xor
				case 4:
					// In
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y+(gateHeight/20), buttonSize, buttonSize), buttonStyle, style)) {						
						connect(0);	
						first = true;
					}
					if (GUI.Button (new Rect (m_Position.x-6, m_Position.y + (gateHeight/2.9f), buttonSize, buttonSize), buttonStyle, style)) {						
						connect(1);		
						first = true;
					}
					// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-4, m_Position.y + (gateHeight/4.5f)-2, buttonSize, buttonSize), outputButton, style)) {
						outputButton = buttonStyle2;						
						DataObject.paint = true;
						DataObject.data = this;
						first = true;
					}
					break;	
			
				// Light
				case 5:
					// In
					if (GUI.Button (new Rect (m_Position.x+20.512f, m_Position.y+(gateHeight/1.23f), buttonSize, buttonSize), buttonStyle, style)) {
						connect(0);
					}
					break;
				
				// Switch
				case 6:
					// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-6, m_Position.y + (gateHeight/2.235f)-2, buttonSize, buttonSize), outputButton, style)) {
						outputButton = buttonStyle2;						
						DataObject.paint = true;
						DataObject.data = this;
						first = true;
					}
					break;
				// One
				case 7:
					// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-6, m_Position.y + (gateHeight/3.35f)-2, buttonSize, buttonSize), outputButton, style)) {
						outputButton = buttonStyle2;						
						DataObject.paint = true;
						DataObject.data = this;
						first = true;
					}
					break;

				// Zero
				case 8:
					// Out
					if (GUI.Button (new Rect (m_Position.x+gateWidth-6, m_Position.y + (gateHeight/3.35f)-2, buttonSize, buttonSize), outputButton, style)) {
						
						outputButton = buttonStyle2;						
						DataObject.paint = true;
						DataObject.data = this;
						first = true;
					}
					break;
			
		}

		
		// Paint Input.touchCount > 0
		if (DataObject.paint && !Dragging ) {

			//while(Input.touchCount == 1){

			Vector2 pointA = new Vector2(drawRect.x,drawRect.y);
			Vector2 pointB =  new Vector2(Input.mousePosition.x, Input.mousePosition.y);

			Matrix4x4 matrix = GUI.matrix;

			if (lineTex == null) {
				lineTex = new Texture2D (1, 1);
			}

			/* Drag ?
			pointA.x += (Screen.width * 0.10f - 3);
			pointA.y += (Screen.height * 0.10f - 22);
			GUI.color = Color.black;
			GUI.DrawTexture (new Rect (pointA.x, pointA.y, 10, LogicFlow.width), lineTex);

			pointA.x += 10;
			
			float angle;
			if (pointA.y > pointB.y) {
				angle = 90;
			}else{
				angle = -90;
			}
			GUIUtility.RotateAroundPivot (angle, pointA);

			float n;
			if(pointA.y < pointB.y){
				GUI.DrawTexture (new Rect (pointA.x, pointA.y, Mathf.Abs(pointA.y-pointB.y)+10, LogicFlow.width), lineTex);
			}else{
				GUI.DrawTexture (new Rect (pointA.x, pointA.y, Mathf.Abs(pointA.y-pointB.y)-10, LogicFlow.width), lineTex);
			}
			
		
			angle = -90;
			GUIUtility.RotateAroundPivot (angle, pointA);
			pointA.y += Mathf.Abs(pointA.y-pointB.y)-10;
			
			if(pointA.y < pointB.y){
				n = pointA.x-pointB.x-5;
			}else{
				n = pointB.x-pointA.x+5;
			}
			if(pointA.y < pointB.y){
				GUI.DrawTexture (new Rect (pointA.x, pointA.y+18, n, LogicFlow.width), lineTex);
			}else{
				GUI.DrawTexture (new Rect (pointA.x, pointA.y, n, LogicFlow.width), lineTex);
			}

			GUI.matrix = matrix;

			//GUI.Label(drawRect, ""+Input.mousePosition.x);	
		
			//paint = false;
			//}

			*/
			GUI.matrix = matrix;

			/*
			if(Input.GetMouseButtonDown(0)){
				Debug.Log("here");
				
				outputButton = buttonStyle;
				DataObject.paint = false;
			}*/
				/*
				if(Input.GetMouseButtonUp(0) && new Rect(data.Position.x,data.Position.y, gateWidth, gateHeight).Contains(new Vector2(Input.mousePosition.x,Input.mousePosition.y))){
					if(data.input.Count < 2){			
						DesignHolder topdesign = DesignHolder.getDesignHolder;
						List<DataObject> temp = new List<DataObject>();
						temp.Add(this);
						temp.Add(data);
						topdesign.createConnection(this, output.Count, data, input.Count);
						topdesign.simulate();
						data.input.Add(this);
						this.output.Add(data);
						LogicFlow.connected.Add(temp);
						paint = false;
					}
				}*/


		}


		Drag (dragRect);
	}
	private void connect(int i){
		if (DataObject.paint) {							
			if(input.Length < 2){			

				DesignHolder topdesign = DesignHolder.getDesignHolder;

				List<DataObject> temp = new List<DataObject>();
				temp.Add(DataObject.data);
				temp.Add(this);
				topdesign.createConnection(DataObject.data, output.Length, this, i);

				topdesign.simulate();

				DataObject.data.output[0] = this;
				this.input[i] = DataObject.data;

				LogicFlow.connected.Add(temp);

				DataObject.paint = false;
				DataObject.data.setOutputTexture(buttonStyle);
				DataObject.data = null;
			}
		}
	}
	
	
	
}