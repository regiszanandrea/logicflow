﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Netlist;

public class LogicFlow : MonoBehaviour {

	public static List< DataObject > list = new List< DataObject > ();
	private List< DataObject > buttonsDraggable = new List< DataObject > ();
	private List< float > gatesPosition = new List< float > ();
	public static List<List<DataObject>> connected = new List<List<DataObject>> ();

	private bool first = false;

	// Textures das images
	public Texture2D or, and, not, xor, lightOff, lightOn, switchOff, switchOn, zero, one, close, save, lineTex, trash, logo, backgroundTextute;
	//public Rect trashRect = new Rect (Screen.width,Screen.height, 64, 64);
	public Rect trashRect = new Rect (Screen.width-32,Screen.height-32, 40, 40);
	public Rect closeRect = new Rect (Screen.width-40,60, 40, 40);
	public Rect saveRect = new Rect (Screen.width-40,10, 40, 40);
	public Texture buttonTexture, buttonTexture2;
	public Touch touch;
	public static float width = 1.9f;
	public GUIStyle style, boxStyle;
	public Vector2 myCircuitsScroll = Vector2.zero;
	public Vector2 basicGatesScroll = Vector2.zero;

	public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
	public float orthoZoomSpeed = 0.5f;



	
	void Start(){

		DesignHolder topdesign = DesignHolder.getDesignHolder;
		//Debug.Log (DesignHolder.getDesignHolder == null);
		// Necessario para criar a base -------------------
		//simulator = new LogicSimulator();	
		topdesign.setTopDesign(Design.createDesign("TopDesign"));
		topdesign.createLibrary();
		//-------------------------------------------------

		// Como pegar o valor logico de um objeto
		// LogicValue     simulator.getValue(DataObject.getDesignObj());
		// 	Debug.Log ("Here: "+simulator.getValue(list[4].getDesignObj()));

		list.Add (new DataObject ("OR", 1, new Vector2 (380, 100), or, style, buttonTexture, buttonTexture2 ));
		list[0].setDesignObj(topdesign.createInstance("OR", 2));

		list.Add (new DataObject ("OR", 1, new Vector2 (480, 200), or, style, buttonTexture, buttonTexture2 ));
		list[1].setDesignObj(topdesign.createInstance("OR", 2));

		list.Add (new DataObject ("Light", 5, new Vector2 (640, 50), lightOff, style, buttonTexture, buttonTexture2 ));
		list[2].setDesignObj(topdesign.createOutput("Light"));

		list.Add (new DataObject ("Light", 5, new Vector2 (630, 200), lightOn, style, buttonTexture, buttonTexture2 ));
     	list[3].setDesignObj(topdesign.createOutput("Light"));

		list.Add (new DataObject ("Switch", 6, new Vector2 (290, 100), switchOff, switchOn, style, buttonTexture, buttonTexture2 ));
	    list[4].setDesignObj(topdesign.createInput("Switch"));
	    topdesign.setValue(list[4].getDesignObj(), LogicValue.ZERO);

		list.Add (new DataObject ("Switch", 6, new Vector2 (290, 200), switchOff, switchOn, style, buttonTexture, buttonTexture2 ));
        list[5].setDesignObj(topdesign.createInput("Zero"));
      	topdesign.setValue(list[5].getDesignObj(), LogicValue.ZERO);

		list.Add (new DataObject ("Switch", 6, new Vector2 (290, 300), switchOff, switchOn, style, buttonTexture, buttonTexture2 ));
     	list[6].setDesignObj(topdesign.createInput("One"));
		topdesign.setValue(list[6].getDesignObj(), LogicValue.ZERO);

	
		List<DataObject> n = new List<DataObject>();
		n.Add (list [0]);
		n.Add (list [1]);
		connected.Add (n);

		
		// Para criar e conectar redes ---------
     	topdesign.createConnection(list[0], 0, list[1], 0);

		n = new List<DataObject>();
		n.Add (list [0]);
		n.Add (list [2]);
		connected.Add (n);

		// Para criar e conectar redes ---------
     	topdesign.createConnection(list[0], 0, list[2], 0);
		
		n = new List<DataObject>();
		n.Add (list [1]);
		n.Add (list [3]);
    	connected.Add (n);

		// Para criar e conectar redes ---------
		topdesign.createConnection(list[1], 0, list[3], 0);
		
		n = new List<DataObject>();
		n.Add (list [4]);
		n.Add (list [0]);
		connected.Add (n);
		
		// Para criar e conectar redes ---------
     	topdesign.createConnection(list[4], 0, list[0], 0);
		
		n = new List<DataObject>();
		n.Add (list [5]);
		n.Add (list [0]);
		connected.Add (n);

		// Para criar e conectar redes ---------
		topdesign.createConnection(list[5], 0, list[0], 1);
		
		n = new List<DataObject>();
		n.Add (list [6]);
		n.Add (list [1]);
		connected.Add (n);

		// Para criar e conectar redes ---------
   	  	topdesign.createConnection(list[6], 0, list[1], 1);

		/*
		// BRC

		string expression = "(!(a*!b)*(b*c))";


		List<List<string>> cubes = new List<List<string>>();

		List<string> cube = new List<string>();
		cube.Add("a"); cube.Add("b"); cube.Add("c");
		cubes.Add(cube);
		cube = new List<string>();
		cube.Add("b"); cube.Add("c");
		cubes.Add(cube);
		
		List<string> varSet = new List<string>();
		varSet.Add("a");
		varSet.Add("b");
		varSet.Add("c");

		SortedDictionary<string, BRC> basicCodes = BRCBuilder.getBasicRepresentationCodes(varSet.Count, varSet);

		BRC functionBRC = BRCHandler.builFunctionRepresentationCode(cubes, basicCodes);

		BRCHandler.displayBinary(functionBRC);

		*/
		// Resoluções
		// Android
		/*
		if (Screen.width == 480 && Screen.height == 320) {
				gatesPosition.Add (15);
				gatesPosition.Add (15);
				gatesPosition.Add (45);
				gatesPosition.Add (75);
		} else if ((Screen.width == 480 && Screen.height == 480) || (Screen.width == 854 && Screen.height == 480) || (Screen.width == 1024 && Screen.height == 600) || (Screen.width == 1280 && Screen.height == 800)) {
				gatesPosition.Add (15);
				gatesPosition.Add (15);
				gatesPosition.Add (64);
				gatesPosition.Add (116);
		}
		

		// iOS
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
				if (Screen.height == 320 && Screen.width == 480) {
						gatesPosition.Add (15);
						gatesPosition.Add (15);
						gatesPosition.Add (75);
						gatesPosition.Add (130);
				}
		}
		if (Screen.width == 1024 && Screen.height == 768) {
						gatesPosition.Add (15);
						gatesPosition.Add (15);
						gatesPosition.Add (65);
						gatesPosition.Add (115);
		} else if(Screen.width == 960 && Screen.height == 640){
			gatesPosition.Add (15);
			gatesPosition.Add (15);
			gatesPosition.Add (75);
			gatesPosition.Add (130);	
		}

		*/


		gatesPosition.Add (10); // X - Columm 1
		gatesPosition.Add (155); // X - Columm 2
		gatesPosition.Add (0); // Y - Line 1
		gatesPosition.Add (70); // Y - Line 2
		gatesPosition.Add (140); // Y - Line 3
		gatesPosition.Add (310); // Y
		//gatesPosition.Add (210);

		buttonsDraggable.Add (new DataObject ("OR", 1, new Vector2 (gatesPosition[0], gatesPosition[2]), or, style, buttonTexture, buttonTexture2 ));
		buttonsDraggable.Add (new DataObject ("AND", 2, new Vector2 (gatesPosition[1],gatesPosition[2]), and, style, buttonTexture , buttonTexture2));
		buttonsDraggable.Add (new DataObject ("INV", 3, new Vector2 (gatesPosition[0], gatesPosition[3]), not, style, buttonTexture, buttonTexture2 ));
		buttonsDraggable.Add (new DataObject ("XOR", 4, new Vector2 (gatesPosition[1], gatesPosition[3]), xor, style, buttonTexture, buttonTexture2 ));
		buttonsDraggable.Add (new DataObject ("Light", 5, new Vector2 (gatesPosition[0], gatesPosition[4]), lightOff, style, buttonTexture, buttonTexture2 ));
		buttonsDraggable.Add (new DataObject ("Switch", 6, new Vector2 (gatesPosition[1], gatesPosition[4]), switchOff, switchOn, style, buttonTexture, buttonTexture2 ));
		buttonsDraggable.Add (new DataObject ("One", 7, new Vector2 (gatesPosition[0], gatesPosition[5]), one, style, buttonTexture, buttonTexture2 ));
		buttonsDraggable.Add (new DataObject ("Zero", 8, new Vector2 (gatesPosition[1], gatesPosition[5]), zero, style, buttonTexture, buttonTexture2 ));

		// Mostra toda estrutura do TopDesign
		//Debug.Log(topdesign.getTopDesign().ToString());
		
		// Call simulator for evaluation
		topdesign.simulate ();
	
	}


	public void OnGUI ()
	{
		//Background
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), backgroundTextute);

		// Buttons
		trashRect.x = Screen.width-52;
		trashRect.y = Screen.height - 48;
		trashRect.height = 40;
		trashRect.width = 40;

		closeRect.x = Screen.width - 52;
		closeRect.y = 15;
		closeRect.height = 40;
		closeRect.width = 40;

		saveRect.x = Screen.width - 52;
		saveRect.y = 60;
		saveRect.height = 40;
		saveRect.width = 40;
		


		DataObject toFront;
		Color color;

		// Logo
		GUI.Label (new Rect (20, 20, 190, 185), logo);

		// Basic Gates
		GUI.BeginGroup (new Rect (5, 190, 330, 200));
		GUI.Box (new Rect (0,0, 330, 200), "Basic Gates" );	
			//basicGatesScroll = GUI.BeginScrollView(new Rect(5, 30, 210, 120), basicGatesScroll, new Rect(0, 0, 100, 310), GUIStyle.none, GUIStyle.none);
			basicGatesScroll = GUI.BeginScrollView(new Rect(0, 30, 320, 160), basicGatesScroll, new Rect(0, 0, 160, 390));	
				foreach (DataObject data in buttonsDraggable)
				{
				
					color = GUI.color;
							
					data.OnGUI ();			
					GUI.color = color;
				}
						
			GUI.EndScrollView();
		GUI.EndGroup ();
		// End - Basic Gates
		


		// My Circuits
		GUI.BeginGroup (new Rect (5, 430, 330, 215));
			
			GUI.Box (new Rect (0,0,330,215), "My Circuits");
			
			myCircuitsScroll = GUI.BeginScrollView(new Rect(0, 30, 330, 190), myCircuitsScroll, new Rect(0, 0, 160, 200));
//			GUI.Button(new Rect(0, 0, 100, 20), "Top-left");
//			GUI.Button(new Rect(50, 0, 100, 20), "Top-right");
//			GUI.Button(new Rect(0, 180, 100, 20), "Bottom-left");
//			GUI.Button(new Rect(50, 180, 100, 20), "Bottom-right");
			GUI.EndScrollView();
		GUI.EndGroup ();
		// End - My Circuits

		// Design Area
		GUI.BeginGroup (new Rect (350, 10, Screen.width-330, Screen.height-15));
			GUI.backgroundColor = Color.white;
			GUI.Box (new Rect (0,0,Screen.width-290,Screen.height-15), "", boxStyle);		
			//GUI.Box (new Rect (0,0,Screen.width-240,Screen.height-15), "");		
		GUI.EndGroup ();
		// Edn - Design Area

		GUI.backgroundColor = Color.white;


		// Roteamento
		if(connected.Count > 0){
			foreach(List<DataObject> entry in connected) {
				Vector2 pointA = entry[0].Position;
				Vector2 pointB = entry[1].Position;



				if(entry[1].m_Value == 5){
					pointB.y += 90;
					pointB.x += 20;
				}
				if(entry[0].m_Value == 6){
					pointA.x += 20;
					pointA.y += 20;
				}
				
				if(entry[0].m_Value == 7 || entry[0].m_Value == 8  ){
					pointA.x -= 5;
					pointA.y += 8;
				}

				if(entry[0].m_Value == 1 || entry[0].m_Value == 2){
					pointA.x += 10;
					pointA.y -= 6;
				}

				Matrix4x4 matrix = GUI.matrix;

				if (lineTex == null) {
					lineTex = new Texture2D (1, 1);
				}
				pointA.x += (Screen.width * 0.10f - 3);
				pointA.y += (Screen.height * 0.10f - 22);

				// HighLight
				DesignHolder topdesign = DesignHolder.getDesignHolder;
				if(topdesign.getValue(entry[0].getDesignObj()) == LogicValue.ONE){
					GUI.color = Color.red;
				}else{
					GUI.color = Color.black;
				}


				GUI.DrawTexture (new Rect (pointA.x, pointA.y, 10, width), lineTex);
				pointA.x += 10;

				float angle;
				if (pointA.y > pointB.y) {
					angle = -90;
				}else{
					angle = 90;
				}
				GUIUtility.RotateAroundPivot (angle, pointA);

				float n;
				if(pointA.y < pointB.y){
					GUI.DrawTexture (new Rect (pointA.x, pointA.y, Mathf.Abs(pointA.y-pointB.y)+10, width), lineTex);
				}else{
					GUI.DrawTexture (new Rect (pointA.x, pointA.y, Mathf.Abs(pointA.y-pointB.y)-10, width), lineTex);
				}

				angle = -90;
				GUIUtility.RotateAroundPivot (angle, pointA);
				pointA.y += Mathf.Abs(pointA.y-pointB.y)-10;

				if(pointA.y > pointB.y){
					n = pointA.x-pointB.x-5;
				}else{
					n = pointB.x-pointA.x+5;
				}
				if(pointA.y < pointB.y){
					GUI.DrawTexture (new Rect (pointA.x, pointA.y+18, n, width), lineTex);
				}else{
					GUI.DrawTexture (new Rect (pointA.x, pointA.y, n, width), lineTex);
				}

				GUI.matrix = matrix;
			}
			GUI.color = Color.white;
		}
		// End - Roteamentos




		toFront  = null;
		foreach (DataObject data in list)
		{
			color = GUI.color;
			

			data.OnGUI ();
			
			GUI.color = color;
			
			if (data.Dragging)
			{
				if (list.IndexOf (data) != list.Count - 1)
				{
					toFront = data;
				}
			}
		}
		
		if (toFront != null)
			// Move an object to front if needed
		{
			list.Remove (toFront);
			list.Add (toFront);
		}



		toFront  = null;
		foreach (DataObject data in buttonsDraggable)
		{								
			if (data.Dragging){
				if (buttonsDraggable.IndexOf (data) != buttonsDraggable.Count){
					toFront = data;
				}
			}
		}



		// Exclusão
		foreach(DataObject data in list){
			//Debug.Log(data.m_Value+":"+data.Position.x+","+data.Position.y);
			if(trashRect.Contains(new Vector2(data.Position.x+(data.gateWidth/2),data.Position.y+(data.gateHeight/2))) && !data.Dragging){	
				foreach(List<DataObject> n in connected){
					if(n[0].Equals(data) || n[1].Equals(data)){
						connected.Remove(n);
					}				
				}
				list.Remove(data);
				break;
			}
		}

		if (toFront != null) {
			buttonsDraggable.Remove (toFront);
			DesignHolder topdesign = DesignHolder.getDesignHolder;
			if (!first) {
				switch(toFront.m_Value){
					case 1:	
						toFront.setDesignObj(topdesign.createInstance("OR", 2));
						buttonsDraggable.Add (new DataObject ("OR", 1, new Vector2 (gatesPosition[0], gatesPosition[2]), or, style, buttonTexture, buttonTexture2 ));
						break;
					case 2:
						toFront.setDesignObj(topdesign.createInstance("AND", 2));
						buttonsDraggable.Add (new DataObject ("AND", 2, new Vector2 (gatesPosition[1],gatesPosition[2]), and, style, buttonTexture, buttonTexture2 ));
						break;
					case 3:
						toFront.setDesignObj(topdesign.createInstance("INV", 1));
						buttonsDraggable.Add (new DataObject ("INV", 3, new Vector2 (gatesPosition[0], gatesPosition[3]), not, style, buttonTexture, buttonTexture2 ));
						break;
					case 4:
						toFront.setDesignObj(topdesign.createInstance("XOR", 2));
						buttonsDraggable.Add (new DataObject ("XOR", 4, new Vector2 (gatesPosition[1], gatesPosition[3]), xor, style, buttonTexture, buttonTexture2 ));
						break;
					case 5:
						toFront.setDesignObj(topdesign.createOutput("LIGHT"));
						buttonsDraggable.Add (new DataObject ("Light", 5, new Vector2 (gatesPosition[0], gatesPosition[4]), lightOff, style, buttonTexture, buttonTexture2 ));
						break;
					case 6:
						toFront.setDesignObj(topdesign.createInput("SWITCH"));
						topdesign.setValue(toFront.getDesignObj(), LogicValue.ZERO);
						buttonsDraggable.Add (new DataObject ("Switch", 6, new Vector2 (gatesPosition[1], gatesPosition[4]), switchOff, switchOn, style, buttonTexture, buttonTexture2 ));
						break;
					case 7:
						toFront.setDesignObj(topdesign.createInput("ONE"));
						topdesign.setValue(toFront.getDesignObj(), LogicValue.ONE);
						buttonsDraggable.Add (new DataObject ("One", 7, new Vector2 (gatesPosition[0], gatesPosition[5]), one, style, buttonTexture, buttonTexture2 ));
						break;
					case 8:
						toFront.setDesignObj(topdesign.createInput("ZERO"));
						topdesign.setValue(toFront.getDesignObj(), LogicValue.ZERO);
						buttonsDraggable.Add (new DataObject ("Zero", 8, new Vector2 (gatesPosition[1], gatesPosition[5]), zero, style, buttonTexture, buttonTexture2 ));
						break;
				}

				list.Add(toFront);
				first = true;
			}
		} else {
			first = false;		
		}

		GUI.Label (trashRect, trash);
		GUI.Label (closeRect, close);
		GUI.Label (saveRect, save);


	}
	void Update(){
		DesignHolder topdesign = DesignHolder.getDesignHolder;
		if(Input.GetMouseButtonDown(0) && saveRect.Contains(new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y))){
			// Save			
		}

		if(Input.GetMouseButtonDown(0) && closeRect.Contains(new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y))){
			list.Clear();
			connected.Clear();
			topdesign.reset(Design.createDesign("TopDesign"));
		}

		foreach (DataObject data in list) {
			if(Input.GetMouseButtonDown(0) && new Rect (data.Position.x, data.Position.y, data.gateHeight, data.gateHeight).Contains(new Vector2 (Input.mousePosition.x, Screen.height - Input.mousePosition.y)) && data.m_Value == 6 ){

				data.changeSwitch ();
				if(data.getSwitch()){
					topdesign.setValue(data.getDesignObj(), LogicValue.ZERO);
				}else{
					topdesign.setValue(data.getDesignObj(), LogicValue.ONE);

				}
				topdesign.simulate();
			}	
		}
		foreach (DataObject data in list) {
			if(data.getName().Contains("Light")){
				if(topdesign.getValue(data.getDesignObj()) == LogicValue.ONE){
					data.setTexture (lightOn);
				}else{
					data.setTexture (lightOff);
				}

			}
		}



		/* Zoom
		// If there are two touches on the device...
		if (Input.touchCount == 2)
		{
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			
			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
			
			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
			
			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
			
			// If the camera is orthographic...
			if (camera.isOrthoGraphic)
			{
				// ... change the orthographic size based on the change in distance between the touches.
				camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
				
				// Make sure the orthographic size never drops below zero.
				camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
			}
			else
			{
				// Otherwise change the field of view based on the change in distance between the touches.
				camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
				
				// Clamp the field of view to make sure it's between 0 and 180.
				camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 0.1f, 179.9f);
			}
		}*/

		// Scroll
		/*if(new Rect (10, 270, 170, 200).Contains(Event.current.mousePosition))
		{
			if(Input.touchCount > 0)
			{
				touch = Input.touches[0];
				if (touch.phase == TouchPhase.Moved)
				{
					myCircuitsScroll.y += touch.deltaPosition.y;
				}
			}
		}*/
	}
}
