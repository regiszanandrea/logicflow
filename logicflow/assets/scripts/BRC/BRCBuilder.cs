﻿using System;
using System.Collections;
using System.Collections.Generic;


public class BRCBuilder {
	public BRCBuilder() {
		
	}

	public static SortedDictionary<string, BRC> getBasicRepresentationCodes(int nVariables, List<string> variablesSet) {
		List<BRC> basicCodesArray = buildBasicRepresentationCodes(nVariables);

		SortedDictionary<string, BRC> basicCodes = new SortedDictionary<string, BRC>();
		string variable;
		BRC brc;

		for(int i=0; i < variablesSet.Count; i++) {
			
			variable = variablesSet[i];
			brc = basicCodesArray[(basicCodesArray.Count-1)-i]; // remove do array do final pra o inicio
			// Direct BRC

			basicCodes.Add(variable, brc);            
			// Complementary BRC
			variable = "!" + variable;
			basicCodes.Add(variable, BRCHandler.not(brc));            
		}      
//		foreach(KeyValuePair<string, BRC> entry in basicCodes) {
//			//Debug.Log(entry.Key + ": ");
//			BRCHandler.displayBinary(entry.Value);
//		}
		
		return basicCodes;
	}
	// parei aqui
	private static List<BRC> buildBasicRepresentationCodes(int nVariables) {
		List<BRC> BRCList = new List<BRC>();
		int nBits = (int)(Math.Pow(2, nVariables));
		double nInteger = (nBits / 64);
		int iteration = 0;
		

		if (nVariables <= 6) {
			return BRCTemplate.getBRCStructure(nVariables);
		} else {
			
			for (int pos = 0; pos < 6; pos++) {
				BRCList.Add(new BRC(nBits));
				
				for (int integer = 0; integer < nInteger; integer++) {
					BRCList[pos].addBRC(BRCTemplate.getIntegerBRC(pos + 1));
				}
			}
			
			for (int pos = 6; pos < nVariables; pos++) {
				BRCList.Add(new BRC(nBits));
				
				int integer = 0;
				while (integer < nInteger) {
					
					/*atribui valor 0 para assinatura*/
					for (int i = 0; i < Math.Pow(2, iteration); i++) {
						BRCList[pos].addBRC(-1);
						integer++;
					}
					
					/*atribui valor -1 para a assinatura*/
					for (int j = 0; j < Math.Pow(2, iteration); j++) {
						BRCList[pos].addBRC(0);
						integer++;
					}
				}
				iteration++;
			}
		}
		
		return BRCList;
	}
}
