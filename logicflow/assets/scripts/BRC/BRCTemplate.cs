﻿using System;
using System.Collections;
using System.Collections.Generic;


public class BRCTemplate {

	public BRCTemplate() {
	}
	
	public static List<BRC> getBRCStructure(int amountVariable) {
		
		List<BRC> Struct = new List<BRC>();
		int nBits = (int)(Math.Pow(2, amountVariable));
		
		switch (amountVariable) {
		case 1:
			Struct.Add(new BRC(nBits));
			Struct[0].setBRC(0, 2L);
			break;
			
		case 2:
			Struct.Add(new BRC(nBits));
			Struct[0].setBRC(0, 10L);
			Struct.Add(new BRC(nBits));
			Struct[1].setBRC(0, 12L);
			break;
			
		case 3:
			Struct.Add(new BRC(nBits));
			Struct[0].setBRC(0, 170L);
			Struct.Add(new BRC(nBits));
			Struct[1].setBRC(0, 204L);
			Struct.Add(new BRC(nBits));
			Struct[2].setBRC(0, 240L);
			break;
			
		case 4:
			Struct.Add(new BRC(nBits));
			Struct[0].setBRC(0, 43690L);
			Struct.Add(new BRC(nBits));
			Struct[1].setBRC(0, 52428L);
			Struct.Add(new BRC(nBits));
			Struct[2].setBRC(0, 61680L);
			Struct.Add(new BRC(nBits));
			Struct[3].setBRC(0, 65280L);
			break;
			
		case 5:
			Struct.Add(new BRC(nBits));
			Struct[0].setBRC(0, 2863311530L);
			Struct.Add(new BRC(nBits));
			Struct[1].setBRC(0, 3435973836L);
			Struct.Add(new BRC(nBits));
			Struct[2].setBRC(0, 4042322160L);
			Struct.Add(new BRC(nBits));
			Struct[3].setBRC(0, 4278255360L);
			Struct.Add(new BRC(nBits));
			Struct[4].setBRC(0, 4294901760L);
			break;
			
		case 6:
			Struct.Add(new BRC(nBits));
			Struct[0].setBRC(0, -6148914691236517206L); 
			Struct.Add(new BRC(nBits));
			Struct[1].setBRC(0, -3689348814741910324L);
			Struct.Add(new BRC(nBits));
			Struct[2].setBRC(0, -1085102592571150096L);
			Struct.Add(new BRC(nBits));
			Struct[3].setBRC(0, -71777214294589696L);
			Struct.Add(new BRC(nBits));
			Struct[4].setBRC(0, -281470681808896L);
			Struct.Add(new BRC(nBits));
			Struct[5].setBRC(0, -4294967296L);
			break;
			
		default:
			break;
		}
		
		return Struct;
	}
	
	public static long getIntegerBRC(int amountVariable) {
		
		long temp = 0;
		
		switch (amountVariable) {
		case 1:
			temp = -6148914691236517206L;
			break;
		case 2:
			temp = -3689348814741910324L;
			break;
		case 3:
			temp = -1085102592571150096L;
			break;
		case 4:
			temp = -71777214294589696L;
			break;
		case 5:
			temp = -281470681808896L;
			break;
		case 6:
			temp = -4294967296L;
			break;
		default:
			break;
		}
		
		return temp;
	}
}
