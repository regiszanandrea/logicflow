﻿using UnityEngine;
using Netlist;
using System.Collections;
using System.Collections.Generic;

public class DesignHolder : MonoBehaviour {

	private Dictionary<string, Design> library = new Dictionary<string, Design>();
	private Design topdesign;
	private int id;
	private LogicSimulator simulator;
	private static bool instantiated = false;

	private static DesignHolder instance;
	
	private DesignHolder() {}
	
	public static DesignHolder getDesignHolder{
		get {
			if (!DesignHolder.instantiated){
				instance = new DesignHolder();
				instance.id = 0;
				instance.simulator = new LogicSimulator();
				instance.library = new Dictionary<string, Design>();
				DesignHolder.instantiated = true;
			}
			return instance;
		}
	}

	public void reset(Design design) {
		instance.topdesign = design;
		instance.id = 0;
		instance.simulator = new LogicSimulator();
		//instance.library = new Dictionary<string, Design>();
		DesignHolder.instantiated = true;
	}

	public void setTopDesign(Design top) {
		topdesign = top;
	}

	public Design getTopDesign() {
		return topdesign;
	}

	public void simulate() {
		simulator.visit (topdesign);
	}

	public void setValue(DesignObj obj, LogicValue value) {
		simulator.setValue(obj, value);
	}

	public LogicValue getValue(DesignObj obj) {
		return simulator.getValue(obj);
	}
	
	public Design createBacisCellDesign(string name, int n_inputs) {
		Cell cell = new Cell(name);
		Design dcell = Design.createCellDesign(cell);
		for (int i = 0; i < n_inputs; i++) {
			Terminal.createTerminal(dcell, "I"+i, TermType.IN);			
		}
		Terminal.createTerminal(dcell, "O0", TermType.OUT);
		return dcell;
	}
	
	public void createLibrary() {
		library.Add("OR", createBacisCellDesign("OR", 2));
		library.Add("AND", createBacisCellDesign("AND", 2));
		library.Add("XOR", createBacisCellDesign("XOR", 2));
		library.Add("INV", createBacisCellDesign("INV", 1));
	}
	
	private string createName(string name) {
		id++;
		return name+id;
	}
	
	public Instance createInstance(string cellname, int n_inputs) {
		Design cell = library[cellname];
		Instance inst = Instance.createInstance(cell, topdesign, createName("I")+"_"+cellname+n_inputs);
		Terminal term;
		for (int i = 0; i < n_inputs; i++) {
			if (Terminal.find(cell, "I"+i, out term))
				InstTerminal.createInstTerminal(inst, term);
		}
		if (Terminal.find(cell, "O0", out term))
			InstTerminal.createInstTerminal(inst, term);		
		return inst;
	}
	
	public Terminal createInput(string name) {
		return Terminal.createTerminal(topdesign, createName(name), TermType.IN);
	}
	
	public Terminal createOutput(string name) {
		return Terminal.createTerminal(topdesign, createName(name), TermType.OUT);
	}
	
	public void createConnection(DataObject source, int iSource, DataObject target, int iTarget) {
		Net net;
		//Debug.Log(" -> [Source] : "+source.getDesignObj().getName());
		//Debug.Log(" -> [Target] : "+target.getDesignObj().getName());
		if (source.getDesignObj().GetType().Name.Equals("Instance")) {
			InstTerminal instterm;
			InstTerminal.find((Instance) source.getDesignObj(), source.getDesignObj().getName()+".O"+iSource, out instterm);			
			if (instterm.isConnected())
				net = instterm.getNet();
			else
				net = Net.createNet(topdesign, createName("n"));
			net.connectInstTerminal(instterm);
		} else {
			Terminal term = (Terminal) source.getDesignObj();
			if (term.isConnected())
				net = term.getNet();
			else
				net = Net.createNet(topdesign, createName("n"));
			net.connectTerminal(term);
		}
		if (target.getDesignObj().GetType().Name.Equals("Instance")) {
			InstTerminal instterm;
			InstTerminal.find((Instance) target.getDesignObj(), target.getDesignObj().getName()+".I"+iTarget, out instterm);
			net.connectInstTerminal(instterm);
		} else {
			net.connectTerminal((Terminal) target.getDesignObj());
		}
	}
}
